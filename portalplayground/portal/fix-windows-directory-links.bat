dir /al | find /i ".vscode" | find "<SYMLINKD>"  >nul || (
  del  /F /Q .vscode
  mklink /d .vscode ..\core.lib\.vscode
)

dir /al "src\app" | find /i "core.lib" | find "<SYMLINKD>" >nul || (
  del  /F /Q src\app\core.lib
  mklink /d src\app\core.lib ..\..\..\core.lib\src\app\core.lib
)

dir /al "src\app" | find /i "core.ui" | find "<SYMLINKD>" >nul || (
  del  /F /Q src\app\core.ui
  mklink /d src\app\core.ui ..\..\..\core.ui\src\app\core.ui
)

dir /al "src\app" | find /i "wamp" | find "<SYMLINKD>" >nul || (
  del  /F /Q src\app\wamp
  mklink /d src\app\wamp ..\..\..\wamp\src\app\wamp
)

dir /al "src" | find /i "assets" | find "<SYMLINKD>" >nul || (
  del  /F /Q src\assets
  mklink /d src\assets ..\..\core.ui\src\assets
)
