import { Component, Input } from '@angular/core';
import { navItems } from './_nav'; // fix this!

@Component({
  selector: 'app-dashboard',
  templateUrl: './defaultlayout.component.html',
  styleUrls: ['./defaultlayout.component.scss']
})
export class DefaultLayoutComponent {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;

  constructor() {
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }
}
