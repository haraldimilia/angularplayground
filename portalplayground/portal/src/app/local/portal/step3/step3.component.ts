import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { DynamicFormComponent, FormDescription, FormType, FormValidationService, ValidationError } from '../../../core.ui';
import { Log } from 'ng2-logger/client';
import { Stepbasic } from '../stepbasic';
import { Workingset } from '../workingset';

const log = Log.create('Step3Component');

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements AfterViewInit, Stepbasic {
  @Input() ws: Workingset = null;
  @ViewChild(DynamicFormComponent) dynamicFrom: DynamicFormComponent;
  patientEmail: string;
  stepStatusValid = false;
  formGroup: FormGroup;
  errorJsonString: any = '';
  datePattern = '(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}';
  formFName = new FormControl(['', [ Validators.required, Validators.minLength(5) ]]);
  formLName = new FormControl(['', Validators.required ]);
  formGender = new FormControl(['', Validators.required ]);
  formInsurance = new FormControl(['', Validators.required ]);
  formDate = new FormControl(['', [Validators.required, Validators.pattern('(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}')]]);
  formEmail = new FormControl(['', [Validators.required, Validators.email]]);
  formPhone = new FormControl(['', [Validators.required, Validators.pattern('/^-?(0|[1-9]\d*)?$/')]]);
  formComment = new FormControl(['', Validators.maxLength(150)]);

  errors: ValidationError = {
    validationErrorMessage: null,
    fieldErrors: {}
  };

  constructor(private formBuilder: FormBuilder,
    private formValidationService: FormValidationService) {
    log.info('constructor');
  }

  ngAfterViewInit() {
    log.info('ngAfterViewInit');
    this.ws.stepBasic = this;
    this.onInit();
    this.ws.stepStatusValid = this.stepStatusValid;

  }

  onInit() {
    log.info('onInit');
    // this.onCreateDynamicItems();
    // this.patientEmail = this.ws.giveEmail();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    log.info('ngOnInit');
    this.formGroup = this.formBuilder.group({
      formFName: this.formFName,
      formLName: this.formLName,
      formGender: this.formGender,
      formDate: this.formDate,
      formStreet: [''],
      formNr: [''],
      formPLZ: [''],
      formOrt: [''],
      formInsurance: this.formInsurance,
      formEmail: this.formEmail,
      formPhone: this.formPhone,
      formComment: this.formComment
    });
  }

  onNext(): boolean {
    log.info('onNext');
    return true;
  }

  onPrev(): boolean {
    log.info('onPrev');
    return true;
  }

  validateForm() {
    this.errorJsonString = '';
    if (!this.formGroup.valid) {

      // https://stackoverflow.com/questions/40680321/get-all-validation-errors-from-angular-2-formgroup
      const getErrors = (formGroup: FormGroup, errors: any = {}) => {
        Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
            errors[field] = control.errors;
          } else if (control instanceof FormGroup) {
            errors[field] = getErrors(control);
          }
        });
        return errors;
      };

      // Calling it:
      this.errorJsonString = getErrors(this.formGroup);

      return;
    }

    log.info('form values', this.formGroup.value);
    this.errorJsonString = {
      ok: true
    };
  }

  private createValidationErrors(errors: Array<string>) {
    this.errors = {
      validationErrorMessage: null,
      fieldErrors: {}
    };

    if (errors != null) {
      this.errors.validationErrorMessage = 'Global summary message';
    }

    for (const key in this.formGroup.controls) {
      if (!this.formGroup.controls.hasOwnProperty(key)) {
        continue;
      }
      if (errors != null) {
        this.errors.fieldErrors[key] = errors;
      }
    }

    this.formValidationService.applyFieldErrorToFrom(this.formGroup, this.errors);
  }

  private updateStepStatusValid() {
    log.info('updateStepStatusValid');
    this.stepStatusValid = true;

    this.ws.stepStatusValid = this.stepStatusValid;
  }

  public checkRequired() {
    if (this.ws.fName != null     && this.ws.fName !== ''     && this.ws.fName !== undefined     &&
        this.ws.lName != null     && this.ws.lName !== ''     && this.ws.lName !== undefined     &&
        this.ws.gender != null    && this.ws.gender !== ''    && this.ws.gender !== undefined    &&
        this.ws.insurance != null && this.ws.insurance !== '' && this.ws.insurance !== undefined &&
        this.ws.phone != null     && this.ws.phone !== ''     && this.ws.phone !== undefined    ) {

      this.updateStepStatusValid();
    } else {
      return null;
    }
  }

   // public onCreateDynamicItems() {

  //   const patDetails: FormDescription = {
  //     name: 'myform', // auto name HTMLIds with systematic names
  //     showFlat: true,
  //     items: [
  //       {
  //         name: 'fname',
  //         type: FormType.Text,
  //         label: 'First Name:*',
  //         parameters: {
  //           placeholder: ''
  //         },
  //         constraints: {
  //           required: true
  //         }
  //       },
  //       {
  //         name: 'lname',
  //         type: FormType.Text,
  //         label: 'Last Name:*',
  //         parameters: {
  //           placeholder: ''
  //         },
  //         constraints: {
  //           required: true
  //         }
  //       },
  //       {
  //         name: 'gender',
  //         kvpName: 'genders',
  //         type: FormType.DropDown,
  //         label: 'Gender*',
  //         parameters: {
  //           placeholder: ''
  //         },
  //         constraints: {
  //           required: true
  //         }
  //       },
  //       {
  //         name: 'mydate',
  //         type: FormType.Date,
  //         label: 'DOB*',
  //         parameters: {
  //         },
  //         constraints: {
  //           required: true
  //         }
  //       },
  //       {
  //         name: 'street',
  //         type: FormType.Text,
  //         label: 'Street',
  //         parameters: {
  //           placeholder: 'Examplestraße'
  //         },
  //         constraints: {
  //         }
  //       },
  //       {
  //         name: 'number',
  //         type: FormType.Text,
  //         label: 'Number',
  //         parameters: {
  //           placeholder: '1'
  //         },
  //         constraints: {
  //         }
  //       },
  //       {
  //         name: 'city',
  //         type: FormType.Text,
  //         label: 'City',
  //         parameters: {
  //           placeholder: 'City'
  //         },
  //         constraints: {
  //         }
  //       },
  //       {
  //         name: 'postcode',
  //         type: FormType.Text,
  //         label: 'Postcode',
  //         parameters: {
  //           placeholder: 'PLZ'
  //         },
  //         constraints: {
  //         }
  //       },
  //       {
  //         name: 'insurance',
  //         type: FormType.DropDown,
  //         kvpName: 'insurance',
  //         label: 'Insurance Provider',
  //         parameters: {
  //           placeholder: 'Choose a Provider...'
  //         },
  //         constraints: {
  //         }
  //       },
  //       {
  //         name: 'email',
  //         type: FormType.Text,
  //         label: 'Email',
  //         value: this.patientEmail,
  //         parameters: {
  //           placeholder: this.patientEmail
  //         },
  //         constraints: {
  //         },
  //         disabled: true
  //       },
  //       {
  //         name: 'phone',
  //         type: FormType.Text,
  //         label: 'Telephone',
  //         parameters: {
  //           placeholder: '+49 1701234567'
  //         },
  //         constraints: {
  //         }
  //       },
  //       {
  //         name: 'comments',
  //         type: FormType.Text,
  //         label: 'Comments',
  //         parameters: {
  //           placeholder: 'Comments..'
  //         },
  //         constraints: {
  //         }
  //       }
  //      ],
  //      kvpitems: {
  //       genders: [
  //         {
  //           label: 'Male',
  //           value: '1'
  //         },
  //         {
  //           label: 'Female',
  //           value: '2'
  //         },
  //         {
  //           label: 'Other',
  //           value: '3'
  //         },
  //       ],
  //       insurance: [
  //         {
  //           label: 'Teckniker Krankenkasse',
  //           value: '1'
  //         },
  //         {
  //           label: 'AOK',
  //           value: '2'
  //         },
  //         {
  //           label: 'Liberty Health Insurance',
  //           value: '3'
  //         },
  //         {
  //           label: 'Viva',
  //           value: '3'
  //         },
  //       ],
  //     }
  //   };
  //   this.dynamicFrom.init(patDetails, (reason: string, valid: boolean, values: any, errors?: any) => {
  //     this.callback(reason, valid, values, errors);
  //   });
  // }

  // public callback (reason: string, valid: boolean, values: any, errors?: any) {
  //   if (valid) {
  //     console.log('from ok -  button', reason, values);
  //   } else {
  //     console.error('from error', reason, values, errors);
  //   }
  // }
}
