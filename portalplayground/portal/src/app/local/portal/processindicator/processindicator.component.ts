import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { KeyValueItem } from '../../../core.ui';
import { Log } from 'ng2-logger/client';
import { Workingset } from '../workingset';

const log = Log.create('ProcessIndicatorComponent');

@Component({
  selector: 'app-processindicator',
  templateUrl: './processindicator.component.html',
  styleUrls: ['./processindicator.component.scss']
})
export class ProcessIndicatorComponent implements OnInit {
  @Input() ws: Workingset;
  @Input() step: boolean;
  @Input() buttonClass: string;
  @Output() myEmitter: EventEmitter<any> = new EventEmitter<any>();
  button: HTMLElement;
  activestep: string;

  constructor() {
    log.info('constructor');
  }

  ngOnInit() {
  }

  onButtonPressed(stepName: string) {
    log.info('onButtonPressed', stepName);
    this.ws.goBackToStep(stepName);
  }

}
