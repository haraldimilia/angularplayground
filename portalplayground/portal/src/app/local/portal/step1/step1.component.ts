import { AfterViewInit, ChangeDetectorRef, Component, Input, LOCALE_ID, ViewChild } from '@angular/core';
import { DateTimeService } from './../../../core.lib/datetime/datetime.service';

import { DynamicFormComponent, FormDescription, FormItem, FormType, KeyValueItem } from '../../../core.ui';
import { Log } from 'ng2-logger/client';
import { Stepbasic } from '../stepbasic';
import { Workingset } from '../workingset';

const log = Log.create('Step1Component');

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss'],
  providers: [{ provide: LOCALE_ID, useValue: 'en_DE' }]
})
export class Step1Component implements AfterViewInit, Stepbasic {
  private static bookingWindowCaptionDefault = 'Start date of search';
  @Input() ws: Workingset = null;
  @ViewChild(DynamicFormComponent) dynamicFrom: DynamicFormComponent;
  bookingWindowCaption: string = Step1Component.bookingWindowCaptionDefault;
  applyedBookingPlanAttributes: false;
  date: Date = new Date(); // TODO: fix me!
  // locale = window.navigator.languages;

  weekDaySelectable = false;
  dayOfWeekItems: KeyValueItem[] = [];
  dayOfWeekValues: KeyValueItem[] = [];

  dayTimeSelectable = false;
  timeOfDayItems: KeyValueItem[] = [];
  timeOfDayValues: KeyValueItem[] = [];

  constructor(private changeDetectorRef: ChangeDetectorRef, private dateTime: DateTimeService) {
    log.info('constructor');
  }

  ngAfterViewInit() {
    log.info('ngAfterViewInit');
    this.onInit();
  }

  onInit() {
    log.info('onInit');
    this.ws.stepBasic = this;
    this.fillCheckboxGroups();
    this.onPurposeChanged();
    this.updateStepStatusValid();
  }

  onNext(): boolean {
    log.info('onNext');
    // console.log(this.ws.date);
    return true;
  }

  onPrev(): boolean {
    log.info('onPrev');
    return true;
  }

  onPurposeChanged() {
    log.info('onPurposeChanged', this.ws ? this.ws.selectedPurposeValue : '');

    this.bookingWindowCaption = Step1Component.bookingWindowCaptionDefault;
    this.applyedBookingPlanAttributes = false;

    this.weekDaySelectable = false;
    this.dayTimeSelectable = false;

    if (this.ws == null || this.ws === undefined ||
      this.ws.selectedPurposeValue == null ||
      this.ws.selectedPurposeValue === undefined ||
      this.ws.selectedPurposeValue === 'null') {
      return;
    }

    this.ws.selectPurpose(Number(this.ws.selectedPurposeValue)).subscribe(
      () => {
        log.info('selectedPurposeConfig', this.ws.selectedPurposeConfig);
        if (this.ws.selectedPurposeConfig && this.ws.selectedPurposeConfig.bookingWindowCaption) {
          // caption changes there
          this.bookingWindowCaption = this.ws.selectedPurposeConfig.bookingWindowCaption;
        }

        this.weekDaySelectable = this.ws.selectedPurposeConfig.weekDaySelectable;
        this.dayTimeSelectable = this.ws.selectedPurposeConfig.dayTimeSelectable;

        // our complex UI might need refresh to give access to dynamicFrom
        this.changeDetectorRef.markForCheck();

        if (this.dynamicFrom) {
          if (this.ws.hasBookingPlan()) {
            this.ws.preparePlanExecution().subscribe(
              () => {
                this.fillDynamicBookingPlan();
              }
            );
          } else {
            this.dynamicFrom.init(null);
          }
        } else {
          this.updateStepStatusValid();
        }
      }
    );
  }

  private fillCheckboxGroups() {
    log.info('fillCheckboxGroups');

    const timeOfDayItems: KeyValueItem[] = this.ws.timeOfDayItems; // we loaded this from backend
    const timeOfDayValues: KeyValueItem[] = JSON.parse(JSON.stringify(timeOfDayItems)); // clone

    const dayOfWeekItems: KeyValueItem[] = [];
    const dayOfWeekValues: KeyValueItem[] = [];

    const days = [ 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su' ];
    const values = [ 1, 2, 3, 4, 5, 6, 0 ];

    for (let i = 0; i < 7; i++) {
      const item: KeyValueItem = {
        label: days[i],
        value: String(values[i])
      };

      dayOfWeekItems.push(item);
      dayOfWeekValues.push(item);
    }

    this.dayOfWeekItems = dayOfWeekItems;
    this.dayOfWeekValues = dayOfWeekValues;
    this.timeOfDayItems = timeOfDayItems;
    this.timeOfDayValues = timeOfDayValues;
  }

  private fillDynamicBookingPlan() {
    log.info('fillDynamicBookingPlan');

    const items: FormItem[] = [];
    const kvpitems: { [kvpName: string]: KeyValueItem[] } = {};

    if (this.ws.planExecutions == null || this.ws.planExecutions === undefined || this.ws.planExecutions.length === 0) {
      return;
    }

    for (const plan of this.ws.planExecutions) {
      if (plan.attributeValues == null || plan.attributeValues === undefined || plan.attributeValues.length === 0) {
        continue;
      }

      for (const attributeValue of plan.attributeValues) {
        if (attributeValue.attributeDescriptor.attributeType == null || attributeValue.attributeDescriptor.attributeType === undefined) {
          continue;
        }

        if (attributeValue.attributeDescriptor.attributeType.attributeValueType === 'LocalDate') {
          items.push(
            {
              name: 'localdate_' + attributeValue.oid + '_' + attributeValue.attributeDescriptor.oid,
              type: FormType.Date,
              label: attributeValue.attributeDescriptor.name,
              constraints: {
                required: true
              },
              htmlId: 'dateOfBirth'
            }
          );
        } else if (attributeValue.attributeDescriptor.attributeType.attributeValueType === 'List') {
          const kvpName = 'list_' + attributeValue.oid + '_' + attributeValue.attributeDescriptor.oid;
          items.push(
            {
              name: 'list_' + attributeValue.oid + '_' + attributeValue.attributeDescriptor.oid,
              kvpName: 'list_' + attributeValue.oid + '_' + attributeValue.attributeDescriptor.oid,
              type: FormType.DropDown,
              label: attributeValue.attributeDescriptor.name,
              constraints: {
                required: true
              },
              htmlId: 'testlist'
            }
          );

          kvpitems[kvpName] = [];
          kvpitems[kvpName].push(
            {
              label: 'Select item...',
              value: null
            }
          );

          for (const item of attributeValue.attributeDescriptor.listAttributeValueOwner.attributeValues) {
            kvpitems[kvpName].push(
              {
                label: item.name,
                value: String(item.oid)
              }
            );
          }
        } else {
          log.error('not implemented attribute value type', attributeValue.attributeDescriptor.attributeType.attributeValueType);
        }
      }
    }

    items.push(
      {
        name: 'search',
        type: FormType.SubmitButton,
        label: 'Search'
      }
    );

    const desc: FormDescription = {
      name: 'bookingplan', // auto name HTMLIds with systematic names
      showFlat: false,
      items: items,
      kvpitems: kvpitems
    };

    if (this.dynamicFrom) {
      this.dynamicFrom.init(desc, this.searchWithPlan);
    }
  }

  private searchWithPlan (reason: string, valid: boolean, values: any, errors?: any) {
    if (valid) {
      log.info('from ok -  button', reason, values);
    } else {
      log.error('from error', reason, values, errors);
    }
  }

  private loadDynamicData() {
    let desc: FormDescription = {
      name: 'myform', // auto name HTMLIds with systematic names
      showFlat: true,
      items: [
        {
          name: 'mydate',
          type: FormType.Date,
          label: 'Date',
          parameters: {
            // placeholder: Date().match('\\d{4}-\\d{2}-\\d{2}'),
          },
          constraints: {
            // pattern: LOCALE_ID.toString()
          },
          htmlId: 'dateofbirth'
        },
        {
          name: 'dayofweek',
          kvpName: 'dayofweek',
          type: FormType.CheckBoxGroup,
          label: 'Day of Week',
          parameters: {
          },
          constraints: {
            required: true
          },
          htmlId: 'daysofweek'
        },
        {
          name: 'timeofday',
          kvpName: 'timeOfDay',
          type: FormType.CheckBoxGroup,
          label: 'Time of Day',
          value: [
            { value: '0' },
            { value: '1' },
            { value: '2' }
          ],
          parameters: {
          },
          constraints: {
            required: true
          },
          htmlId: 'timeofday'
        },
        {
          name: 'available',
          type: FormType.DropDown,
          kvpName: 'availableApps',
          label: 'Choose an available appointment',
          parameters: {
          },
          constraints: {
          },
          htmlId: 'availability'
        },
      ],
      kvpitems: {
        dayofweek: [
          {
            label: 'Sunday',
            value: '0',
            disabled: true,
            htmlId: 'sunday'
          },
          {
            label: 'Monday',
            value: '1',
            htmlId: 'monday'
          },
          {
            label: 'Tuesday',
            value: '2',
            htmlId: 'tuesday'
          },
          {
            label: 'Wednesday',
            value: '3',
            htmlId: 'wednesday'
          },
          {
            label: 'Thursday',
            value: '4',
            htmlId: 'thursday'
          },
          {
            label: 'Friday',
            value: '5',
            htmlId: 'friday'
          },
          {
            label: 'Saturday',
            value: '6',
            htmlId: 'saturday'
          },
          {
            label: 'Invisible Day',
            value: '-1',
            visible: false,
            htmlId: 'sunday'
          }
        ],
        timeOfDay: [
          {
            label: 'Morning',
            value: '0',
            disabled: true,
            htmlId: 'timeofdaymorning'
          },
          {
            label: 'Afternoon',
            value: '1',
            htmlId: 'timeofdayafternoon'
          },
          {
            label: 'Night',
            value: '2',
            htmlId: 'timeofdayevening'
          }
        ],
        availableApps: [
          {
            label: 'Select an appointment..',
            value: '0'
          },
          {
            label: '08:00',
            value: '08:00'
          },
          {
            label: '08:55',
            value: '08:55'
          },
          {
            label: '10:20',
            value: '10:20'
          },
          {
            label: '10:55',
            value: '10:55'
          },
          {
            label: '11:15',
            value: '11:15'
          },
          {
            label: '14:00',
            value: '14:00'
          },
          {
            label: '14:15',
            value: '14:15'
          },
          {
            label: '14:20',
            value: '14:20'
          },
          {
            label: '14:50',
            value: '14:50'
          },
          {
            label: '15:00',
            value: '15:00'
          },
          {
            label: '15:10',
            value: '15:10'
          },
          {
            label: '15:30',
            value: '15:30'
          }
        ]
      }
    };

    this.updateStepStatusValid();
    if (!this.ws.stepStatusValid) {
      desc = null;
    }
    if (this.dynamicFrom) {
      this.dynamicFrom.init(desc);
    }
  }

  private updateStepStatusValid() {
    log.info('updateStepStatusValid');
    let stepStatusValid = true;

    if ((this.ws.selectedPurposeValue === 'null' ||
      this.ws.selectedPurposeValue == null ||
      this.ws.selectedPurposeValue === undefined)) {
      stepStatusValid = false;
    }

    this.ws.stepStatusValid = stepStatusValid;
  }
}
