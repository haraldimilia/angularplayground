import { DateTimeService } from '../../core.lib';
import { DialogButtons, DialogDefaultButton, DialogService, KeyValueItem } from '../../core.ui';
import { Log } from 'ng2-logger/client';
import { Observable } from 'rxjs';
import { Stepbasic } from './stepbasic';
import { Config, PlanExecution, PortalSessionService, Purpose } from '../../wamp';

const log = Log.create('PortalPageComponent');

export class Workingset {
  private stepTransitions: string[] = [
    'step1', 'step2', 'step3', 'step4'
  ];
  date:   	      Date = new Date(); // TODO: fix me!
  // appDate:      Date;
  fName:          string;
  lName:          string;
  gender:         string;
  dob:            Date = new Date();
  plz:            string;
  ort:            string;
  insurance:      string;
  newEmail:       string;
  patientEmail:   string;
  userInputCode:  string;
  phone:          string;
  comment:        string;
  tandc =         false;
  account =       false;

  selectedPurposeValue: string = null;
  selectedPurpose: Purpose = null;
  selectedPurposeConfig: Config = null;
  purposeList: KeyValueItem[] = [];
  timeOfDayItems: KeyValueItem[] = [];
  planExecutions: PlanExecution[] = [];
  currentStep = this.stepTransitions[0]; // start step

  private _stepStatusValid = false;
  get stepStatusValid(): boolean {
    // log.info('get stepStatusValid');
    return this._stepStatusValid;
  }
  set stepStatusValid(value: boolean) {
    // log.info('set stepStatusValid', value);
    this._stepStatusValid = value;
  }

  private _stepBasic: Stepbasic;
  get stepBasic(): Stepbasic {
    // log.info('get stepBasic');
    return this._stepBasic;
  }
  set stepBasic(value: Stepbasic) {
    // log.info('set stepBasic', value);
    this._stepBasic = value;
  }

  constructor(private portalSessionService: PortalSessionService,
              private dateTimeService: DateTimeService,
              private dialogService: DialogService ) {
    log.info('constructor');
    this.patientEmail = '';
  }

  init() {
    log.info('init');

    this.purposeList = [];

    this.purposeList.push({
      label: 'Bitte eine Behandlung auswählen...',
      value: null,
      disabled: true
    });

    for (const purpose of this.portalSessionService.purposes) {
      this.purposeList.push({
        label: purpose.name,
        value: String(purpose.oid)
      });
    }
    this.selectedPurposeValue = this.purposeList[0].value;

    this.timeOfDayItems = [];
    for (const timeOfDayInterval of this.portalSessionService.timeOfDayIntervals) {
      this.timeOfDayItems.push({
        label: timeOfDayInterval.name,
        value: String(timeOfDayInterval.beginTime), // TODO: fix me
        description: this.dateTimeService.createIntervalCaptionText(timeOfDayInterval.beginTime, timeOfDayInterval.endTime)
      });
    }

  }

  selectPurpose(purposeOid: number): Observable<boolean> {
    let selectedPurpose: Purpose = null;
    for (const purpose of this.portalSessionService.purposes) {
      if (purpose.oid === purposeOid) {
        selectedPurpose = purpose;
        break;
      }
    }
    this.selectedPurpose = selectedPurpose;
    this.planExecutions = [];

    return new Observable<boolean>((observer) => {
      this.portalSessionService.selectPurpose(purposeOid).subscribe(
        (config: Config) => {
          this.selectedPurposeConfig = config;
          observer.next(true);
          observer.complete();
        },
        (err: any) => {
          log.error('selectPurpose', err);
          observer.error(err);
          observer.complete();
        }
      );
    });
  }

  preparePlanExecution(): Observable<boolean> {
    return new Observable<boolean>((observer) => {

      const planExecutions: PlanExecution[] = [];

      if (this.selectedPurpose.bookingPlans == null || this.selectedPurpose.bookingPlans.length === 0) {
        observer.next(true);
        observer.complete();
        return;
      }

      let numberOfPlans = 0;
      for (const bookingPlan of this.selectedPurpose.bookingPlans) {
        if (bookingPlan.planScripts == null || bookingPlan.planScripts.length === 0) {
          continue;
        }
        for (const planscript of bookingPlan.planScripts) {
          // count the plans
          numberOfPlans++;
        }
      }

      let currentPlans = 0;
      for (const bookingPlan of this.selectedPurpose.bookingPlans) {
        if (bookingPlan.planScripts == null || bookingPlan.planScripts.length === 0) {
          continue;
        }
        for (const planscript of bookingPlan.planScripts) {
          this.portalSessionService.preparePlanExecution(planscript.oid, this.selectedPurpose.oid).subscribe(
            (planExecution: PlanExecution) => {
              planExecutions.push(planExecution);
              currentPlans++;
              if (currentPlans === numberOfPlans) {
                // close obsever when we counted enough
                this.planExecutions = planExecutions;
                observer.next(true);
                observer.complete();
                return;
              }
            },
            (err: any) => {
              log.error('preparePlanExecution', err);
              observer.error(err);
              observer.complete();
            }
          );
        }
      }
    });
  }

  hasBookingPlan(): boolean {
    // check if purpose is selected
    if (this.selectedPurpose == null || this.selectedPurpose === undefined) {
      return false;
    }

    if (this.selectedPurpose.bookingPlans == null || this.selectedPurpose.bookingPlans === undefined) {
      return false;
    }

    // we need more then one item
    return this.selectedPurpose.bookingPlans.length > 0;
  }

  hasNoBookingPlan(): boolean {
    // check if purpose is selected
    if (this.selectedPurpose == null || this.selectedPurpose === undefined) {
      return false;
    }

    if (this.selectedPurpose.bookingPlans == null || this.selectedPurpose.bookingPlans === undefined) {
      return true;
    }

    return this.selectedPurpose.bookingPlans.length === 0;
  }

  onTransitionPrev() {
    log.info('onTransitionPrev');
    if (!this.stepBasic.onPrev()) {
      return;
    }

    let index = this.stepTransitions.indexOf(this.currentStep);
    if (index < 0) { // not found
      return;
    }
    index--;
    if (index > this.stepTransitions.length - 1) { // right side
      return;
    }
    if (index < 0) { // left side
      return;
    }

    const step = this.stepTransitions[index];
    this.currentStep = step;
  }

  onTransitionNext() {
    log.info('onTransitionNext');
    if (!this.stepBasic.onNext()) {
      return;
    }

    let index = this.stepTransitions.indexOf(this.currentStep);
    if (index < 0) { // not found
      return;
    }
    index++;
    if (index > this.stepTransitions.length - 1) { // right side
      return;
    }
    if (index < 0) { // left side
      return;
    }

    const step = this.stepTransitions[index];
    this.currentStep = step;
  }

  goBackToStep(stepName: any): any {
    log.info('goBackToStep', stepName);

    if (this.stepTransitions.indexOf(stepName) === -1) {
      // not found
      return;
    }
    const index = this.stepTransitions.indexOf(stepName);
    const currenIndex = this.stepTransitions.indexOf(this.currentStep);

    if (index >= currenIndex || index < 0) { // invalid
      return;
    }

    // head back
    const step = this.stepTransitions[index];
    this.currentStep = step;
  }

  stepFinished(step: string): boolean {
    const currentIndex = this.stepTransitions.indexOf(this.currentStep);
    if (currentIndex < 0) { // not found
      return false;
    }

    const index = this.stepTransitions.indexOf(step);
    if (index < 0) { // not found
      return false;
    }

    return index > currentIndex;
  }

  activeStep(step: string): boolean {
    const currentIndex = this.stepTransitions.indexOf(this.currentStep);
    if (currentIndex < 0) { // not found
      return false;
    }

    const index = this.stepTransitions.indexOf(step);
    if (index < 0) { // not found
      return false;
    }

    return index === currentIndex;
  }

}
