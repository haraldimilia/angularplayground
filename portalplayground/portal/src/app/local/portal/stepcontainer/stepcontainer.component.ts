import { Component, Input, OnInit } from '@angular/core';

import { Log } from 'ng2-logger/client';
import { Workingset } from '../workingset';

const log = Log.create('StepContainerComponent');

@Component({
  selector: 'app-stepcontainer',
  templateUrl: './stepcontainer.component.html',
  styleUrls: ['./stepcontainer.component.scss']
})
export class StepContainerComponent implements OnInit {
  @Input() ws: Workingset;

  constructor() {
    log.info('constructor');
  }

  ngOnInit() {
  }

  onPrev() {
    log.info('onPrev');
    this.ws.onTransitionPrev();
  }

  onNext() {
    log.info('onNext');
    this.ws.onTransitionNext();
  }
}
