import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';

import { PortalPageComponent } from './portalpage/portalpage.component';

const routes: Routes = [
  {
    path: '',
    component: PortalPageComponent,
    data: {
      title: 'Portal'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortalRoutingModule { }
