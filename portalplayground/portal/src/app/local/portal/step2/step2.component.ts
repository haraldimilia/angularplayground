import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormValidationService } from './../../../core.ui/validation/formvalidation.service';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EMAIL_VALIDATOR } from '@angular/forms/src/directives/validators';
import { DynamicFormComponent, FormDescription, FormType } from '../../../core.ui';
import { Log } from 'ng2-logger/client';
import { Stepbasic } from '../stepbasic';
import { Workingset } from '../workingset';

const log = Log.create('Step2Component');

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements AfterViewInit, Stepbasic {
  @Input() ws: Workingset = null;
  @ViewChild(DynamicFormComponent) dynamicFrom: DynamicFormComponent;
  verifyForm: FormDescription;
  error: string;
  showError = false;
  verificationCode: string;
  // tslint:disable-next-line:max-line-length
  EMAIL_PATTERN = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  emailSent = false;
  inputCode = false;
  incorrect = false;
  correct = false;
  codeAccepted = false;
  emailError = false;
  stepStatusValid = false;
  formGroup: FormGroup;
  alreadyVerified = false;

  constructor(private formBuilder: FormBuilder,
              private formValidationService: FormValidationService) {
    log.info('constructor');
  }

  ngAfterViewInit() {
    log.info('ngAfterViewInit');
    this.ws.stepBasic = this;
    this.onInit();
    this.ws.stepStatusValid = this.stepStatusValid;
  }

  onInit() {
    log.info('onInit');
    this.checkEmail();
    this.updateStepStatusValid();
    this.error = null;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    log.info('ngOnInit');
    this.formGroup = this.formBuilder.group({
      formEmail: ['', [Validators.required, Validators.email]]
    });
  }


  onNext(): boolean {
    log.info('onNext');
    return true;
  }

  onPrev(): boolean {
    log.info('onPrev');
    return true;
  }

  userInput(data) {
    console.log('CHECKING ' + this.verificationCode);
    this.inputCode = true;
    if (data === this.verificationCode) {
      this.correct = true;
      this.incorrect = false;
      this.error = null;
      this.codeAccepted = true;
    } else {
      this.incorrect = true;
      this.correct = false;
      this.codeAccepted = false;
      this.error = data + ' is incorrect';
    }
  }

  private updateStepStatusValid() {
    this.ws.tandc = !this.ws.tandc;
    log.info('updateStepStatusValid');
    if ( this.ws.tandc === true) {
      this.stepStatusValid = true;
    } else {
      this.stepStatusValid = false;
    }
    this.ws.stepStatusValid = this.stepStatusValid;
  }

  generateCode(email) {
    if (this.EMAIL_PATTERN.test(email)) {
      this.emailSent = true;
      const random = (Math.random() * 100000).toString();
      this.verificationCode = random.substring(0, 5);
      console.log('GENERATED ' + this.verificationCode);
      this.emailError = false;
      this.error = '';
    } else {
      this.emailError = true;
      this.error = 'email does not match the required pattern';
    }
  }

  checkEmail() {
    console.log('checkEmail');
    if (this.ws.newEmail === this.ws.patientEmail && this.ws.newEmail !== null && this.ws.newEmail !== undefined) {
      this.alreadyVerified = true;
      this.inputCode = false;
    } else {
      this.alreadyVerified = false;
      this.codeAccepted = false;
      this.ws.tandc = false;
      this.ws.account = false;
      this.ws.patientEmail = this.ws.newEmail;
      this.ws.userInputCode = null;
      this.generateCode(this.ws.patientEmail);
    }
  }
}

