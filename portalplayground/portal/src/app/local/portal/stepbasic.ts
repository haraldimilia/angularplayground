import { Workingset } from './workingset';

export interface Stepbasic {
  ws: Workingset;
  onInit();
  onNext(): boolean;
  onPrev(): boolean;
}
