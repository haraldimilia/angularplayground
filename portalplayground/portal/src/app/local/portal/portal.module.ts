import { CommonModule } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { ContainerModule, DialogModule, DynamicFormModule, FormModule } from '../../core.ui';
import { PortalRoutingModule } from './portal.routing';
import { PortalPageComponent } from './portalpage/portalpage.component';
import { ProcessIndicatorComponent } from './processindicator/processindicator.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { Step4Component } from './step4/step4.component';
import { StepContainerComponent } from './stepcontainer/stepcontainer.component';
import { WampPortalModule } from '../../wamp';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PortalRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    DynamicFormModule,
    ContainerModule,
    FormModule,
    WampPortalModule,
    DialogModule
  ],
  declarations: [
    PortalPageComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component,
    StepContainerComponent,
    ProcessIndicatorComponent,
  ],
})
export class PortalModule { }
