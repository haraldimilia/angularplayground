
import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { DynamicFormComponent, FormDescription, FormType } from '../../../core.ui';
import { DateTimeService } from './../../../core.lib/datetime/datetime.service';
import { Log } from 'ng2-logger/client';
import { Stepbasic } from '../stepbasic';
import { Workingset } from '../workingset';

const log = Log.create('Step4Component');

@Component({
  selector: 'app-step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.scss']
})
export class Step4Component implements AfterViewInit, Stepbasic {
  @Input() ws: Workingset = null;
  @ViewChild(DynamicFormComponent) dynamicFrom: DynamicFormComponent;
   clinic: string;
   stepStatusValid = false;

  constructor(private dateTime: DateTimeService) {
    log.info('constructor');
    this.clinic = 'Klinik Wildau';
    // console.log(this.locale);
  }

  ngAfterViewInit() {
    log.info('ngAfterViewInit');
    this.ws.stepBasic = this;
    this.ws.stepStatusValid = this.stepStatusValid;
    this.onInit();
  }

  onInit() {
    log.info('onInit');
    this.onCreateDynamicItems();
  }

  onNext(): boolean {
    log.info('onNext');
    return true;
  }

  onPrev(): boolean {
    log.info('onPrev');
    return true;
  }

  public onCreateDynamicItems() {
    const timepickerValue = new Date();
    timepickerValue.setHours(13, 0, 0, 0);

    const appDetails: FormDescription = {
      name: 'myform', // auto name HTMLIds with systematic names
      showFlat: false,
      items: [
        {
          name: 'day',
          type: FormType.Text,
          label: 'Am:',
          // value: this.ws.date.toLocaleDateString(this.dateTime.locale),
          // call to locale variable works, but uses en-US date format because of my language settings
          value: this.ws.date.toLocaleDateString('de-DE'),
          static: true
        },
        {
          name: 'time',
          type: FormType.Text,
          label: 'Um:',
          value: 'this.time',
          static: true
        },
        {
          name: 'clinic',
          type: FormType.Text,
          label: 'Praxis',
          value: this.clinic,
          static: true
        },
        {
          name: 'purpose',
          type: FormType.Text,
          label: 'Behandlung:',
          value: this.ws.selectedPurpose.name,
          static: true
        },
        {
          name: 'name',
          type: FormType.Text,
          label: 'Name:',
          value: this.ws.fName + ' ' + this.ws.lName,
          static: true
        },
        {
          name: 'dob',
          type: FormType.Text,
          label: 'Geburtsdatum:',
          value: this.ws.dob.toLocaleDateString('de-DE'),
          // value: this.ws.dob.toLocaleDateString(this.dateTime.locale),
          // call to locale variable works, but uses en-US date format because of my language settings
          static: true
        },
        {
          name: 'email',
          type: FormType.Text,
          label: 'Email:',
          value: this.ws.patientEmail,
          static: true
        },
        {
          name: 'phone',
          type: FormType.Text,
          label: 'Telephonnummer:',
          value: this.ws.phone,
          static: true
        },
        {
          name: 'comments',
          type: FormType.Text,
          label: 'Kommentar:',
          value: this.ws.comment,
          static: true
        },
        {
          name: 'finish',
          type: FormType.Button,
          label: 'Danke',
        },
        {
          name: 'download',
          type: FormType.Button,
          label: 'Download PDF',
        },
       ]
    };

    this.dynamicFrom.init(appDetails, (reason: string, valid: boolean, values: any, errors?: any) => {
      this.callback(reason, valid, values, errors);
    });
  }

  public callback (reason: string, valid: boolean, values: any, errors?: any) {
      if (valid) {
        switch (reason) {
          case 'finish': {

            this.ws.date = new Date();
            this.ws.fName = null;
            this.ws.lName = null;
            this.ws.gender = null;
            this.ws.dob = null;
            this.ws.plz = null;
            this.ws.ort = null;
            this.ws.insurance = null;
            this.ws.patientEmail = null;
            this.ws.phone = null;
            this.ws.comment = null;
            this.ws.goBackToStep('step1');
            break;
          }
        }

        console.log('from ok -  button', reason, values);
      } else {
        console.error('from error', reason, values, errors);
      }
    }
}
