import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DateTimeService } from '../../../core.lib';
import { DialogButtons, DialogDefaultButton, DialogService } from '../../../core.ui';
import { DialogIcons } from '../../../core.ui/dialog/dialogicons';
import { Log } from 'ng2-logger/client';
import { PortalSessionService } from '../../../wamp';
import { Workingset } from '../workingset';

const log = Log.create('PortalPageComponent');

@Component({
  selector: 'app-portalpage',
  templateUrl: './portalpage.component.html',
  styleUrls: ['./portalpage.component.scss']
})
export class PortalPageComponent implements OnInit {
  ws: Workingset = null;
  serviceIsRunning = false;
  icon: any = 'f104';

  constructor(private activatedRoute: ActivatedRoute,
    private portalSessionService: PortalSessionService,
    private dateTimeService: DateTimeService, private dialogService: DialogService) {
    log.info('constructor');
  }

  ngOnInit() {
    log.info('ngOnInit');

    let uuid = this.activatedRoute.snapshot.paramMap.get('uuid');
    if (!uuid) {
      uuid = '';
    }

    this.serviceIsRunning = true;
    this.portalSessionService.portalLogin(uuid).subscribe(
      (ok) => {
        if (!ok) {
          this.serviceIsRunning = false;
          return;
        }

        // we could do a login
        // now load the base data
        // please note: it's a not so well design in the API that this requires multplie ping-pongs
        // we try hiding that here

        this.portalSessionService.loadBaseData()
        .subscribe(
          (data: boolean) => {
            this.serviceIsRunning = false;
            if (!data) {
              log.error('loadBaseData failed');
              return;
            }

            log.info('purpose list', this.portalSessionService.purposes);
            this.ws = new Workingset(this.portalSessionService, this.dateTimeService, this.dialogService);
            this.ws.init();
          },
          (err: any) => {
            log.error('loadBaseData error', err);
            this.serviceIsRunning = false;
          }
        );
      },
      (err) => {
        this.serviceIsRunning = false;
        log.error('portalLogin', err);
        console.log('TRIGGER DIALOG ERROR BOX');

        const dialog = this.dialogService.showText(
          err, 'Error',
          // tslint:disable-next-line:max-line-length
          DialogButtons.RetryCancel, DialogDefaultButton.Button1, DialogIcons.Warning, { hasCloseButton: false, disableCloseByBackdropOrESC: true });
          dialog.onClosed = () => {
            log.info('dialog closed', dialog.dialogResult);
          };
      }
    );


  }
}
