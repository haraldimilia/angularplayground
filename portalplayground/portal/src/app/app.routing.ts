import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { environment } from '../environments/environment';

// Import Containers
import { DefaultLayoutComponent } from './local/layout/defaultlayout/defaultlayout.component';

import { Error404PageComponent } from './local/error404page/error404page.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo:  environment.production ? '404' : 'home/96944e36-56a3-41fc-8a5e-fc28201613eb', // developer hack
    pathMatch: 'full',
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Portal'
    },
    children: [
      {
        path: 'home/:uuid',
        loadChildren: './local/portal/portal.module#PortalModule'
      },
    ]
  },
  {
    path: '404',
    component: DefaultLayoutComponent,
    data: {
      title: 'Portal'
    },
    children: [
      {
        path: '',
        component: Error404PageComponent,
        data: {
          title: 'Page not found'
        }
      }
    ]
  },
  {
    path: '**', // anything else
    component: DefaultLayoutComponent,
    data: {
      title: 'Portal'
    },
    children: [
      {
        path: '',
        component: Error404PageComponent,
        data: {
          title: 'Page not found'
        }
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
