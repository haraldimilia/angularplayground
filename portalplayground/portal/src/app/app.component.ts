import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { NotificationService } from './core.ui';

import { Log } from 'ng2-logger/client';

const log = Log.create('AppComponent');

@Component({
  // tslint:disable-next-line
  selector: 'body', // coreui theme needs this
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private router: Router, private notificationService: NotificationService) {
    log.info('constructor');
  }

  ngOnInit() {
    log.info('ngOnInit');
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      log.info('window.scrollTo(0, 0)');
      window.scrollTo(0, 0);

      log.info('clearing toasts');
      this.notificationService.clearAllToasts();
    });
  }
}
