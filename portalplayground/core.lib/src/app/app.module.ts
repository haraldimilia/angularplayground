import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { LoggerConfigModule } from './core.lib';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    LoggerConfigModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
