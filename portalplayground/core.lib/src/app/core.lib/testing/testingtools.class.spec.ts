/**
 * this class is only used in spec files
 */
export class TestingTools {

  /**
   * create a random string for unittests
   * @param length optional length - default is 32
   */
  public static randomStringWithLength(length?: number): string {
    // https://www.mediacollege.com/internet/javascript/number/random.html
    const DEFAULT_LENGTH = 32;
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    if (length == null || length === undefined || length < 0) {
      length = DEFAULT_LENGTH;
    }
    let randomstring = '';
    for (let i = 0; i < length; i++) {
      const rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
  }

  /**
   * create a random string for unittests (16 to avoid issues with ngModel length in the DOM)
   */
  public static randomString(): string {
    // NOTE: for some reasons the browser dom of angular doesnt reflect the full string
    // thats maybe some optimization - so limit the key length here to 16
    // this is _NOT_ an issue...
    return TestingTools.randomStringWithLength(16);
  }
}
