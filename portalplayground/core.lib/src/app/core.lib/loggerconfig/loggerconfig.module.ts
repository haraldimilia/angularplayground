import { Log } from 'ng2-logger/client';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

const log = Log.create('LoggerConfigModule');

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class LoggerConfigModule {
  private static instance: LoggerConfigModule = null;

  constructor() {
    // https://stackoverflow.com/questions/36198785/how-do-i-create-a-singleton-service-in-angular-2
    // force this as singleton
    if (LoggerConfigModule.instance) {
      return LoggerConfigModule.instance;
    }

    // read this here and decide for production
    // https://www.npmjs.com/package/ng2-logger

    log.info('constructor');

    return this;
  }
}
