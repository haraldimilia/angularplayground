import { inject, TestBed } from '@angular/core/testing';

import { EncryptionService } from './encryption.service';

import { TestingTools } from '../testing/testingtools.class.spec';

describe('EncryptionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EncryptionService]
    });
  });

  it('should be created', inject([EncryptionService], (service: EncryptionService) => {
    expect(service).toBeTruthy();
  }));

  it('should hash a value', inject([EncryptionService], (service: EncryptionService) => {
    const inputValue = TestingTools.randomString();
    const outputValue = service.hash(inputValue);

    expect(outputValue).not.toBeNull();
    expect(outputValue).not.toBeUndefined();
  }));

  it('should vaidate a hash value', inject([EncryptionService], (service: EncryptionService) => {
    const inputValue = TestingTools.randomString();
    const outputValue = service.hash(inputValue);

    expect(outputValue).not.toEqual(inputValue); // hash needs to be something complete different then the input
    expect(service.compare(inputValue, outputValue)).toBeTruthy();
  }));

  it('should fail validating a bad hash value', inject([EncryptionService], (service: EncryptionService) => {
    const inputValue = TestingTools.randomString();
    const badHash = TestingTools.randomString();

    expect(service.compare(inputValue, badHash)).toBeFalsy();
  }));

});
