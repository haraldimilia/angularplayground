import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { EncryptionService } from './encryption.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    EncryptionService
  ]
})
export class EncryptionModule { }
