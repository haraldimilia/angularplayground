import { Injectable } from '@angular/core';
import * as bcryptjs from 'bcryptjs';

@Injectable({
  providedIn: 'root'
})
export class EncryptionService {

  constructor() { }

  /**
   * decodes the PIN code from the service
   * @param code code from server
   * @param hashValue hash from server
   */
  compare(code: any, hashValue: string): boolean {
    // https://www.npmjs.com/package/bcryptjs
    return bcryptjs.compareSync(code, hashValue);
  }

  /**
   * hashes a given value
   * @param value string to hash
   */
  hash(value: any): string {
    // https://www.npmjs.com/package/bcryptjs
    return bcryptjs.hashSync(value);
  }
}
