import { inject, TestBed } from '@angular/core/testing';

import { DateTimeService } from './datetime.service';

describe('DatetimeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DateTimeService]
    });
  });

  it('should be created', inject([DateTimeService], (service: DateTimeService) => {
    expect(service).toBeTruthy();
  }));

  it('should return the browsers localtime', inject([DateTimeService], (service: DateTimeService) => {
    const browserLocalTime = service.browserLocalTime();
    expect(browserLocalTime).not.toBeNull();
    expect(browserLocalTime).not.toBeUndefined();
    expect(browserLocalTime instanceof Date).toBeTruthy();
  }));
});
