import { CommonModule } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';

import { DateTimeService } from './datetime.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    DateTimeService,
    {provide: LOCALE_ID, useValue: 'de-DE'}
  ]
})
export class DateTimeModule { }
