import { Injectable } from '@angular/core';

import { DateTime } from 'luxon';

// we use luxon not moments
// https://moment.github.io/luxon/

@Injectable({
  providedIn: 'root'
})
export class DateTimeService {
  locale = window.navigator.language;

  constructor() { }

  /**
   * gets the local time of the browser
   *
   * (this is in general the most uninteressting information you can ever have)
   */
  browserLocalTime(): any {
    const result = DateTime.local().toJSDate();
    return result;
  }


  /**
   * creates a caption text for a given interval text e.g "12:00 - 18:00"
   * this is currently used for the time of day picker
   *
   * this is depending on the current browser locals / i18n settings
   *
   * @param beginTime
   * @param endTime
   */
  createIntervalCaptionText(beginTime: any, endTime: any): string {
    // todo parameter type not clear
    return 'xxx-xxx'; // TODO implement this
  }


}
