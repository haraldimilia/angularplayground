import { Component } from '@angular/core';
import { Log } from 'ng2-logger/client';

const log = Log.create('AppComponent');

@Component({
  // tslint:disable-next-line
  selector: 'body', // coreui theme needs this
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor() {
    log.info('constructor');
  }
}
