// This file is required by karma.conf.js and loads recursively all the .spec and framework files

// have to import this before we import testing from angular - this is some Jasmin 3.x issue
/* tslint:disable:ordered-imports */
import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/proxy.js';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/jasmine-patch';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';
/* tslint:enable:ordered-imports */

import { Log } from 'ng2-logger/client';
Log.setProductionMode();
Log.onlyModules(); // disable everything

import { getTestBed } from '@angular/core/testing';
import {
  platformBrowserDynamicTesting,
  BrowserDynamicTestingModule
} from '@angular/platform-browser-dynamic/testing';
// import 'zone.js/dist/zone-testing';

declare const require: any;

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting()
);
// we only do tests in specific directories - find the tests.
const context = require.context('app/core.lib', true, /\.spec\.ts$/);
// And load the modules.
context.keys().map(context);
