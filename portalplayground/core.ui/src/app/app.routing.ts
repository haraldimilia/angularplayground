import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './local/layout/defaultlayout/defaultlayout.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './local/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'containertest',
        loadChildren: './local/containertest/containertest.module#ContainerTestModule'
      },
      {
        path: 'formtest',
        loadChildren: './local/formtest/formtest.module#FormTestModule'
      },
      {
        path: 'validationtest',
        loadChildren: './local/validationtest/validationtest.module#ValidationTestModule'
      },
      {
        path: 'dynamicformtest',
        loadChildren: './local/dynamicformtest/dynamicformtest.module#DynamicFormTestModule'
      },
      {
        path: 'notificationtest',
        loadChildren: './local/notificationtest/notificationtest.module#NotificationTestModule'
      },
      {
        path: 'dialogtest',
        loadChildren: './local/dialogtest/dialogtest.module#DialogTestModule'
      },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
