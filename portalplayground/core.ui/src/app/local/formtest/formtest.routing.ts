import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';

import { FormPageComponent } from './formpage/formpage.component';

const routes: Routes = [
  {
    path: '',
    component: FormPageComponent,
    data: {
      title: 'Form Test'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormTestRoutingModule { }
