import { Component } from '@angular/core';
import { KeyValueItem } from '../../../core.ui';

@Component({
  selector: 'app-form-page',
  templateUrl: './formpage.component.html',
  styleUrls: ['./formpage.component.scss']
})
export class FormPageComponent {
  label: string;
  disabled = false;
  static = false;
  horizontal = true;

  // text input
  textInput = 'This is my great text.';
  textInputNumericMask = 100; // can be numeric in numericMode
  passwortTextInput = 'secret';
  helpTextInput = 'help text input';
  placeholderTextInput = '';
  disabledTextInput = 'I am disabled.';
  staticTextInput = 'I am static.';
  textSizeLargeTextInput = 'Large text';
  textSizeDefaultTextInput = 'Default text';
  textSizeSmallTextInput = 'Small text';

  // datepicker
  datepickerValue: Date;
  // timepicker
  timepickerValue: Date;

  // checkbox
  checkBox = true;

  // checkbox group
  checkboxGropItems: KeyValueItem[] = [
    {
      label: 'Sunday',
      value: '0'
    },
    {
      label: 'Monday',
      value: '1'
    },
    {
      label: 'Tuesday',
      value: '2'
    },
    {
      label: 'Wednesday',
      value: '3'
    },
    {
      label: 'Thursday',
      value: '4'
    },
    {
      label: 'Friday',
      value: '5'
    },
    {
      label: 'Saturday',
      value: '6'
    },
    {
      label: 'Invisible Day',
      value: '-1',
      visible: false
    }
  ];

  checkboxGropValues: KeyValueItem[] = [
    this.checkboxGropItems[1],
    this.checkboxGropItems[4],
    this.checkboxGropItems[5],
  ];

  // radio
  radioItems: KeyValueItem[] = [
    {
      label: 'Option one',
      value: 'option1'
    },
    {
      label: 'Option two (preselected at beginning)',
      value: 'option2'
    },
    {
      label: 'Option three is disabled',
      value: 'option3',
      disabled: true
    }
  ];

  radioValue: string = this.radioItems[1].value;

  // dropdown
  dropdownItems: KeyValueItem[] = [
    {
      label: 'Select item...',
      value: null
    },
    {
      label: 'Label 1',
      value: '1'
    },
    {
      label: 'Label 2 disabled',
      value: '2',
      disabled: true
    },
    {
      label: 'Label 3 invisible',
      value: '3',
      visible: true
    },
    {
      label: 'Label 4',
      value: '4'
    },
    {
      label: 'Label 5',
      value: '5'
    }
  ];

  dropdownValue: string = this.dropdownItems[4].value;

  constructor() {
    let date = new Date();
    date.setHours(0, 0, 0, 0);
    this.datepickerValue = date;

    date = new Date();
    date.setHours(13, 0, 0, 0);
    this.timepickerValue = date;
  }

  toggleDisabled() {
    this.disabled = !this.disabled;
  }

  toggleStatic() {
    this.static = !this.static;
  }

  toogleHorizontal() {
    this.horizontal = !this.horizontal;
  }

  onGoButton(value: string) {
    console.log('go!');
  }
}
