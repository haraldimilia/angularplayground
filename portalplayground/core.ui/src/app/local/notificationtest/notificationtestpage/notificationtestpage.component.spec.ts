import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationTestPageComponent } from './notificationtestpage.component';

describe('NotificationTestPageComponent', () => {
  let component: NotificationTestPageComponent;
  let fixture: ComponentFixture<NotificationTestPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationTestPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationTestPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
