import { Component } from '@angular/core';

import { NotificationService } from '../../../core.ui';

import { Log } from 'ng2-logger/client';

const log = Log.create('NotificationTestPageComponent');


@Component({
  selector: 'app-notificationtestpage',
  templateUrl: './notificationtestpage.component.html',
  styleUrls: ['./notificationtestpage.component.scss']
})
export class NotificationTestPageComponent {


  constructor(private notificationService: NotificationService) {
    log.info('constructor');
  }

  onShowToast() {
    log.info('onShowToast');
    this.notificationService.showToast('hello from showToast()');
  }

  onShowSuccessToast() {
    log.info('onShowSuccessToast');
    this.notificationService.successToast('hello from successToast()');
  }

  onShowErrorToast() {
    log.info('onShowErrorToast');
    this.notificationService.errorToast('hello from errorToast()');
  }

  onShowInfoToast() {
    log.info('onShowInfoToast');
    this.notificationService.infoToast('hello from infoToast()');
  }

  onShowWarningToast() {
    log.info('onShowWarningToast');
    this.notificationService.warningToast('hello from warningToast()');
  }

}
