import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';

import { NotificationTestPageComponent } from './notificationtestpage/notificationtestpage.component';

const routes: Routes = [
  {
    path: '',
    component: NotificationTestPageComponent,
    data: {
      title: 'Notification Test'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationTestRoutingModule { }
