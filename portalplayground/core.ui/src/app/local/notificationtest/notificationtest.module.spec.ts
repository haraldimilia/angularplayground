import { NotificationTestModule } from './notificationtest.module';

describe('NotificationTestModule', () => {
  let notificationTestModule: NotificationTestModule;

  beforeEach(() => {
    notificationTestModule = new NotificationTestModule();
  });

  it('should create an instance', () => {
    expect(notificationTestModule).toBeTruthy();
  });
});
