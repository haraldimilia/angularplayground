import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { NotificationTestRoutingModule } from './notificationtest.routing';
import { NotificationTestPageComponent } from './notificationtestpage/notificationtestpage.component';

import { NotificationModule } from './../../core.ui';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NotificationTestRoutingModule,
    BsDropdownModule.forRoot(),
    ButtonsModule.forRoot(),
    NotificationModule
  ],
  declarations: [NotificationTestPageComponent]
})
export class NotificationTestModule { }
