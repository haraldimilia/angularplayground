import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';

import { DynamicFormPageComponent } from './dynamicformpage/dynamicformpage.component';

const routes: Routes = [
  {
    path: '',
    component: DynamicFormPageComponent,
    data: {
      title: 'Dynamic Form Test'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DynamicFormTestRoutingModule { }
