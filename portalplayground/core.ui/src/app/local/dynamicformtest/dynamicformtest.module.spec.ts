import { DynamicFormTestModule } from './dynamicformtest.module';

describe('DynamicFormTestModule', () => {
  let dynamicFormTestModule: DynamicFormTestModule;

  beforeEach(() => {
    dynamicFormTestModule = new DynamicFormTestModule();
  });

  it('should create an instance', () => {
    expect(dynamicFormTestModule).toBeTruthy();
  });
});
