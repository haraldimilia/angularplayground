import { Component, OnInit, ViewChild } from '@angular/core';
import {
  DynamicFormComponent, DynamicFormHandler, FormDescription,
  FormItem,
  FormType
} from '../../../core.ui';

@Component({
  selector: 'app-dynamicformpage',
  templateUrl: './dynamicformpage.component.html',
  styleUrls: ['./dynamicformpage.component.scss']
})
export class DynamicFormPageComponent implements OnInit {
  @ViewChild(DynamicFormComponent)
  dynamicFrom: DynamicFormComponent;
  showFlat = false;
  desc: FormDescription;

  constructor() { }

  ngOnInit() {
  }

  toogleFlatView() {
    this.showFlat = !this.showFlat;
  }

  onCreateDynamicItems() {
    const timepickerValue = new Date();
    timepickerValue.setHours(13, 0, 0, 0);

    const desc: FormDescription = {
      name: 'myform', // auto name HTMLIds with systematic names
      showFlat: this.showFlat,
      cardLabel: 'My Card Caption',
      items: [
        {
          name: 'static',
          value: 'My static text with lots of letters',
          type: FormType.Text,
          label: 'Static text',
          static: true
        },
        {
          name: 'testdisabled',
          type: FormType.Text,
          label: 'Text Disabled',
          disabled: true,
          parameters: {
            placeholder: 'my disabled placeholder'
          }
        },
        {
          name: 'name',
          type: FormType.Text,
          htmlId: 'customId', // overwrite ids if you want
          label: 'Name',
          parameters: {
            placeholder: 'please enter the name'
          },
          constraints: {
            required: true
          }
        },
        {
          name: 'firstname',
          type: FormType.Text,
          label: 'Firstname',
          parameters: {
            placeholder: 'please enter the first name'
          }
        },
        {
          name: 'register',
          type: FormType.CheckBox,
          label: 'Register this email',
          help: 'This needs to be enabled to receive any mails.',
          value: true,
          parameters: {
            caption: 'Enable to register your email. (Checked by default via value)'
          }
        },
        {
          name: 'dayofweek',
          kvpName: 'dayofweek',
          type: FormType.CheckBoxGroup,
          label: 'Day of Week',
          value: [
            { value: '2' },
            { value: '3' },
            { value: '4' }
          ],
          parameters: {
          },
          constraints: {
            required: true
          }
        },
        {
          name: 'mydate',
          type: FormType.Date,
          label: 'Date',
          parameters: {
          },
          constraints: {
          }
        },
        {
          name: 'dropdown',
          kvpName: 'dropdown',
          type: FormType.DropDown,
          label: 'Drop Down',
          value: '2',
          parameters: {
          }
        },
        {
          name: 'radio',
          kvpName: 'radio',
          type: FormType.Radio,
          label: 'Radio',
          help: 'This radio buttons are awesome.',
          value: 'option2',
          parameters: {
          }
        },
        {
          name: 'mytime',
          type: FormType.Time,
          label: 'Time',
          // value: timepickerValue,
          parameters: {
          },
          constraints: {
            // required: true
          }
        },
        {
          name: 'next',
          type: FormType.SubmitButton,
          label: 'Next'
        },
        {
          name: 'back',
          type: FormType.Button,
          label: 'Back'
        }
      ],
      kvpitems: {
        dayofweek: [
          {
            label: 'Sunday',
            value: '0',
            disabled: true
          },
          {
            label: 'Monday',
            value: '1'
          },
          {
            label: 'Tuesday',
            value: '2'
          },
          {
            label: 'Wednesday',
            value: '3'
          },
          {
            label: 'Thursday',
            value: '4'
          },
          {
            label: 'Friday',
            value: '5'
          },
          {
            label: 'Saturday',
            value: '6'
          },
          {
            label: 'Invisible Day',
            value: '-1',
            visible: false
          }
        ],
        dropdown: [
          {
            label: 'Select item...',
            value: null
          },
          {
            label: 'Label 1',
            value: '1'
          },
          {
            label: 'Label 2 disabled',
            value: '2',
            disabled: true
          },
          {
            label: 'Label 3 invisible',
            value: '3',
            visible: true
          },
          {
            label: 'Label 4',
            value: '4'
          },
          {
            label: 'Label 5',
            value: '5'
          }
        ],
        radio: [
          {
            label: 'Option one',
            value: 'option1'
          },
          {
            label: 'Option two (preselected at beginning)',
            value: 'option2'
          },
          {
            label: 'Option three is disabled',
            value: 'option3',
            disabled: true
          }
        ],
      }
    };
    this.desc = desc;
    this.dynamicFrom.init(this.desc, this.callback);
  }

  cleanForm() {
    this.desc = null;
    this.dynamicFrom.init(this.desc, null);
  }

  private callback (reason: string, valid: boolean, values: any, errors?: any) {
    if (valid) {
      console.log('from ok -  button', reason, values);
    } else {
      console.error('from error', reason, values, errors);
    }
  }

}
