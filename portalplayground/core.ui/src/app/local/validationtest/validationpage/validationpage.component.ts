import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { FieldError, FormValidationService, KeyValueItem, ValidationError } from '../../../core.ui';

import { Log } from 'ng2-logger/client';

const log = Log.create('ValidationPageComponent');


@Component({
  selector: 'app-validation-page',
  templateUrl: './validationpage.component.html',
  styleUrls: ['./validationpage.component.scss']
})
export class ValidationPageComponent implements OnInit {
  errorJsonString: any = '';
  disabled = false;
  static = false;

  errors: ValidationError = {
    validationErrorMessage: null,
    fieldErrors: {}
  };

  formGroup: FormGroup;
  radioItems: KeyValueItem[] = [
    {
      label: 'Option one is this and that—be sure to include why it#s great',
      value: 'option1'
    },
    {
      label: 'Option two can be something else and selecting it will deselect option one',
      value: 'option2'
    },
    {
      label: 'Option three is disabled',
      value: 'option3',
      disabled: true
    }
  ];

  dropdownItems: KeyValueItem[] = [
    {
      label: 'Select item...',
      value: null
    },
    {
      label: 'Label 1',
      value: '1'
    },
    {
      label: 'Label 2',
      value: '2'
    },
    {
      label: 'Label 3',
      value: '3',
      visible: false
    },
    {
      label: 'Label 4',
      value: '4'
    },
    {
      label: 'Label 5',
      value: '5'
    }
  ];

  constructor(private formBuilder: FormBuilder,
    private formValidationService: FormValidationService) {
    log.info('constructor');
  }

  // change the formMinText / formMaxText titles to something more logical
  ngOnInit() {
    log.i('ngOnInit');
    this.formGroup = this.formBuilder.group({
      formText: ['', Validators.required],
      formMin: ['4', Validators.min(5)],
      formMax: ['4', Validators.max(8)],
      formMinLength: ['abc', Validators.minLength(10)],
      formMaxLength: ['abc', Validators.maxLength(2)],
      formEmail: ['', [Validators.required, Validators.email]],
      formPattern: ['INVALID', [Validators.required, Validators.pattern('^[a-z0-9_-]')]],
      formCheckbox: ['', Validators.required],
      formRadio: ['', Validators.required],
      formDropDownList: ['', Validators.required],
    });
    // this.formGroup.controls['formText'].setValue('Text');
    this.formGroup.controls['formCheckbox'].setValue(true);
    this.formGroup.controls['formRadio'].setValue('option2');
    this.formGroup.controls['formDropDownList'].setValue('4');
  }

  toggleDisabled() {
    log.i('toggleDisabled');
    this.disabled = !this.disabled;
  }

  toggleStatic() {
    log.i('toggleStatic');
    this.static = !this.static;
  }

  createOneError() {
    log.i('createOneError');
    const err = ['Error Text'];
    this.createValidationErrors(err);
  }

  createMannyErrors() {
    log.i('createMannyErrors');
    const err = ['Error One', 'Error Two', 'Error Three'];
    this.createValidationErrors(err);
  }

  removeErrors() {
    log.i('removeErrors');
    const err = null;
    this.createValidationErrors(err);
  }

  validateForm() {
    this.errorJsonString = '';
    if (!this.formGroup.valid) {

      // https://stackoverflow.com/questions/40680321/get-all-validation-errors-from-angular-2-formgroup
      const getErrors = (formGroup: FormGroup, errors: any = {}) => {
        Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
            errors[field] = control.errors;
          } else if (control instanceof FormGroup) {
            errors[field] = getErrors(control);
          }
        });
        return errors;
      };

      // Calling it:
      this.errorJsonString = getErrors(this.formGroup);

      return;
    }

    log.info('form values', this.formGroup.value);
    this.errorJsonString = {
      ok: true
    };
  }

  private createValidationErrors(errors: Array<string>) {
    this.errors = {
      validationErrorMessage: null,
      fieldErrors: {}
    };

    if (errors != null) {
      this.errors.validationErrorMessage = 'Global summary message';
    }

    for (const key in this.formGroup.controls) {
      if (!this.formGroup.controls.hasOwnProperty(key)) {
        continue;
      }
      if (errors != null) {
        this.errors.fieldErrors[key] = errors;
      }
    }

    this.formValidationService.applyFieldErrorToFrom(this.formGroup, this.errors);
  }

}
