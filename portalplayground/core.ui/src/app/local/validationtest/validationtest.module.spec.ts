import { ValidationTestModule } from './validationtest.module';

describe('ValidationTestModule', () => {
  let validationTestModule: ValidationTestModule;

  beforeEach(() => {
    validationTestModule = new ValidationTestModule();
  });

  it('should create an instance', () => {
    expect(validationTestModule).toBeTruthy();
  });
});
