import { Component, OnInit } from '@angular/core';
import { DialogRef } from '../../../core.ui';

@Component({
  selector: 'app-customdialogcontent',
  templateUrl: './customdialogcontent.component.html'
})
export class CustomDialogContentComponent implements OnInit {
  title: string;
  closeBtnName = 'Close';
  list: any[] = [];

  constructor(public dialogRef: DialogRef) {}

  ngOnInit() {
    this.list.push('PROFIT!!!');
  }

  onClose() {
    this.dialogRef.hide();
  }
}
