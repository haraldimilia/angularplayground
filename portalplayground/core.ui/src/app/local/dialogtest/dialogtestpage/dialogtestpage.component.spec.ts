import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogTestPageComponent } from './dialogtestpage.component';

describe('DialogTestPageComponent', () => {
  let component: DialogTestPageComponent;
  let fixture: ComponentFixture<DialogTestPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogTestPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTestPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
