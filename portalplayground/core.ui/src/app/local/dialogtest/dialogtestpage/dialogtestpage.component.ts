import { Component, HostBinding } from '@angular/core';

import { DialogButtons, DialogDefaultButton, DialogIcons, DialogService } from '../../../core.ui';

import { CustomDialogContentComponent } from '../dialogs/customdialogcontent.component';

import { CustomBodyDialogComponent } from '../dialogs/custombodydialog.component';
import { Log } from 'ng2-logger/client';

const log = Log.create('DialogTestPageComponent');

@Component({
  selector: 'app-dialogtestpage',
  templateUrl: './dialogtestpage.component.html',
  styleUrls: ['./dialogtestpage.component.scss']
})
export class DialogTestPageComponent {

  constructor(private dialogService: DialogService) { }

  onShowTextHelloWorldDialog() {
    const dialog = this.dialogService.showText('Hello World');
    dialog.onClosed = () => {
      log.info('dialog closed', dialog.dialogResult);
    };
  }

  onShowTextTitleYesNoDialog() {
    const dialog = this.dialogService.showText('Hello World', 'My Title', DialogButtons.YesNo);
    dialog.onClosed = () => {
      log.info('dialog closed', dialog.dialogResult);
    };
  }

  onShowTextNoHeaderCloseButtonDialog() {
    const dialog = this.dialogService.showText('The title has no close button.', 'My Title',
    DialogButtons.OK, DialogDefaultButton.Button1, DialogIcons.None, { hasCloseButton: false });
    dialog.onClosed = () => {
      log.info('dialog closed', dialog.dialogResult);
    };
  }

  onShowTextNoEscBackdropClickDialog() {
    const dialog = this.dialogService.showText(
    'This Dialog can only be closed with the button. No ESC Key nor Backdrop clicks. So the user is forced to make a decission', 'My Title',
    DialogButtons.YesNo, DialogDefaultButton.Button1,  DialogIcons.None, { hasCloseButton: false, disableCloseByBackdropOrESC: true });
    dialog.onClosed = () => {
      log.info('dialog closed', dialog.dialogResult);
    };
  }

  onShowCustomBodyDialog() {
    const dialog = this.dialogService.showCustomBody(CustomBodyDialogComponent, 'My Title',
      DialogButtons.OKCancel, DialogDefaultButton.Button2);
    dialog.onAfterBodyContentViewInit = (contentInstance: CustomBodyDialogComponent) => {
      contentInstance.list = [
        'aaa',
        'bbb',
        'ccc',
      ];
     };
    dialog.onClosed = () => {
      log.info('dialog closed', dialog.dialogResult);
    };
  }

  onShowCustomDialog() {
    // this is the bootstrap example
    // including BS tags
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Full Custom - Modal with component'
    };

    const dialogRef = this.dialogService.showTemplateDialog(CustomDialogContentComponent, {initialState});
    dialogRef.content.closeBtnName = 'Close';
  }
}
