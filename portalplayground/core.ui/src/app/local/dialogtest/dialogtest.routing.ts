import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';

import { DialogTestPageComponent } from './dialogtestpage/dialogtestpage.component';

const routes: Routes = [
  {
    path: '',
    component: DialogTestPageComponent,
    data: {
      title: 'Dialog Test'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DialogTestRoutingModule { }
