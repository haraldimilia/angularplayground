
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { DialogTestRoutingModule } from './dialogtest.routing';

import { DialogModule } from './../../core.ui';

import { CustomBodyDialogComponent } from './dialogs/custombodydialog.component';
import { CustomDialogContentComponent } from './dialogs/customdialogcontent.component';
import { DialogTestPageComponent } from './dialogtestpage/dialogtestpage.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DialogTestRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    DialogModule
  ],
  declarations: [
    DialogTestPageComponent,
    CustomDialogContentComponent,
    CustomBodyDialogComponent
  ],
  entryComponents: [
    CustomDialogContentComponent,
    CustomBodyDialogComponent
  ]
})
export class DialogTestModule { }
