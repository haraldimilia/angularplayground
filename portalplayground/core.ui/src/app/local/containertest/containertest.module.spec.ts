import { ContainerTestModule } from './containertest.module';

describe('ContainerTestModule', () => {
  let containerTestModule: ContainerTestModule;

  beforeEach(() => {
    containerTestModule = new ContainerTestModule();
  });

  it('should create an instance', () => {
    expect(containerTestModule).toBeTruthy();
  });
});
