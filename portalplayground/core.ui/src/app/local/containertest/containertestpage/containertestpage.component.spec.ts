import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerTestPageComponent } from './containertestpage.component';

describe('ContainerTestPageComponent', () => {
  let component: ContainerTestPageComponent;
  let fixture: ComponentFixture<ContainerTestPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerTestPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerTestPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
