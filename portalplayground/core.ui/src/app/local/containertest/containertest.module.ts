
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { ContainerTestRoutingModule } from './containertest.routing';
import { ContainerTestPageComponent } from './containertestpage/containertestpage.component';

import { ContainerModule } from './../../core.ui';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ContainerTestRoutingModule,
    BsDropdownModule.forRoot(),
    ButtonsModule.forRoot(),
    ContainerModule
  ],
  declarations: [ContainerTestPageComponent]
})
export class ContainerTestModule { }
