import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';

import { ContainerTestPageComponent } from './containertestpage/containertestpage.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerTestPageComponent,
    data: {
      title: 'Container Test'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContainerTestRoutingModule { }
