export { ElementSize, InputGroup } from './form/uibasic';
export { KeyValueItem } from './form/keyvalueitem';

export { DynamicFormComponent } from './dynamicform/dynamicform/dynamicform.component';
export { FormItem } from './dynamicform/formitem';
export { FormType } from './dynamicform/formtype';
export { FormDescription } from './dynamicform/formdescription';
export { DynamicFormHandler } from './dynamicform/dynamicformhandler';

export { ContainerModule } from './container/container.module';
export { DynamicFormModule } from './dynamicform/dynamicform.module';
export { FormModule } from './form/form.module';
export { ValidationModule } from './validation/validation.module';
export { NotificationModule } from './notification/notification.module';

export { FieldError, ValidationError } from './validation/validationerror';

export { FormValidationService } from './validation/formvalidation.service';

export { NotificationService } from './notification/notification.service';

export { DialogModule } from './dialog/dialog.module';
export { DialogService } from './dialog/dialog.service';
export { CustomDialogOptions } from './dialog/customdialogoptions';
export { DialogOptions } from './dialog/dialogoptions';
export { DialogButtons } from './dialog/dialogbuttons';
export { DialogIcons } from './dialog/dialogicons';
export { DialogResult } from './dialog/dialogresult';
export { DialogDefaultButton } from './dialog/dialogdefaultbutton';
export { UIDialogComponent } from './dialog/uidialog/uidialog.component';



export { UIToolsModule } from './uitools/uitools.module';

 // we can't get rid of this (only by rewriting the loader)
export { BsModalRef as DialogRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

