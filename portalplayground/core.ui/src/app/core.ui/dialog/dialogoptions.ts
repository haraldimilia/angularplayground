export interface DialogOptions {
  hasCloseButton?: boolean;
  disableCloseByBackdropOrESC?: boolean;
}
