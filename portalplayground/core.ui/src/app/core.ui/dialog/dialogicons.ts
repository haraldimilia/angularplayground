// idea from here: https://msdn.microsoft.com/de-de/library/system.windows.forms.messageboxicon(v=vs.110).aspx
export enum DialogIcons {
  Asterisk = '../../../assets/img/messagebox/asterisk.png',
  Error = '../../../assets/img/messagebox/cancel.png',
  Exclamation = '../../../assets/img/messagebox/exclamation.png',
  Hand = '../../../assets/img/messagebox/hold.png',
  Information = '../../../assets/img/messagebox/information.png',
  None = 'None',
  Question = '../../../assets/img/messagebox/question.png',
  Stop = '../../../assets/img/messagebox/stop.png',
  Warning = '../../../assets/img/messagebox/warning.png'
  // TODO: fill in the rest
}
