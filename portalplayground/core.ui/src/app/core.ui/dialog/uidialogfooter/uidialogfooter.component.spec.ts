import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../../container/container.module';
import { ValidationModule } from '../../validation/validation.module';

import { AutofocusDirective } from '../../uitools/autofocus.directive';
import { UIDialogFooterComponent } from './uidialogfooter.component';
import { UIErrorReporterComponent } from '../../form/uierrorreporter/uierrorreporter.component';

describe('UIDialogFooterComponent', () => {
  let component: UIDialogFooterComponent;
  let fixture: ComponentFixture<UIDialogFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
      ],
      declarations: [
        AutofocusDirective,
        UIErrorReporterComponent,
        UIDialogFooterComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UIDialogFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
