import { Component, Input } from '@angular/core';
// we can't get rid of BsModalRef (only by rewriting the loader) - so we shadow it
import { BsModalRef as DialogRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { DialogButtons } from '../dialogbuttons';
import { DialogDefaultButton } from '../dialogdefaultbutton';
import { DialogResult } from '../dialogresult';

@Component({
  // tslint:disable-next-line
  selector: 'ui-dialog-footer', // we want the selector to start with "ui-"
  templateUrl: './uidialogfooter.component.html',
  styleUrls: ['./uidialogfooter.component.scss']
})
export class UIDialogFooterComponent {
  private _buttons: DialogButtons = DialogButtons.None;
  private _defaultButton: DialogDefaultButton = DialogDefaultButton.None;
  public primaryButton: DialogResult = null;

  @Input() dialogRef: DialogRef;
  @Input() set buttons(value: DialogButtons) {
    this._buttons = value;
    this.updateButtonStyles();
  }
  get buttons(): DialogButtons {
    return this._buttons;
  }
  @Input() set defaultButton(value: DialogDefaultButton) {
    this._defaultButton = value;
    this.updateButtonStyles();
  }
  get defaultButton(): DialogDefaultButton {
    return this._defaultButton;
  }

  onButtonPressed(dialogResult: DialogResult) {
    if (this.dialogRef == null || this.dialogRef === undefined) {
      return;
    }

    if (this.dialogRef.content != null && this.dialogRef.content !== undefined) {
      if (this.dialogRef.content.dialogResult != null && this.dialogRef.content.dialogResult !== undefined) {
        this.dialogRef.content.dialogResult = dialogResult;
      }
      if (this.dialogRef.content.onClosed != null && this.dialogRef.content.onClosed !== undefined) {
        this.dialogRef.content.onClosed();
      }
    }

    this.dialogRef.hide();
  }

  private updateButtonStyles() {
    let primaryButton: DialogResult = null;

    switch (this.buttons) {
      case DialogButtons.AbortRetryIgnore:
        switch (this.defaultButton) {
          case DialogDefaultButton.Button1:
            primaryButton = DialogResult.Abort;
            break;
          case DialogDefaultButton.Button2:
            primaryButton = DialogResult.Abort;
            break;
          case DialogDefaultButton.Button3:
            primaryButton = DialogResult.Abort;
            break;
        }
        break;
      case DialogButtons.OK:
        switch (this.defaultButton) {
          case DialogDefaultButton.Button1:
            primaryButton = DialogResult.OK;
            break;
        }
        break;
      case DialogButtons.OKCancel:
        switch (this.defaultButton) {
          case DialogDefaultButton.Button1:
            primaryButton = DialogResult.OK;
            break;
          case DialogDefaultButton.Button2:
            primaryButton = DialogResult.Cancel;
            break;
        }
        break;
      case DialogButtons.RetryCancel:
        switch (this.defaultButton) {
          case DialogDefaultButton.Button1:
            primaryButton = DialogResult.Retry;
            break;
          case DialogDefaultButton.Button2:
            primaryButton = DialogResult.Cancel;
            break;
        }
        break;
      case DialogButtons.YesNo:
        switch (this.defaultButton) {
          case DialogDefaultButton.Button1:
            primaryButton = DialogResult.Yes;
            break;
          case DialogDefaultButton.Button2:
            primaryButton = DialogResult.No;
            break;
        }
        break;
      case DialogButtons.YesNoCancel:
        switch (this.defaultButton) {
          case DialogDefaultButton.Button1:
            primaryButton = DialogResult.Yes;
            break;
          case DialogDefaultButton.Button2:
            primaryButton = DialogResult.No;
            break;
          case DialogDefaultButton.Button3:
            primaryButton = DialogResult.Cancel;
            break;
        }
        break;
    }

    this.primaryButton = primaryButton;
  }
}
