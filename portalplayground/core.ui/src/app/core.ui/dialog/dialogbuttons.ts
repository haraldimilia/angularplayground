// idea from here: https://msdn.microsoft.com/de-de/library/system.windows.forms.messageboxbuttons(v=vs.110).aspx
export enum DialogButtons {
  None = 'None',
  AbortRetryIgnore = 'AbortRetryIgnore',
  OK = 'OK',
  OKCancel = 'OKCancel',
  RetryCancel = 'RetryCancel',
  YesNo = 'YesNo',
  YesNoCancel = 'YesNoCancel',
}
