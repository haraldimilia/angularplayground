import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UIDialogComponent } from './uidialog.component';
import { UIToolsModule } from '../../uitools/uitools.module';

import { DialogService } from '../dialog.service';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UIDialogBodyComponent } from '../uidialogbody/uidialogbody.component';
import { UIDialogBodyContentDirective } from '../uidialogbodycontent.directive';
import { UIDialogBodyTextComponent } from '../uidialogbodytext/uidialogbodytext.component';
import { UIDialogFooterComponent } from '../uidialogfooter/uidialogfooter.component';
import { UIDialogHeaderComponent } from '../uidialogheader/uidialogheader.component';


describe('UIDialogComponent', () => {
  let component: UIDialogComponent;
  let fixture: ComponentFixture<UIDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ModalModule.forRoot(),
        UIToolsModule
      ],
      providers: [
        DialogService,
      ],
      declarations: [
        UIDialogHeaderComponent,
        UIDialogFooterComponent,
        UIDialogBodyComponent,
        UIDialogComponent,
        UIDialogBodyTextComponent,
        UIDialogBodyContentDirective
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UIDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
