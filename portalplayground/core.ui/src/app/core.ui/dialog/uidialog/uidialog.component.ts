import { Component, Input, Type } from '@angular/core';
// we can't get rid of BsModalRef (only by rewriting the loader) - so we shadow it
import { BsModalRef as DialogRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { DialogBase } from '../dialogbase';
import { DialogButtons } from '../dialogbuttons';
import { DialogDefaultButton } from '../dialogdefaultbutton';
import { DialogIcons } from '../dialogicons';
import { DialogResult } from '../dialogresult';

@Component({
  // tslint:disable-next-line
  selector: 'ui-dialog', // we want the selector to start with "ui-"
  templateUrl: './uidialog.component.html',
  styleUrls: ['./uidialog.component.scss']
})
export class UIDialogComponent implements DialogBase {
  @Input() dialogRef: DialogRef;
  @Input() title = 'Dialog';
  @Input() hasCloseButton = true;
  @Input() buttons: DialogButtons = DialogButtons.None;
  @Input() defaultButton: DialogDefaultButton = DialogDefaultButton.None;
  @Input() dialogIcons: DialogIcons = DialogIcons.None;
  @Input() bodyComponent: Type<any> = null;

  dialogResult: DialogResult = DialogResult.None;

  // DialogBase
  instance: UIDialogComponent;
  onClosed(): any { }
  onAfterBodyContentViewInit(contentInstance: any): any  { }
}
