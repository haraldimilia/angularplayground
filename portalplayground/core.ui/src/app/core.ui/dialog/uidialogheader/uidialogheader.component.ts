import { Component, Input } from '@angular/core';

// we can't get rid of BsModalRef (only by rewriting the loader) - so we shadow it
import { BsModalRef as DialogRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { DialogResult } from './../dialogresult';

@Component({
  // tslint:disable-next-line
  selector: 'ui-dialog-header', // we want the selector to start with "ui-"
  templateUrl: './uidialogheader.component.html',
  styleUrls: ['./uidialogheader.component.scss']
})
export class UIDialogHeaderComponent {
  @Input() dialogRef: DialogRef;
  @Input() title = 'Dialog';
  @Input() hasCloseButton = true;

  onClose() {
    if (!this.hasCloseButton) {
      return;
    }

    if (this.dialogRef == null || this.dialogRef === undefined) {
      return;
    }

    if (this.dialogRef.content != null && this.dialogRef.content !== undefined) {
      if (this.dialogRef.content.dialogResult != null && this.dialogRef.content.dialogResult !== undefined) {
        this.dialogRef.content.dialogResult = DialogResult.Cancel;
      }
      if (this.dialogRef.content.onClosed != null && this.dialogRef.content.onClosed !== undefined) {
        this.dialogRef.content.onClosed();
      }
    }

    this.dialogRef.hide();
  }
}
