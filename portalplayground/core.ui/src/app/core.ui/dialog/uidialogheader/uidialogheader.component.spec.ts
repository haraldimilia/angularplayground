import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UIDialogHeaderComponent } from './uidialogheader.component';

describe('UIDialogHeaderComponent', () => {
  let component: UIDialogHeaderComponent;
  let fixture: ComponentFixture<UIDialogHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UIDialogHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UIDialogHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
