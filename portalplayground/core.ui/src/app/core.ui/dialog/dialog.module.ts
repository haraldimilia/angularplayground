
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DialogService } from './dialog.service';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UIDialogComponent } from './uidialog/uidialog.component';
import { UIDialogBodyComponent } from './uidialogbody/uidialogbody.component';
import { UIDialogBodyContentDirective } from './uidialogbodycontent.directive';
import { UIDialogBodyTextComponent } from './uidialogbodytext/uidialogbodytext.component';
import { UIDialogFooterComponent } from './uidialogfooter/uidialogfooter.component';
import { UIDialogHeaderComponent } from './uidialogheader/uidialogheader.component';
import { UIToolsModule } from './../uitools/uitools.module';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    UIToolsModule
  ],
  declarations: [
    UIDialogHeaderComponent,
    UIDialogFooterComponent,
    UIDialogBodyComponent,
    UIDialogComponent,
    UIDialogBodyTextComponent,
    UIDialogBodyContentDirective,
    UIDialogBodyTextComponent,
  ],
  providers: [
    DialogService,
  ],
  entryComponents: [
    UIDialogComponent,
    UIDialogBodyTextComponent,
  ]
})
export class DialogModule { }
