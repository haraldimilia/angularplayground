// idea from here: https://msdn.microsoft.com/de-de/library/system.windows.forms.dialogresult(v=vs.110).aspx
export enum DialogResult {
  Abort = 'Abort',
  Stop = 'Stop',
  Cancel = 'Cancel',
  Ignore = 'Ignore',
  No = 'No',
  None = 'None',
  OK = 'OK',
  Retry = 'Retry',
  Yes = 'Yes'
}
