import { DialogResult } from './dialogresult';

export interface DialogBase {
  instance: any;
  dialogResult: DialogResult;
  onClosed(): any;
  onAfterBodyContentViewInit(contentInstance: any): any;
}
