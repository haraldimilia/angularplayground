import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UIDialogBodyTextComponent } from './uidialogbodytext.component';

describe('UIDialogBodyTextComponent', () => {
  let component: UIDialogBodyTextComponent;
  let fixture: ComponentFixture<UIDialogBodyTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UIDialogBodyTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UIDialogBodyTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
