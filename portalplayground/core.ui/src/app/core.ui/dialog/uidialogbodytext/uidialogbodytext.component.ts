import { AfterViewInit, Component, Input } from '@angular/core';
import { DialogIcons } from '../dialogicons';

@Component({
  // tslint:disable-next-line
  selector: 'ui-dialog-body-text', // we want the selector to start with "ui-"
  templateUrl: './uidialogbodytext.component.html',
  styleUrls: ['./uidialogbodytext.component.scss']
})

export class UIDialogBodyTextComponent implements AfterViewInit {
  @Input() text: string = null;
  @Input() icon: DialogIcons = DialogIcons.None;
  // iconSrc = '../../../assets/img/brand/' + this.icon;
  icons = true;

  constructor() {
  }

  ngAfterViewInit() {
    if (this.icon === 'None') {
      this.icons = false;
    }
  }
}
