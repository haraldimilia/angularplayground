import { AfterViewInit, Component, ComponentFactoryResolver, ElementRef, Input, Type, ViewChild } from '@angular/core';
// we can't get rid of BsModalRef (only by rewriting the loader) - so we shadow it
import { BsModalRef as DialogRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { DialogIcons } from '../dialogicons';
import { UIDialogBodyContentDirective } from '../uidialogbodycontent.directive';
import { UIDialogBodyTextComponent } from '../uidialogbodytext/uidialogbodytext.component';

// rtfm: https://angular.io/guide/dynamic-component-loader

@Component({
  // tslint:disable-next-line
  selector: 'ui-dialog-body', // we want the selector to start with "ui-"
  templateUrl: './uidialogbody.component.html',
  styleUrls: ['./uidialogbody.component.scss']
})
export class UIDialogBodyComponent implements AfterViewInit {
  private initalized = false;
  private _component: Type<any> = null;
  @Input() dialogRef: DialogRef;
  @Input() dialogIcons: DialogIcons = DialogIcons.None;

  @ViewChild(UIDialogBodyContentDirective) uiDialogBodyContent: UIDialogBodyContentDirective;

  @Input()
  get component(): Type<any> {
    return this._component;
  }

  set component(value: Type<any>) {
    this._component = value;
    if (this.initalized) {
      this.createChild();
    }
  }

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngAfterViewInit() {
    this.initalized = true;
    this.createChild();
  }

  protected createChild() {
    const viewContainerRef = this.uiDialogBodyContent.viewContainerRef;
    viewContainerRef.clear();

    if (this.component == null || this.component === undefined) {
      return;
    }

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.component);
    const componentRef = viewContainerRef.createComponent(componentFactory);

    if (this.dialogRef && this.dialogRef.content && componentRef) {
      if (!(componentRef.instance instanceof UIDialogBodyTextComponent)) {
        componentRef.instance.dialogIcons = this.dialogIcons;
      }

      const onAfterBodyContentViewInit: any = (<any>this.dialogRef.content).onAfterBodyContentViewInit;
      if (onAfterBodyContentViewInit) {
        onAfterBodyContentViewInit(componentRef.instance);
      }
    }
  }
}
