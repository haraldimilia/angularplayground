import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DialogService } from './../dialog.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../../container/container.module';
import { ValidationModule } from '../../validation/validation.module';

import { ModalModule } from 'ngx-bootstrap/modal';
import { UIDialogBodyComponent } from './uidialogbody.component';
import { UIDialogBodyContentDirective } from '../uidialogbodycontent.directive';
import { UIToolsModule } from '../..';

describe('UIDialogBodyComponent', () => {
  let component: UIDialogBodyComponent;
  let fixture: ComponentFixture<UIDialogBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        ModalModule.forRoot(),
        UIToolsModule
      ],
      providers: [
        DialogService,
      ],
      declarations: [
        UIDialogBodyContentDirective,
        UIDialogBodyComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UIDialogBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
