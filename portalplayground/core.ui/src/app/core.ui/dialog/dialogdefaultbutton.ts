// idea from here: https://msdn.microsoft.com/de-de/library/system.windows.forms.messageboxdefaultbutton(v=vs.110).aspx
export enum DialogDefaultButton {
  None = 'None',
  Button1 = 'Button1',
  Button2 = 'Button2',
  Button3 = 'Button3',
}
