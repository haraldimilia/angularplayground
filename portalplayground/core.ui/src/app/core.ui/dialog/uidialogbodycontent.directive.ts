import { Directive, ViewContainerRef } from '@angular/core';

// https://angular.io/guide/dynamic-component-loader

@Directive({
  // tslint:disable-next-line
  selector: '[uiDialogBodyContent]', // we want the selector to start with "ui-"
})
export class UIDialogBodyContentDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
