import { ChangeDetectorRef, Injectable, OnDestroy, TemplateRef, Type } from '@angular/core';
import { CustomDialogOptions } from './customdialogoptions';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/observable/combineLatest';

// we can't get rid of BsModalRef (only by rewriting the loader) - so we shadow it
import { BsModalRef as DialogRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { DialogBase } from './dialogbase';
import { DialogButtons } from './dialogbuttons';
import { DialogDefaultButton } from './dialogdefaultbutton';
import { DialogIcons } from './dialogicons';
import { DialogOptions } from './dialogoptions';
import { DialogResult } from './dialogresult';
import { UIDialogComponent } from './uidialog/uidialog.component';
import { UIDialogBodyTextComponent } from './uidialogbodytext/uidialogbodytext.component';

// this is a wrapper for ngx bootstrap
// https://valor-software.com/ngx-bootstrap/#/modals

@Injectable({
  providedIn: 'root'
})
export class DialogService implements OnDestroy {
  subscriptions: Subscription[] = [];

  constructor(private modalService: BsModalService) {

  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  /**
   * wrapper for the NGX Bootstrap
   */
  public showTemplateDialog(content: string | TemplateRef<any> | any, config?: CustomDialogOptions): DialogRef {
    const result = this.modalService.show(content, config);

    const subscription = this.modalService.onHide.subscribe((reason: string) => {
      // const _reason = reason ? `, dismissed by ${reason}` : '';
      // console.log(`onHide event has been fired${_reason}`);

      if (reason !== null || reason !== undefined) {
        if (result.content.dialogResult != null && result.content.dialogResult !== undefined
          && result.content.dialogResult === DialogResult.None) {
            // we are closed with the reason ESC or Backdrop and no dialogresult has been set
          if (result.content.onClosed != null && result.content.onClosed !== undefined) {
            // call the on Closed handler
            result.content.onClosed();
          }
        }
      }

      subscription.unsubscribe();
      const index = this.subscriptions.indexOf(subscription);
      if (index > -1) {
        this.subscriptions.splice(index, 1);
      }
    });
    this.subscriptions.push(subscription);

    return result;
  }

  /**
   * this sets the default push button to the first button of the button type
   */
  public showText(text: string, caption?: string, buttons?: DialogButtons,
    defaultButton?: DialogDefaultButton, dialogIcons?: DialogIcons,  options?: DialogOptions): DialogBase {
    // for text dialogs set the button "OK" as default
    if (buttons == null || buttons === undefined) {
      buttons = DialogButtons.OK;
    }
    if (defaultButton == null || defaultButton === undefined) {
      defaultButton = DialogDefaultButton.Button1;
    }
    if (dialogIcons == null || dialogIcons === undefined) {
      dialogIcons = DialogIcons.None;
    }
    const result = this.showCustomBodyInternal(UIDialogBodyTextComponent, caption, buttons, defaultButton, dialogIcons, options);
    if (result.content) {
      const dialog: UIDialogComponent = <UIDialogComponent>result.content;
      dialog.onAfterBodyContentViewInit = (contentInstance: UIDialogBodyTextComponent) => {
        contentInstance.text = text;
        contentInstance.icon = dialogIcons;
      };
    }
    return result.content;
  }

  /**
   * this does not set the default push button - please specify if you want one
   */
  public showCustomBody(type: Type<any>, caption?: string, buttons?: DialogButtons,
    defaultButton?: DialogDefaultButton, options?: DialogOptions): DialogBase {
    const result = this.showCustomBodyInternal(type, caption, buttons, defaultButton, DialogIcons.None, options);
    return result.content;
  }

  public hide(level: number): void {
    this.modalService.hide(level);
  }

  private showCustomBodyInternal(type: Type<any>, caption?: string, buttons?: DialogButtons,
    defaultButton?: DialogDefaultButton, dialogIcons?: DialogIcons, options?: DialogOptions): DialogRef {
    let config: CustomDialogOptions = null;

    if (options && options.disableCloseByBackdropOrESC) {
      config = {
        ignoreBackdropClick: true,
        keyboard: false
      };
    }

    const result = this.showTemplateDialog(UIDialogComponent, config);
    if (result.content) {
      const dialog: UIDialogComponent = <UIDialogComponent>result.content;
      dialog.instance = dialog;
      dialog.dialogRef = result;
      dialog.bodyComponent = type;

      if (caption != null && caption !== undefined) {
        dialog.title = caption;
      }

      if (buttons != null && buttons !== undefined) {
        dialog.buttons = buttons;
      }

      if (defaultButton != null && defaultButton !== undefined) {
        dialog.defaultButton = defaultButton;
      }

      if (dialogIcons != null && dialogIcons !== undefined) {
        dialog.dialogIcons = dialogIcons;
      }

      if (options != null && options !== undefined) {
        if (options.hasCloseButton != null && options.hasCloseButton !== undefined) {
          dialog.hasCloseButton = options.hasCloseButton;
        }
      }
    }
    return result;
  }
}
