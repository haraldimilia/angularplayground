import { Injectable } from '@angular/core';

import { ActiveToast, IndividualConfig, ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor(private toastr: ToastrService) {}

  showToast(message?: string, title?: string, override?: Partial<IndividualConfig>, type?: string) {
    this.toastr.show(message, title, override, type);
  }

  successToast(message?: string, title?: string, override?: Partial<IndividualConfig>): ActiveToast<any> {
    return this.toastr.success(message, title, override);
  }

  errorToast(message?: string, title?: string, override?: Partial<IndividualConfig>): ActiveToast<any> {
    return this.toastr.error(message, title, override);
  }

  infoToast(message?: string, title?: string, override?: Partial<IndividualConfig>): ActiveToast<any> {
    return this.toastr.info(message, title, override);
  }

  warningToast(message?: string, title?: string, override?: Partial<IndividualConfig>): ActiveToast<any> {
    return this.toastr.warning(message, title, override);
  }

  clearAllToasts() {
    this.toastr.clear();
  }
}
