import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ToastrModule } from 'ngx-toastr';

import { NotificationService } from './notification.service';

@NgModule({
  imports: [
    CommonModule,
    /*
    // import in app.module
    ToastrModule.forRoot({ // read more here - https://github.com/scttcper/ngx-toastr
      timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    */
  ],
  declarations: [
  ],
  providers: [
    NotificationService
  ]
})
export class NotificationModule { }
