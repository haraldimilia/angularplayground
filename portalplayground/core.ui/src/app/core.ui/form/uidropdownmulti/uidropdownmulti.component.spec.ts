import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { By } from '@angular/platform-browser';
import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';
import { UIDropDownMultiComponent } from './uidropdownmulti.component';

describe('UIDropDownMultiComponent', () => {
  let component: UIDropDownMultiComponent;
  let fixture: ComponentFixture<UIDropDownMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UIDropDownMultiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UIDropDownMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

  // it('should create a label', () => {
  //   const label = TestingTools.randomString();
  //   component.label = label;
  //   fixture.detectChanges();

  //   const element = fixture.debugElement.query(By.css('label'));
  //   expect(element.nativeElement.textContent).toContain(label);
  //   expect(component).toBeTruthy();
  // });
});
