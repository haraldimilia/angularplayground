import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line
  selector: 'ui-dropdown-multi', // we want the selector to start with "ui-"
  templateUrl: './uidropdownmulti.component.html',
  styleUrls: ['./uidropdownmulti.component.scss']
})
export class UIDropDownMultiComponent implements OnInit {

  label: string;
  constructor() { }

  ngOnInit() {
  }

}
