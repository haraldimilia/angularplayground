import { Component, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { ElementSize, InputGroup } from '../uibasic';
import { UIBasicComponent } from '../uibasic.component';

import { KeyValueItem } from '../keyvalueitem';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  // tslint:disable-next-line
  selector: 'ui-checkboxgroup', // we want the selector to start with "ui-"
  templateUrl: './uicheckboxgroup.component.html',
  styleUrls: ['./uicheckboxgroup.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: UICheckBoxGroupComponent, multi: true }
  ]
})
export class UICheckBoxGroupComponent extends UIBasicComponent<KeyValueItem[]> {
  textvalue: string;
  enabled: boolean;
  @ViewChild(UIErrorReporterComponent) errorReporter: UIErrorReporterComponent;
  @Input() visible = true;
  @Input() disabled = false;
  @Input() static = false;
  @Input() label = '';
  @Input() help = '';
  @Input() htmlId: string = UIBasicComponent.uniqueHtmlId();
  @Input() horizontal: boolean;
  @Input() horizontalcols: number;
  @Input() size: ElementSize = ElementSize.Default;
  @Input() inputgroup: InputGroup = InputGroup.Invisible;
  @Input() value: KeyValueItem[] = null;

  @Input() items: KeyValueItem[] = [];

  isActive(probeItem: KeyValueItem): boolean {
    if (this.value == null || this.value === undefined || this.value.length === 0) {
      return false;
    }

    // TODO: this is a performance issue - make value as setter/getter and create a hashmap for the item.value <-> item
    for (const item of this.value) {
      if (item.value === probeItem.value) {
        return true;
      }
    }

    return false;
  }

  onToggle(toogleValue: KeyValueItem) {
    if (this.static || this.disabled) {
      return;
    }

    if (toogleValue && toogleValue.disabled) {
      return;
    }

    let currentValues: KeyValueItem[] = [];

    if (this.value != null && this.value !== undefined && this.value.length > 0) {
      currentValues = JSON.parse(JSON.stringify(this.value));
    }

    const hashMap: { [value: string]: KeyValueItem } = {};

    for (let i = 0; i < currentValues.length; i++) {
      const item = currentValues[i];
      if (item.value === toogleValue.value) {
        // toggle removed from the array
        currentValues.splice(i, 1);
        this.value = currentValues;
        this.onChange(this.value);
        return;
      }
      hashMap[item.value] = item; // create a hashmap
    }

    // we added a value - now create a
    // sorted currentValue list (according to the reference values)
    hashMap[toogleValue.value] = toogleValue; // create a hashmap
    currentValues = [];

    for (const item of this.items) {
      if (hashMap.hasOwnProperty(item.value)) {
        currentValues.push(item);
      }
    }

    // write values back
    this.value = currentValues;
    this.onChange(this.value);
  }
}
