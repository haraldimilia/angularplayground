import { Component, ViewChild } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../../container/container.module';
import { ValidationModule } from '../../validation/validation.module';

import { By } from '@angular/platform-browser';
import { KeyValueItem } from '../keyvalueitem';
import { UICheckBoxGroupComponent } from './uicheckboxGROUP.component';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  template: `
  <ui-checkboxgroup
    [visible]="visible"
    [disabled]="disabled"
    [static]="static"
    [label]="label"
    [help]="help"
    [(ngModel)]="value"
    [ngModelOptions]="{standalone: true}"
    [items]="items"
    >
  </ui-checkboxgroup>
`
})
export class TestComponent {
  @ViewChild(UICheckBoxGroupComponent) item: UICheckBoxGroupComponent;
  visible = true;
  disabled = false;
  static = false;
  label = '';
  help = '';
  value: KeyValueItem[] = null;
  items: KeyValueItem[] = [];
}

// input is declared here but no value
describe('UICheckBoxGroupComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  const dummyItems: KeyValueItem[] = [
    {
      label: TestingTools.randomString(),
      value: '0'
    },
    {
      label: TestingTools.randomString(),
      value: '1'
    },
    {
      label: TestingTools.randomString(),
      value: '2'
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
      ],
      declarations: [
        UIErrorReporterComponent,
        UICheckBoxGroupComponent,
        TestComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('form basic', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should create a label', () => {
      const label = TestingTools.randomString();
      component.label = label;
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('label'));
      expect(element.nativeElement.textContent).toContain(label);
      expect(component).toBeTruthy();
    });
  });

  describe('visibility', () => {
    it('it should be not visible in invisible mode', fakeAsync(() => {
      const label = TestingTools.randomString();
      component.label = label;
      component.visible = false;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('label'));
      expect(element).toBeNull();
    }));
  });

  describe('static', () => {
    it('should have a disabled buttons when static', fakeAsync(() => {
      component.static = true;
      component.items = dummyItems; // we need values
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const elements = fixture.debugElement.queryAll(By.css('button'));
      for (const element of elements) {
        expect(element.nativeElement.disabled).toBe(true);
      }
    }));

    it('it should have buttons when not static', fakeAsync(() => {
      component.static = true;
      component.items = dummyItems; // we need values
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const elements = fixture.debugElement.queryAll(By.css('button'));
      for (const element of elements) {
        expect(element.nativeElement.disabled).toBe(true);
      }
    }));
  });

  describe('html ids', () => {
    it('it should create an automatic unique htmlid', async(() => {
      const html_id = component.item.htmlId;
      expect(html_id).not.toBeNull();
    }));

    it('it should set predictable html id to child elements', async(() => {
      component.items = dummyItems; // we need values
      const html_id = TestingTools.randomString();
      const input_id = html_id + '_0_button'; // we test on button 0
      component.item.htmlId = html_id;
      fixture.detectChanges();
      const element = fixture.debugElement.query(By.css('button'));
      expect(element.nativeElement.outerHTML).toContain(input_id);
    }));

    it('it should set button html id when static', async(() => {
      component.items = dummyItems; // we need values
      const html_id = TestingTools.randomString();
      const input_id = html_id + '_0_button'; // we test on button 0
      component.item.htmlId = html_id;
      component.static = true;
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('button'));
      expect(element.nativeElement.outerHTML).toContain(input_id);
    }));

    it('it should set predictable html id to help elements', async(() => {
      const html_id = TestingTools.randomString();
      const help = TestingTools.randomString();
      const help_id = html_id + '_help';
      component.item.help = help;
      component.item.htmlId = html_id;
      fixture.detectChanges();
      expect(fixture.elementRef.nativeElement.outerHTML).toContain(help_id);
    }));
  });

  describe('data input', () => {
    it('should activate selected items', fakeAsync(() => {
      const input = true;
      component.items = dummyItems; // we need values
      component.value =  // activate 1 and 2 and let 0 to be inactive
        [
          dummyItems[1],
          dummyItems[2],
        ];
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();
      let counter = 0;
      const elements = fixture.debugElement.queryAll(By.css('button'));
      for (const element of elements) {
        // test for the "active" class
        if (counter === 0) {
          expect(element.nativeElement.outerHTML).not.toContain('active');
        } else {
          expect(element.nativeElement.outerHTML).toContain('active');
        }
        counter++;
      }

    }));

    it('should be active when clicked', fakeAsync(() => {
      const startValue = false;
      component.items = dummyItems; // we need values
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();


      // first button
      const buttonEl = fixture.debugElement.query(By.css('button')).nativeElement;
      expect(buttonEl.outerHTML).not.toContain('active');
      buttonEl.click(); // toggles the start value
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      expect(buttonEl.outerHTML).toContain('active');
      expect(component.value.length).toEqual(1);
      expect(component.value[0].label).toEqual(dummyItems[0].label);
      expect(component.value[0].value).toEqual(dummyItems[0].value);
    }));
  });

  describe('specific parts', () => {
    // NOP
  });
});
