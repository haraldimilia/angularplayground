import { Component, ViewChild } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../../container/container.module';
import { ValidationModule } from '../../validation/validation.module';

import { By } from '@angular/platform-browser';
import { UIDropDownComponent } from './uidropdown.component';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  template: `
  <ui-dropdown
    [label]="'Dropdown'"
    [items]="dropdownItems"
    [(ngModel)]="dropdownValue"
    [ngModelOptions]="{standalone: true}"
    [horizontal]="horizontal"
    [disabled]="disabled"
    [static]="static"
    >
  </ui-dropdown>
`
})
export class TestComponent {
  @ViewChild(UIDropDownComponent) item: UIDropDownComponent;
  visible = true;
  disabled = false;
  static = false;
  label = '';
  help = '';
  value: string = null;
}

describe('UIDropDownComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
      ],
      declarations: [
        UIErrorReporterComponent,
        UIDropDownComponent,
        TestComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
