import { Component, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { ElementSize, InputGroup } from '../uibasic';
import { UIBasicComponent } from '../uibasic.component';

import { KeyValueItem } from '../keyvalueitem';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  // tslint:disable-next-line
  selector: 'ui-dropdown', // we want the selector to start with "ui-"
  templateUrl: './uidropdown.component.html',
  styleUrls: ['./uidropdown.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: UIDropDownComponent, multi: true }
  ]
})
export class UIDropDownComponent extends UIBasicComponent<string> {
  @ViewChild(UIErrorReporterComponent) errorReporter: UIErrorReporterComponent;
  @Input() visible = true;
  @Input() disabled = false;
  @Input() static = false;
  @Input() label = '';
  @Input() help = '';
  @Input() htmlId: string = UIBasicComponent.uniqueHtmlId();
  @Input() horizontal: boolean;
  @Input() horizontalcols: number;
  @Input() size: ElementSize = ElementSize.Default;
  @Input() inputgroup: InputGroup = InputGroup.Invisible;
  @Input() value: string = null;

  @Input() items: KeyValueItem[] = [];

  public static getDefaultDropDownItem(): KeyValueItem[] {
    const result: KeyValueItem[] = [
      {
        label: 'Select item...',
        value: null
      }
    ];
    return result;
  }
}
