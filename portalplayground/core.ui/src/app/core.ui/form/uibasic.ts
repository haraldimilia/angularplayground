export enum ElementSize {
  Default = 'Default',
  Small = 'Small',
  Large = 'Large'
}

export enum InputGroup {
  Invisible = 'Invisible',
  Right = 'Right',
  Left = 'Left'
}

export interface UIBasic {
  visible?: boolean;
  disabled?: boolean;
  static?: boolean;
  label?: string;
  help?: string;
  size?: ElementSize;
  inputgroup?: InputGroup;
  htmlId?: string;
  horizontal?: boolean;
  horizontalcols?: number;
}
