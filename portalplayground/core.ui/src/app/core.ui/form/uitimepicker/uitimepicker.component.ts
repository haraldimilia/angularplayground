import { Component, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { ElementSize, InputGroup } from '../uibasic';
import { UIBasicComponent } from '../uibasic.component';

import { KeyValueItem } from '../keyvalueitem';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  // tslint:disable-next-line
  selector: 'ui-timepicker', // we want the selector to start with "ui-"
  templateUrl: './uitimepicker.component.html',
  styleUrls: ['./uitimepicker.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: UITimePickerComponent, multi: true }
  ]
})
export class UITimePickerComponent extends UIBasicComponent<Date> {
  @ViewChild(UIErrorReporterComponent) errorReporter: UIErrorReporterComponent;
  @Input() visible = true;
  @Input() disabled = false;
  @Input() static = false;
  @Input() label = '';
  @Input() help = '';
  @Input() htmlId: string = UIBasicComponent.uniqueHtmlId();
  @Input() horizontal: boolean;
  @Input() horizontalcols: number;
  @Input() size: ElementSize = ElementSize.Default;
  @Input() inputgroup: InputGroup = InputGroup.Invisible;
  @Input() value: Date = null;
}
