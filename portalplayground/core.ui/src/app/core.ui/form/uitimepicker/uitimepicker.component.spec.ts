import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { ContainerModule } from '../../container/container.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';
import { ValidationModule } from '../../validation/validation.module';

import { By } from '@angular/platform-browser';
import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';
import { UITimePickerComponent } from './uitimepicker.component';


describe('UITimePickerComponent', () => {
  let component: UITimePickerComponent;
  let fixture: ComponentFixture<UITimePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
      ],
      declarations: [
        UIErrorReporterComponent,
        UITimePickerComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UITimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a label', () => {
    const label = TestingTools.randomString();
    component.label = label;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('label'));
    expect(element.nativeElement.textContent).toContain(label);
    expect(component).toBeTruthy();
  });
});
