import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  // tslint:disable-next-line
  selector: 'ui-error-reporter', // we want the selector to start with "ui-"
  templateUrl: './uierrorreporter.component.html',
  styleUrls: ['./uierrorreporter.component.scss']
})
export class UIErrorReporterComponent {
  // this is a wrapper around - https://github.com/UltimateAngular/ngx-errors

  static errorsData = [
    { name: 'required', text: 'Input required.', rules: ['dirty'] },
    { name: 'minlength', text: 'Input doesn\'t match the minimum required length.', rules: ['dirty'] },
    { name: 'maxlength', text: 'Input doesn\'t match the maxium required length.', rules: ['dirty'] },
    { name: 'min', text: 'Input doesn\'t match the minimum.', rules: ['dirty'] },
    { name: 'max', text: 'Input doesn\'t match the maximum.', rules: ['dirty'] },
    { name: 'pattern', text: 'Input doesn\'t match the required pattern.', rules: ['dirty'] },
    { name: 'email', text: 'Input is not a vaild email.', rules: ['dirty'] },
    { name: 'remote', showValue: true, rules: ['dirty'] }
  ];

  errors = UIErrorReporterComponent.errorsData;
  label: string;
  @Input() formGroup: FormGroup;
  @Input() formControlName: string;



}
