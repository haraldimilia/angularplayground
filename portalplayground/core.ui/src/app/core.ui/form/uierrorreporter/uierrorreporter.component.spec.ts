import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToastrModule, ToastrService } from 'ngx-toastr';

import { FormModule } from './../form.module';

import { FormsModule, FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ValidationModule } from '../../validation/validation.module';

import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { By } from '@angular/platform-browser';
import { FormValidationService } from '../../validation/formvalidation.service';
import { NotificationModule } from '../../notification/notification.module';
import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';
import { UIErrorReporterComponent } from './uierrorreporter.component';

@Component({
  // we use this stupid template with modes because of timing and synchronisation issues
  // maybee the UIErrorReporterComponent handler in combination with the BSControlStatusDirective
  // does not support the changes of validator type
  template: `
    <form [formGroup]="formGroup" autocomplete="off" novalidate action="#" method="#">
      <ui-textinput
        *ngIf="mode=='noValidator'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='required'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='minLength'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='maxLength'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='min'"
        [label]="mode"
        [numericMode]="numericMode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='max'"
        [numericMode]="'true'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='email'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='pattern'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='checkbox'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='radio'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='dropdownlist'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
      <ui-textinput
        *ngIf="mode=='remote'"
        [label]="mode"
        [formControlName]="'formText'">
      </ui-textinput>
    </form>
  `
})
export class TestComponent implements OnInit {
  formGroup: FormGroup;
  mode = 'noValidator';

  constructor(public formBuilder: FormBuilder,
    public formValidationService: FormValidationService,
    protected changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      formText: [''], // no validation at all
    });
    // this.formGroup.controls['formText'].setValue(null);
  }

  newValidator(mode: string, value?: string, param?: any) {
    if (value == null || value === undefined) {
      value = '';
    }
    this.mode = mode;
    switch (mode) {
      case 'required':
        this.formGroup = this.formBuilder.group({
          formText: [value, Validators.required],
        });
        break;
      case 'minLength':
        this.formGroup = this.formBuilder.group({
          formText: [value, Validators.minLength(param)],
        });
        break;
      case 'maxLength':
        this.formGroup = this.formBuilder.group({
          formText: [value, Validators.maxLength(param)],
        });
        break;
      case 'min':
        this.formGroup = this.formBuilder.group({
          formText: [value, Validators.min(param)],
        });
        break;
      case 'max':
        this.formGroup = this.formBuilder.group({
          formText: [value, Validators.max(param)],
        });
        break;
      case 'email':
        this.formGroup = this.formBuilder.group({
          formText: [value, Validators.email],
        });
        break;
      case 'pattern':
        this.formGroup = this.formBuilder.group({
          formText: [value, Validators.pattern(param)],
        });
        break;

      default:
        throw new Error('not supported mode ' + mode);
    }
  }

  setRemoteErrors(serverErrors: string[]) {
    this.mode = 'remote';
    this.formGroup = this.formBuilder.group({
      formText: [''],
    });

    this.changeDetectorRef.detectChanges();

    const errors = {
      validationErrorMessage: null,
      fieldErrors: {}
    };

    for (const key in this.formGroup.controls) {
      if (!this.formGroup.controls.hasOwnProperty(key)) {
        continue;
      }
      if (serverErrors != null) {
        errors.fieldErrors[key] = serverErrors;
      }
    }

    this.formValidationService.applyFieldErrorToFrom(this.formGroup, errors);
    this.changeDetectorRef.detectChanges();
  }
}

// unit test is looking for text in string eg. email, pattern, min, max etc
describe('UIErrorReporterComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        FormModule,
        ReactiveFormsModule,
        NotificationModule,
        ValidationModule,
        ToastrModule.forRoot({ // read more here - https://github.com/scttcper/ngx-toastr
          timeOut: 10000,
          positionClass: 'toast-top-right',
          preventDuplicates: true,
        }),
      ],
      declarations: [
        TestComponent,
      ]
    })
      .compileComponents();
  }));

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    tick(100);
    fixture.detectChanges();
  }));

  it('should create', fakeAsync(() => {
    expect(component).toBeTruthy();
    const valid = component.formGroup.valid;
    expect(valid).toBeTruthy();
  }));

  describe('Validator types for form', () => {
    it('show a required text', fakeAsync(() => {
      component.newValidator('required');
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const valid = component.formGroup.valid;
      expect(valid).toBeFalsy();

      const errorText = getErrorDataText('required');
      expect(fixture.nativeElement.outerHTML).toContain(errorText);
    }));

    it('should be recognised as email', fakeAsync(() => {
      component.newValidator('email');
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const valid = component.formGroup.valid;
      expect(valid).toBeTruthy();

      const errorText = getErrorDataText('email');
      expect(fixture.nativeElement.outerHTML).toContain(errorText);
    }));

    it('should have correct email pattern', fakeAsync(() => {
      component.newValidator('pattern');
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const valid = component.formGroup.valid;
      expect(valid).toBeTruthy();

      const errorText = getErrorDataText('pattern');
      expect(fixture.nativeElement.outerHTML).toContain(errorText);
    }));

    it('show MIN Length text characters', fakeAsync(() => {
      component.newValidator('minLength');
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const valid = component.formGroup.valid;
      expect(valid).toBeTruthy();

      const errorText = getErrorDataText('minlength');
      expect(fixture.nativeElement.outerHTML).toContain(errorText);
    }));

    it('show MAX Length text characters', fakeAsync(() => {
      component.newValidator('maxLength');
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const valid = component.formGroup.valid;
      expect(valid).toBeTruthy();

      const errorText = getErrorDataText('maxlength');
      expect(fixture.nativeElement.outerHTML).toContain(errorText);
    }));

    it('show remote errors', fakeAsync(() => {
      const err = [TestingTools.randomString(), TestingTools.randomString(), TestingTools.randomString()];
      component.setRemoteErrors(err);
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const valid = component.formGroup.valid;
      expect(valid).toBeFalsy();

      const documentText = fixture.nativeElement.outerHTML;
      for (const item of err) {
        expect(documentText).toContain(item);
      }
    }));
  });
});

function getErrorDataText(name: string): string {
  for (const item of UIErrorReporterComponent.errorsData) {
    if (item.name === name) {
      return item.text;
    }
  }
  throw new Error(name + ' not found in the error data text ');
}
