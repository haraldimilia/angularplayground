import { Component, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { ElementSize, InputGroup } from '../uibasic';
import { UIBasicComponent } from '../uibasic.component';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  // tslint:disable-next-line
  selector: 'ui-textinput', // we want the selector to start with "ui-"
  templateUrl: './uitextinput.component.html',
  styleUrls: ['./uitextinput.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: UITextInputComponent, multi: true }
  ]
})
export class UITextInputComponent extends UIBasicComponent<string> {
  private _numericMode = false;

  @ViewChild(UIErrorReporterComponent) errorReporter: UIErrorReporterComponent;
  @Input() visible = true;
  @Input() disabled = false;
  @Input() static = false;
  @Input() label = '';
  @Input() help = '';
  @Input() htmlId: string = UIBasicComponent.uniqueHtmlId();
  @Input() horizontal: boolean;
  @Input() horizontalcols: number;
  @Input() size: ElementSize = ElementSize.Default;
  @Input() inputgroup: InputGroup = InputGroup.Invisible;
  @Input() value: string = null;

  @Input() placeholder = '';
  @Input() length: number = null;
  @Input() password: boolean = null;
  @Input() autocomplete = false;

  // masked input
  // https://www.npmjs.com/package/ngx-mask
  @Input() mask?: string = null;
  @Input() patterns?: any = null;
  @Input() specialCharacters ?: string[] = null;
  @Input() dropSpecialCharacters?: boolean = null;
  @Input() clearIfNotMatch?: boolean = null;

  @Input()
  get numericMode(): boolean {
      return this._numericMode;
  }
  set numericMode(value: boolean) {
    if (value) {
      this.mask = '0000000000000';
      this.patterns = null;
      this.specialCharacters = null;
      this.dropSpecialCharacters = true;
      this.clearIfNotMatch = null;
    } else {
      this.mask = null;
      this.patterns = null;
      this.specialCharacters = null;
      this.dropSpecialCharacters = true;
      this.clearIfNotMatch = null;
    }
    this._numericMode = value;
  }

  public registerOnChange(fn: any): void {
    // wrapper for on change
    // no action when we are in a no edit mode
    this.onChange = (value: string) => {
      if (this.static === undefined ||
        this.static === null ||
        this.static === true) {
        return;
      }
      if (this.disabled === undefined ||
        this.disabled === null ||
        this.disabled === true) {
        return;
      }

      if (this.numericMode) {
        if (value !== null && value !== undefined && String(value).trim().length > 0) {
          value = <any>Number(value); // sort of a hack but this makes everybody the hanlding of numerics more easy
        }
      }

      fn(value);
    };
  }

}
