import { Component, ViewChild } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../../container/container.module';
import { ValidationModule } from '../../validation/validation.module';

import { By } from '@angular/platform-browser';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';
import { UITextInputComponent } from './uitextinput.component';

@Component({
  template: `
    <ui-textinput #item
      [visible]="visible"
      [disabled]="disabled"
      [static]="static"
      [label]="label"
      [help]="help"
      [(ngModel)]="value"
      [ngModelOptions]="{standalone: true}"
      >
    </ui-textinput>
`
})
export class TestComponent {
  @ViewChild(UITextInputComponent) item: UITextInputComponent;
  visible = true;
  disabled = false;
  static = false;
  label = '';
  help = '';
  value: string = null;
}

describe('UITextInputComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
      ],
      declarations: [
        UIErrorReporterComponent,
        UITextInputComponent,
        TestComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('form basic', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should create a label', () => {
      const label = TestingTools.randomString();
      component.label = label;
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('label'));
      expect(element.nativeElement.textContent).toContain(label);
      expect(component).toBeTruthy();
    });
  });

  describe('visibility', () => {
    it('it should be not visible in invisible mode', async(() => {
      const label = TestingTools.randomString();
      component.label = label;
      component.visible = false;
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('label'));
      expect(element).toBeNull();
    }));
  });

  describe('static', () => {
    it('should have no input when static', async(() => {
      component.static = true;
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('input'));
      expect(element).toBeNull();
    }));

    it('it should have an input when not static', fakeAsync(() => {
      component.static = false;
      fixture.detectChanges();
      tick(100); // Simulates the asynchronous passage of time for the timers in the fakeAsync zone
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('input'));
      expect(element).not.toBeUndefined();
      expect(element).not.toBeNull();
    }));
  });

  describe('html ids', () => {
    it('it should create an automatic unique htmlid', async(() => {
      const html_id = component.item.htmlId;
      expect(html_id).not.toBeNull();
    }));

    it('it should set predictable html id to child elements', async(() => {
      const html_id = TestingTools.randomString();
      const input_id = html_id + '_input';
      component.item.htmlId = html_id;
      fixture.detectChanges();
      const element = fixture.debugElement.query(By.css('input'));
      expect(element.nativeElement.outerHTML).toContain(input_id);
    }));

    it('it should set static html id when static', async(() => {
      const html_id = TestingTools.randomString();
      const input_id = html_id + '_static';
      component.item.htmlId = html_id;
      component.static = true;
      fixture.detectChanges();
      expect(fixture.elementRef.nativeElement.outerHTML).toContain(input_id);
    }));

    it('it should not set input html id when static', async(() => {
      const html_id = TestingTools.randomString();
      const input_id = html_id + '_input';
      component.item.htmlId = html_id;
      component.static = true;
      fixture.detectChanges();
      const element = fixture.debugElement.query(By.css('input'));
      expect(element).toBeNull(); // no input at all
    }));

    it('it should set predictable html id to help elements', async(() => {
      const html_id = TestingTools.randomString();
      const help = TestingTools.randomString();
      const help_id = html_id + '_help';
      component.item.help = help;
      component.item.htmlId = html_id;
      fixture.detectChanges();
      expect(fixture.elementRef.nativeElement.outerHTML).toContain(help_id);
    }));
  });

  describe('data input', () => {
    it('should create an input', fakeAsync(() => {
      const input = TestingTools.randomString();
      component.value = input;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('input'));
      expect(element.nativeElement.outerHTML).toContain(input);
    }));
  });

  describe('specifict parts', () => {
    it('should create placeholder', fakeAsync(() => {
      const input = TestingTools.randomString();
      component.value = input;
      const placeholder = TestingTools.randomString();
      component.item.placeholder = placeholder;

      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('input'));
      expect(element.nativeElement.outerHTML).toContain(placeholder);
    }));

    it('should return the text on input', fakeAsync(() => {
      const input = TestingTools.randomString();
      component.value = input;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const changedinput = TestingTools.randomString();

      const inputEl = fixture.debugElement.query(By.css('input')).nativeElement;
      inputEl.value = changedinput;
      inputEl.dispatchEvent(new Event('input')); // https://github.com/angular/angular/issues/22606
      inputEl.dispatchEvent(new Event('change')); // we are listening to "change" events
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      expect(component.value).not.toBe(input); // value must flip
      expect(component.value).toBe(changedinput);
    }));
  });
});
