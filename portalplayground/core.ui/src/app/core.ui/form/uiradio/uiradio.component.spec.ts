import { Component, ViewChild } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../../container/container.module';
import { ValidationModule } from '../../validation/validation.module';

import { By } from '@angular/platform-browser';
import { KeyValueItem } from '../keyvalueitem';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';
import { UIRadioComponent } from './uiradio.component';


@Component({
  template: `
  <ui-radio #item
    [visible]="visible"
    [disabled]="disabled"
    [static]="static"
    [label]="label"
    [help]="help"
    [(ngModel)]="value"
    [ngModelOptions]="{standalone: true}"
    [items]="items"
    >
  </ui-radio>
`
})
export class TestComponent {
  @ViewChild(UIRadioComponent) item: UIRadioComponent;
  visible = true;
  disabled = false;
  static = false;
  label = '';
  help = '';
  value: string = null;
  items: KeyValueItem[] = [];
}

describe('UIRadioComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
      ],
      declarations: [
        UIErrorReporterComponent,
        UIRadioComponent,
        TestComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('form basic', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should create a label', () => {
      const label = TestingTools.randomString();
      component.label = label;
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('label'));
      expect(element.nativeElement.textContent).toContain(label);
      expect(component).toBeTruthy();
    });
  });

  describe('static', () => {
    it('should have no input when static', async(() => {
      const disabled = TestingTools.randomString();
      component.static = true;
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('input'));
      expect(element).toBeNull();
    }));
  });

  describe('visibility', () => {
    it('it should be not visible in invisible mode', fakeAsync(() => {
      const label = TestingTools.randomString();
      component.label = label;
      component.visible = false;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('label'));
      expect(element).toBeNull();
    }));
  });

  describe('elements', () => {
    it('it should display elements for items', async(() => {
      const items: KeyValueItem[] = [
        {
          label: 'Option one',
          value: TestingTools.randomString()
        },
        {
          label: 'Option two (preselected at beginning)',
          value: TestingTools.randomString()
        },
        {
          label: 'Option three is disabled',
          value: TestingTools.randomString(),
          disabled: true
        }
      ];
      component.items = items;
      fixture.detectChanges();

      const testValue = items[1].value;
      let itemFound = false;

      const elements = fixture.debugElement.queryAll(By.css('input'));
      for (const element of elements) {
        // console.log(element.nativeElement.outerHTML, testValue);
        if (String(element.nativeElement.outerHTML).indexOf(testValue) !== -1) {
          itemFound = true;
          break;
        }
      }
      expect(itemFound).toBeTruthy();
    }));
  });
});
