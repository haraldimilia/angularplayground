import { Component, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { ElementSize, InputGroup } from '../uibasic';
import { UIBasicComponent } from '../uibasic.component';

import { KeyValueItem } from '../keyvalueitem';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  // tslint:disable-next-line
  selector: 'ui-radio', // we want the selector to start with "ui-"
  templateUrl: './uiradio.component.html',
  styleUrls: ['./uiradio.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: UIRadioComponent, multi: true }
  ]
})
export class UIRadioComponent extends UIBasicComponent<string> {
  @ViewChild(UIErrorReporterComponent) errorReporter: UIErrorReporterComponent;
  @Input() visible = true;
  @Input() disabled = false;
  @Input() static = false;
  @Input() label = '';
  @Input() help = '';
  @Input() htmlId: string = UIBasicComponent.uniqueHtmlId();
  @Input() horizontal: boolean;
  @Input() horizontalcols: number;
  @Input() size: ElementSize = ElementSize.Default;
  @Input() inputgroup: InputGroup = InputGroup.Invisible;
  @Input() value: string = null;

  @Input() items: KeyValueItem[] = [];

  public registerOnChange(fn: any): void {
    this.onChange = (value: any) => {
      if (this.static === undefined ||
        this.static === null ||
        this.static === true) {
        return;
      }
      if (this.disabled === undefined ||
        this.disabled === null ||
        this.disabled === true) {
        return;
      }
      if (value) {
        if (value.disabled !== undefined &&
          value.disabled !== null &&
          value.disabled === true) {
          return;
        }
      }
      fn(value.value);
    };
  }
}
