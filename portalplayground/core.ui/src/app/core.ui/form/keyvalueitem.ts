export interface KeyValueItem {
  label: string;
  value: string;
  description?: string; // tooltip etc
  disabled?: boolean;
  visible?: boolean;
  htmlId?: string;
}
