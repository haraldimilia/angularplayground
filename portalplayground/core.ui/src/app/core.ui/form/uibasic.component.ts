import { Input, Self } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

import { ElementSize, InputGroup, UIBasic } from './uibasic';

import { FromGroupComponent } from '../container/fromgroup/fromgroup.component';

/* tslint:disable */
// http://almerosteyn.com/2016/04/linkup-custom-control-to-ngcontrol-ngmodel
// http://blog.thoughtram.io/angular/2016/07/27/custom-form-controls-in-angular-2.html
// https://blog.angularindepth.com/never-again-be-confused-when-implementing-controlvalueaccessor-in-angular-forms-93b9eee9ee83
// https://github.com/maximusk/custom-form-control-that-implements-control-value-accessor-and-wraps-jquery-slider/blob/master/src/app/ngx-jquery-slider.component.ts
/* tslint:enable */

export abstract class UIBasicComponent<T> implements UIBasic, ControlValueAccessor {
  private static unique = 0;

  // validation states
  isNgTouched: boolean = null;
  isNgUntouched: boolean = null;
  isNgDirty: boolean = null;
  isNgPristine: boolean = null;
  isNgValid: boolean = null;
  isNgInvalid: boolean = null;

  abstract errorReporter: any;

  abstract visible = true;
  abstract disabled = false;
  abstract static = false;
  abstract label = '';
  abstract help = '';
  private _htmlId = UIBasicComponent.uniqueHtmlId();
  @Input()
  get htmlId(): string {
      return this._htmlId;
  }
  set htmlId(value: string) {
    // make sure that we don't get a null value
    if (value == null || value === undefined) {
      return;
    }
    this._htmlId = value;
  }
  abstract horizontal = FromGroupComponent.Horizontal;
  abstract horizontalcols = FromGroupComponent.DefaultColums;
  abstract size = ElementSize.Default;
  abstract inputgroup = InputGroup.Invisible;
  abstract value: T = null;

  public static uniqueHtmlId(): string {
    return '___' + String(UIBasicComponent.unique++);
  }

  public onChange(value: T): void { }

  public onTouch(value: T): void { }

  public writeValue(value: T): void {
    this.value = value;
  }

  public registerOnChange(fn: any): void {
    // wrapper for on change
    // no action when we are in a no edit mode
    this.onChange = (value: T) => {
      if (this.static === undefined ||
        this.static === null ||
        this.static === true) {
        return;
      }
      if (this.disabled === undefined ||
        this.disabled === null ||
        this.disabled === true) {
        return;
      }
      fn(value);
    };
  }

  public registerOnTouched(fn: any): void { this.onTouch = fn; }
}
