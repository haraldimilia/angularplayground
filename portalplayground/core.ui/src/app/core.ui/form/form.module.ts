import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../container/container.module';
import { ValidationModule } from '../validation/validation.module';

import { UICheckBoxComponent } from './uicheckbox/uicheckbox.component';
import { UICheckBoxGroupComponent } from './uicheckboxgroup/uicheckboxgroup.component';
import { UIDatePickerComponent } from './uidatepicker/uidatepicker.component';
import { UIDropDownComponent } from './uidropdown/uidropdown.component';
import { UIDropDownMultiComponent } from './uidropdownmulti/uidropdownmulti.component';
import { UIErrorReporterComponent } from './uierrorreporter/uierrorreporter.component';
import { UIRadioComponent } from './uiradio/uiradio.component';
import { UITextInputComponent } from './uitextinput/uitextinput.component';
import { UITimePickerComponent } from './uitimepicker/uitimepicker.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ContainerModule,
    ValidationModule,
    NgxErrorsModule,
    NgxMaskModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
  ],
  declarations: [
    UITextInputComponent,
    UICheckBoxComponent,
    UICheckBoxGroupComponent,
    UIDropDownComponent,
    UIDatePickerComponent,
    UITimePickerComponent,
    UIDropDownMultiComponent,
    UIRadioComponent,
    UIErrorReporterComponent,
  ],
  exports: [
    UITextInputComponent,
    UICheckBoxComponent,
    UICheckBoxGroupComponent,
    UIDropDownComponent,
    UIDatePickerComponent,
    UITimePickerComponent,
    UIDropDownMultiComponent,
    UIRadioComponent,
    // UIErrorReporterComponent <- private component
  ]
})
export class FormModule { }
