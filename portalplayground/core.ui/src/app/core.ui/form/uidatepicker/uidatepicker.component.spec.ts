import { Component, ViewChild } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../../container/container.module';
import { ValidationModule } from '../../validation/validation.module';

import { By } from '@angular/platform-browser';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { UIDatePickerComponent } from './uidatepicker.component';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  template: `
    <ui-datepicker #item
      [disabled]="disabled"
      [label]="label"
      [(ngModel)]="value"
      [ngModelOptions]="'{standalone: true}'"
      >
    </ui-datepicker>
`
})

export class TestComponent {
  @ViewChild(UIDatePickerComponent) item: UIDatePickerComponent;
  visible = true;
  disabled = false;
  static = false;
  label = '';
  minDate?: Date = null;
  maxDate?: Date = null;
  value: any = 'xxxoooxxx';
}

fdescribe('UIDatePickerComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  const dpcomp = new UIDatePickerComponent();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
      ],
      declarations: [
        UIErrorReporterComponent,
        UIDatePickerComponent,
        TestComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  fit('should create', () => {
    expect(component).toBeTruthy();
  });

  fit('should create a label', () => {
    const label = TestingTools.randomString();
    component.label = label;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('label'));
    expect(element.nativeElement.textContent).toContain(label);
    expect(component).toBeTruthy();
  });

  xit('should be able to select a date', fakeAsync (() => {
    const date = new Date();
    component.value = date;
    fixture.detectChanges();
    tick(100);
    fixture.detectChanges();
    // debugger;

    const element = fixture.debugElement.query(By.css('date'));
    expect(element.nativeElement.textContent).toContain(date);
    expect(component).toBeTruthy();
  }));

  fit('should be able to select date (todays date)', fakeAsync (() => {
    const date = new Date();
    const dateString = date.toLocaleDateString('de-DE');

    fixture.detectChanges();
    tick(100);
    fixture.detectChanges();

    const expectedStartDate = moment(
        date.getUTCMonth() +
            '/' +
            date.getUTCDate() +
            '/' +
            date.getUTCFullYear()
    ).format('dd.mm.yyyy');

    expect(dateString).toBe('19.7.2018');
  }));

  // doesnt work - need help
  xit('should be able to select date (previous)', fakeAsync (() => {
    const date = new Date();
    const dateString = date.toLocaleDateString('de-DE');

    fixture.detectChanges();
    tick(100);
    fixture.detectChanges();

    const expectedStartDate = moment(
        date.getUTCMonth() +
            '/' +
            date.getUTCDate() +
            '/' +
            date.getUTCFullYear()
    ).format('dd.mm.yyyy');

    expect(dateString).toBe('');
  }));

  // todays date expecting to be todays date
  // working?
  fit('should display the selected date', fakeAsync (() => {
    const date = new Date();
    component.value = date;
    fixture.detectChanges();
    tick(100);
    fixture.detectChanges();
    expect(new Date).toContain(date);
  }));

  xit('should not be able to select future dates', () => {
    const date = new Date();
    component.value = date;
    fixture.detectChanges();
    tick(100);
    fixture.detectChanges();
  });

  // need to get line of code that picks an actual date
  fit('should return true if date is in the future', () => {
    const date = '18/07/2018';
    const result = dpcomp.checkDate(date);
    expect(result).toBeTruthy();
  });
});
