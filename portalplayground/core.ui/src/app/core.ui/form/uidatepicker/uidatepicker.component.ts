import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { ElementSize, InputGroup } from '../uibasic';
import { UIBasicComponent } from '../uibasic.component';

import { KeyValueItem } from '../keyvalueitem';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  // tslint:disable-next-line
  selector: 'ui-datepicker', // we want the selector to start with "ui-"
  templateUrl: './uidatepicker.component.html',
  styleUrls: ['./uidatepicker.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: UIDatePickerComponent, multi: true }
  ]
})
export class UIDatePickerComponent extends UIBasicComponent<Date> implements AfterViewInit {
  private viewInitalized = false;
  private _placeholder = '';

  @ViewChild(UIErrorReporterComponent) errorReporter: UIErrorReporterComponent;
  @Input() visible = true;
  @Input() disabled = false;
  @Input() static = false;
  @Input() label = '';
  @Input() help = '';
  @Input() htmlId: string = UIBasicComponent.uniqueHtmlId();
  @Input() horizontal: boolean;
  @Input() horizontalcols: number;
  @Input() size: ElementSize = ElementSize.Default;
  @Input() inputgroup: InputGroup = InputGroup.Invisible;
  @Input() value: Date = null;

  @Input() minDate?: Date = null;
  @Input() maxDate?: Date = null;

  @Input() get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    // placeholder seems to be broken for null / undefined
    if (value == null || value === undefined) {
      value = '';
    }
    this._placeholder = value;
  }

  ngAfterViewInit() {
    // sort of a bug in bsDatepicker
    // we have to wait until the view is initalized
    this.viewInitalized = true;
  }

  public registerOnChange(fn: any): void {
    this.onChange = (value: any) => {
      if (this.static === undefined ||
        this.static === null ||
        this.static === true) {
        return;
      }
      if (this.disabled === undefined ||
        this.disabled === null ||
        this.disabled === true) {
        return;
      }
      if (value) {
        if (value.disabled !== undefined &&
          value.disabled !== null &&
          value.disabled === true) {
          return;
        }
      }
      // give the control the time to initalize itself
      if (!this.viewInitalized) {
        return;
      }
      fn(value);
    };
  }

  // James fnx
  // The 'check_date' will always be in the format `dd.mm.yyyy`
  public checkDate(check_date: string): boolean {
    const today: any = new Date();
    const dateParts: any = check_date.split('.');
    const dateObject: any = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);

    if (dateObject.getTime() > today.getTime()) {
        return true;
    }

    return false;
  }

  // public valueChanged(value: string) {
  //   const element = document.querySelector('.foo');
  //   // set: here we write the value
  //   element.className = 'setting-a-property';
  //   // get: we read the value
  //   console.log(element.className); // 'setting-a-property';
  // }
}
