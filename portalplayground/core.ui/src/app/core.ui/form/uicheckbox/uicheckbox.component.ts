import { Component, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { ElementSize, InputGroup } from '../uibasic';
import { UIBasicComponent } from '../uibasic.component';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  // tslint:disable-next-line
  selector: 'ui-checkbox', // we want the selector to start with "ui-"
  templateUrl: './uicheckbox.component.html',
  styleUrls: ['./uicheckbox.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: UICheckBoxComponent, multi: true }
  ]
})
export class UICheckBoxComponent extends UIBasicComponent<boolean> {
  textvalue: string;
  isChecked: any;
  @ViewChild(UIErrorReporterComponent) errorReporter: UIErrorReporterComponent;
  @Input() visible = true;
  @Input() disabled = false;
  @Input() static = false;
  @Input() label = '';
  @Input() help = '';
  @Input() htmlId: string = UIBasicComponent.uniqueHtmlId();
  @Input() horizontal: boolean;
  @Input() horizontalcols: number;
  @Input() size: ElementSize = ElementSize.Default;
  @Input() inputgroup: InputGroup = InputGroup.Invisible;
  @Input() value: boolean = null;

  @Input() caption = '';
}
