import { Component, ViewChild } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingTools } from '../../../core.lib/testing/testingtools.class.spec';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';

import { ContainerModule } from '../../container/container.module';
import { ValidationModule } from '../../validation/validation.module';

import { By } from '@angular/platform-browser';
import { UICheckBoxComponent } from './uicheckbox.component';
import { UIErrorReporterComponent } from '../uierrorreporter/uierrorreporter.component';

@Component({
  template: `
  <ui-checkbox
    [visible]="visible"
    [disabled]="disabled"
    [static]="static"
    [label]="label"
    [help]="help"
    [(ngModel)]="value"
    [ngModelOptions]="{standalone: true}"
    >
  </ui-checkbox>
`
})
export class TestComponent {
  @ViewChild(UICheckBoxComponent) item: UICheckBoxComponent;
  visible = true;
  disabled = false;
  static = false;
  label = '';
  help = '';
  value: boolean = null;
}

// input is declared here but no value
describe('UICheckBoxComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        ContainerModule,
        ValidationModule,
        NgxErrorsModule,
        NgxMaskModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
      ],
      declarations: [
        UIErrorReporterComponent,
        UICheckBoxComponent,
        TestComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('form basic', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should create a label', () => {
      const label = TestingTools.randomString();
      component.label = label;
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('label'));
      expect(element.nativeElement.textContent).toContain(label);
      expect(component).toBeTruthy();
    });
  });

  describe('visibility', () => {
    it('it should be not visible in invisible mode', fakeAsync(() => {
      const label = TestingTools.randomString();
      component.label = label;
      component.visible = false;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('label'));
      expect(element).toBeNull();
    }));
  });

  describe('static', () => {
    it('should have a disabled input when static', fakeAsync(() => {
      component.static = true;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('input'));
      expect(element.nativeElement.disabled).toBe(true);
    }));

    it('it should have an input when not static', fakeAsync(() => {
      component.static = true;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('input'));
      expect(element.nativeElement.disabled).toBe(true);
    }));
  });

  describe('html ids', () => {
    it('it should create an automatic unique htmlid', async(() => {
      const html_id = component.item.htmlId;
      expect(html_id).not.toBeNull();
    }));

    it('it should set predictable html id to child elements', async(() => {
      const html_id = TestingTools.randomString();
      const input_id = html_id + '_input';
      component.item.htmlId = html_id;
      fixture.detectChanges();
      const element = fixture.debugElement.query(By.css('input'));
      expect(element.nativeElement.outerHTML).toContain(input_id);
    }));

    it('it should set input html id when static', async(() => {
      const html_id = TestingTools.randomString();
      const input_id = html_id + '_input';
      component.item.htmlId = html_id;
      component.static = true;
      fixture.detectChanges();
      expect(fixture.elementRef.nativeElement.outerHTML).toContain(input_id);
    }));

    it('it should set predictable html id to help elements', async(() => {
      const html_id = TestingTools.randomString();
      const help = TestingTools.randomString();
      const help_id = html_id + '_help';
      component.item.help = help;
      component.item.htmlId = html_id;
      fixture.detectChanges();
      expect(fixture.elementRef.nativeElement.outerHTML).toContain(help_id);
    }));
  });

  describe('data input', () => {
    it('should create an input', fakeAsync(() => {
      const input = true;
      component.value = input;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const element = fixture.debugElement.query(By.css('input'));
      expect(element.nativeElement.outerHTML).toContain(input);
      expect(component).toBeTruthy();
    }));

    it('should be checked when clicked', fakeAsync(() => {
      const startValue = false;
      component.value = startValue;
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const inputEl = fixture.debugElement.query(By.css('input')).nativeElement;
      inputEl.click(); // toggles the start value
      inputEl.dispatchEvent(new Event('input')); // https://github.com/angular/angular/issues/22606
      inputEl.dispatchEvent(new Event('change')); // we are listening to "change" events
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      expect(component.value).not.toBe(startValue); // value must flip
      expect(component.value).toBe(!startValue);
    }));
  });

  describe('specific parts', () => {
    it('should create a caption', fakeAsync(() => {
      const caption = TestingTools.randomString();
      component.item.caption = caption;

      fixture.detectChanges(); // initialize controls
      tick(100); // wait registration controls
      fixture.detectChanges();

      expect(fixture.nativeElement.outerHTML).toContain(caption);
    }));
  });
});

