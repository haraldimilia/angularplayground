import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ContainerModule } from '../container/container.module';
import { DynamicFormComponent } from './dynamicform/dynamicform.component';
import { FormModule } from '../form/form.module';
import { ValidationModule } from '../validation/validation.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormModule,
    ValidationModule,
    ContainerModule
  ],
  declarations: [
    DynamicFormComponent
  ],
  exports: [
    DynamicFormComponent
  ],
})
export class DynamicFormModule { }
