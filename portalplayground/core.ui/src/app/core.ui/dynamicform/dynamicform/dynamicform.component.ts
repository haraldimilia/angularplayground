import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { UIBasicComponent } from '../../form/uibasic.component';

import { FormDescription } from '../formdescription';
import { FormItem } from '../formitem';
import { FormType } from '../formtype';

import { KeyValueItem } from '../../form/keyvalueitem';
import { Log } from 'ng2-logger/client';
import { ElementSize, InputGroup } from '../../form/uibasic';

const log = Log.create('DynamicFormComponent');

@Component({
  // tslint:disable-next-line
  selector: 'ui-dynamic-form', // we want the selector to start with "ui-"
  templateUrl: './dynamicform.component.html',
  styleUrls: ['./dynamicform.component.scss']
})
export class DynamicFormComponent implements OnInit {
  formGroup: FormGroup;
  desc: FormDescription;
  buttons: FormItem[];
  kvpitems: { [kvpName: string]: KeyValueItem[] } = {};
  callback: (reason: string, valid: boolean, values: any, errors?: any) => any = null;

  constructor(private formBuilder: FormBuilder) {
    log.info('constructor');
  }

  ngOnInit() {
    this.init(null); // create empty form
  }

  init(desc?: FormDescription, callback?: (reason: string, valid: boolean, values: any, errors?: any) => any) {
    log.info('init', desc);

    if (desc == null || desc === undefined ||
      desc.items == null || desc.items === undefined ||
      desc.items.length === 0) {
      this.desc = null;
      this.buttons = null;
      this.kvpitems = {};
      this.callback = null;
      this.formGroup = this.formBuilder.group({});
      return;
    }

    // deep clone (as we modify the data)
    desc = JSON.parse(JSON.stringify(desc));

    let formName = desc.name;
    if (formName == null || formName === undefined) {
      formName = null;
    }

    const buttons: FormItem[] = [];
    let kvpitems: { [kvpName: string]: KeyValueItem[] } = {};
    const buttonTypes: FormType[] = [
      FormType.SubmitButton,
      FormType.Button
    ];
    const controlsConfig: any = {};
    const keyValuePairNames: string[] = [];

    if (desc.kvpitems != null && desc.kvpitems !== undefined) {
      kvpitems = desc.kvpitems;
    }

    for (const item of desc.items) {
      if (formName !== null && (item.htmlId == null || item.htmlId === undefined)) {
        // we have a form name and no given htmlId
        // create a systematic name
        item.htmlId = formName + '_' + item.name;
      }
      if (item.kvpName != null && item.kvpName !== undefined && item.kvpName.length > 0) {
        if (keyValuePairNames.indexOf(item.kvpName)) {
          // save this
          keyValuePairNames.push(item.kvpName);
        }
      }
      if (item.visible == null || item.visible === undefined) {
        item.visible = true;
      }
      if (item.disabled == null || item.disabled === undefined) {
        item.disabled = false;
      }
      if (item.static == null || item.static === undefined) {
        item.static = false;
      }
      if (item.size == null || item.size === undefined) {
        item.size = ElementSize.Default;
      }
      if (item.inputgroup == null || item.inputgroup === undefined) {
        item.inputgroup = InputGroup.Invisible;
      }
      // step over buttons
      if (buttonTypes.indexOf(item.type) !== -1) {
        buttons.push(item);
        continue;
      }

      let defaultValue: any = '';
      if (item.value !== null && item.value !== undefined) {
        defaultValue = item.value;
      }

      const config = [];

      // take care of disabled state
      if (item.disabled !== null && item.disabled !== undefined && item.disabled) {
        // check the documentation on formbuilder why we create a struct here
        defaultValue = {
          value: defaultValue,
          disabled: true
        };
      }
      config.push(defaultValue);

      if (item.constraints !== null && item.constraints !== undefined) {
        const constraints: any[] = [];

        if (item.constraints.min) {
          constraints.push(Validators.min(item.constraints.min));
        }
        if (item.constraints.max) {
          constraints.push(Validators.max(item.constraints.max));
        }
        if (item.constraints.required) {
          constraints.push(Validators.required);
        }
        if (item.constraints.email) {
          constraints.push(Validators.email);
        }
        if (item.constraints.minLength) {
          constraints.push(Validators.minLength(item.constraints.minLength));
        }
        if (item.constraints.maxLength) {
          constraints.push(Validators.maxLength(item.constraints.maxLength));
        }
        if (item.constraints.pattern) {
          constraints.push(Validators.pattern(item.constraints.pattern));
        }
        if (constraints.length === 1) {
          config.push(constraints[0]);
        }
        if (constraints.length > 1) {
          config.push(constraints);
        }
      }

      controlsConfig[item.name] = config;
    }

    if (keyValuePairNames.length > 0) {
      log.info('loading key value paris with the following key', keyValuePairNames);
    }

    this.formGroup = this.formBuilder.group(controlsConfig);
    this.kvpitems = kvpitems;
    this.buttons = buttons;
    this.desc = desc;
    this.callback = callback;
  }

  resolveKeyValuePair(keyName: string): KeyValueItem[] {
    if (keyName == null || keyName === undefined) {
      return;
    }

    if (this.kvpitems == null || this.kvpitems === undefined) {
      log.error('resolveKeyValuePair - trying to resolv key without kvpitems', keyName);
      return;
    }

    if (!this.kvpitems.hasOwnProperty(keyName)) {
      log.error('resolveKeyValuePair - no such key', keyName);
      return;
    }

    return this.kvpitems[keyName];
  }

  onSubmit(name: string) {
    log.info('onSubmit', name);

    if (!this.formGroup.valid) {

      // https://stackoverflow.com/questions/40680321/get-all-validation-errors-from-angular-2-formgroup
      const getErrors = (formGroup: FormGroup, errors: any = {}) => {
        Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
            errors[field] = control.errors;
          } else if (control instanceof FormGroup) {
            errors[field] = getErrors(control);
          }
        });
        return errors;
      };

      // Calling it:
      const formErrors = getErrors(this.formGroup);
      // log.error('form errors', formErrors);

      if (this.callback) {
        this.callback(name, false, this.formGroup.value, formErrors);
      }

      return;
    }

    // log.info('form values', this.formGroup.value);
    if (this.callback) {
      this.callback(name, true, this.formGroup.value, null);
    }
  }

}
