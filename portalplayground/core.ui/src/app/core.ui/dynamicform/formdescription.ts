import { FormItem } from './formitem';
import { KeyValueItem } from '../form/keyvalueitem';

export interface FormDescription {
  /**
   * if you set a name here, every form htmlId will be set to "name_" + item.name
   */
  name?: string;
  /**
   * auto completion for form
   */
  autocomplete?: boolean;
  showFlat?: boolean;
  cardLabel?: string;
  items: FormItem[];

  kvpitems?: { [kvpName: string]: KeyValueItem[] };
}
