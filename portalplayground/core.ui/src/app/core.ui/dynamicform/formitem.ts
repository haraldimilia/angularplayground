import { FormConstraint } from './formconstraint';
import { FormType } from './formtype';
import { ElementSize, InputGroup } from '../form/uibasic';

export interface FormItem  {
  /**
   * element name - this is an idenifyer so use: [a-z]*[A-Z]*[0-9]*
   */
  name: string;
  type: FormType;
  /**
   * initial value
   */
  value?: any;

  /**
   * key value pair name (this is needed for loading items for the combox)
   */
  kvpName?: string;

  // start: from UI Basic
  visible?: boolean;
  disabled?: boolean;
  static?: boolean;
  label?: string;
  help?: string;
  size?: ElementSize;
  inputgroup?: InputGroup;
  htmlId?: string;
  horizontal?: boolean;
  horizontalcols?: number;
  // end from UI Basic

  parameters?: { [index: string]: any };
  constraints?: FormConstraint;
}


