export interface FormConstraint {
  min?: number;
  max?: number;
  required?: boolean;
  email?: boolean;
  minLength?: number;
  maxLength?: number;
  pattern?: string;
}
