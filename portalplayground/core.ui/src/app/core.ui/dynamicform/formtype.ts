export enum FormType {
  Text = 'Text',
  CheckBox = 'CheckBox',
  CheckBoxGroup = 'CheckBoxGroup',
  DropDown = 'DropDown',
  Date = 'Date',
  Radio = 'Radio',
  Time = 'Time',
  Button = 'Button',
  SubmitButton = 'SubmitButton'
}
