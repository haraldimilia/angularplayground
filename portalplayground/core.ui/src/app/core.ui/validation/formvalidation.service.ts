import { Injectable, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ValidationError } from './validationerror';

import { NotificationService } from '../notification/notification.service';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  constructor(private notificationService: NotificationService) { }

  public applyFieldErrorToFrom(formGroup: FormGroup, errors: ValidationError, hideToast?: boolean) {
    let err = {};

    if (formGroup === undefined || formGroup === null ||
      formGroup.controls === undefined || formGroup.controls === null) {
      return;
    }

    if (errors !== undefined && errors !== null &&
      errors.fieldErrors !== undefined && errors.fieldErrors !== null) {
      err = errors.fieldErrors;
    }

    for (const key in formGroup.controls) {
      if (!formGroup.controls.hasOwnProperty(key)) {
        continue;
      }
      let formErrors = null;

      if (err[key] !== undefined) {
        formErrors = formGroup.controls[key].errors;
        if (formErrors == null || formErrors === undefined) {
          formErrors = {};
        }

        formErrors = {}; // we decided to replace any frontend error always with servererrors
        formErrors.remote = err[key];
      }
      formGroup.controls[key].setErrors(formErrors);
    }

    if (hideToast || errors.validationErrorMessage == null || errors.validationErrorMessage === undefined ) {
      return;
    }

    this.notificationService.errorToast(errors.validationErrorMessage);

  }

}
