import { Attribute, ChangeDetectorRef, Directive, Host, HostBinding, Input, Optional, Self, SkipSelf } from '@angular/core';
import { AbstractControl, ControlContainer, FormControlName, FormGroupDirective, NgControl } from '@angular/forms';

import { UIBasicComponent } from '../form/uibasic.component';
import { UIErrorReporterComponent } from '../form/uierrorreporter/uierrorreporter.component';

// http://www.damirscorner.com/blog/posts/20171013-AccessParentComponentFromAngularDirective.html
// https://stackoverflow.com/questions/46810764/angular-4-bootstrap-4-consolidating-validation-classes

@Directive({
  // tslint:disable-next-line
  selector:
    'ui-checkbox,ui-checkbox[formControlName],' +
    'ui-checkboxgroup,ui-checkboxgroup[formControlName],' +
    'ui-dropdown,ui-dropdown[formControlName],' +
    'ui-radio,ui-radio[formControlName],' +
    'ui-textinput,ui-textinput[formControlName],' +
    'ui-datepicker,ui-datepicker[formControlName],' +
    'ui-timepicker,ui-timepicker[formControlName]'
})
export class BSControlStatusDirective {
  private errorContainerIsPrepared = false;

  constructor(@Self() protected control: NgControl,
    @Attribute('formControlName') protected formControlName: string,
    @Optional() @Host() @SkipSelf() protected parent: ControlContainer,
    protected changeDetectorRef: ChangeDetectorRef) {
  }

  @HostBinding('class.ng-touched')
  get ngClassTouched(): boolean {
    if (this.control.control == null) {
      return false;
    }
    if (this.control.valueAccessor && this.control.valueAccessor instanceof UIBasicComponent) {
      (<UIBasicComponent<any>>(this.control.valueAccessor)).isNgTouched = this.control.control.touched;
      if (!this.errorContainerIsPrepared) {
        this.setupErrorContainer(<any>this.control.valueAccessor);
      }
    }
    return this.control.control.touched;
  }

  @HostBinding('class.ng-untouched')
  get ngClassUntouched(): boolean {
    if (this.control.control == null) {
      return false;
    }
    if (this.control.valueAccessor && this.control.valueAccessor instanceof UIBasicComponent) {
      (<UIBasicComponent<any>>(this.control.valueAccessor)).isNgUntouched = this.control.control.untouched;
      if (!this.errorContainerIsPrepared) {
        this.setupErrorContainer(<any>this.control.valueAccessor);
      }
    }
    return this.control.control.untouched;
  }

  @HostBinding('class.ng-dirty')
  get ngClassDirty(): boolean {
    if (this.control.control == null) {
      return false;
    }
    if (this.control.valueAccessor && this.control.valueAccessor instanceof UIBasicComponent) {
      (<UIBasicComponent<any>>(this.control.valueAccessor)).isNgDirty = this.control.control.dirty;
      if (!this.errorContainerIsPrepared) {
        this.setupErrorContainer(<any>this.control.valueAccessor);
      }
    }
    return this.control.control.dirty;
  }

  @HostBinding('class.ng-pristine')
  get ngClassPristine(): boolean {
    if (this.control.control == null) {
      return false;
    }
    if (this.control.valueAccessor && this.control.valueAccessor instanceof UIBasicComponent) {
      (<UIBasicComponent<any>>(this.control.valueAccessor)).isNgPristine = this.control.control.pristine;
      if (!this.errorContainerIsPrepared) {
        this.setupErrorContainer(<any>this.control.valueAccessor);
      }
    }
    return this.control.control.pristine;
  }

  @HostBinding('class.ng-valid')
  get ngClassValid(): boolean {
    if (this.control.control == null) {
      return false;
    }
    if (this.control.valueAccessor && this.control.valueAccessor instanceof UIBasicComponent) {
      (<UIBasicComponent<any>>(this.control.valueAccessor)).isNgValid = this.control.control.valid;
      if (!this.errorContainerIsPrepared) {
        this.setupErrorContainer(<any>this.control.valueAccessor);
      }
    }
    return this.control.control.valid;
  }

  @HostBinding('class.ng-invalid')
  get ngClassInvalid(): boolean {
    if (this.control.control == null) {
      return false;
    }
    if (this.control.valueAccessor && this.control.valueAccessor instanceof UIBasicComponent) {
      (<UIBasicComponent<any>>(this.control.valueAccessor)).isNgInvalid = this.control.control.invalid;
      if (!this.errorContainerIsPrepared) {
        this.setupErrorContainer(<any>this.control.valueAccessor);
      }
    }
    return this.control.control.invalid;
  }

  private setupErrorContainer(component: UIBasicComponent<any>) {
    if (this.errorContainerIsPrepared === true) {
      return;
    }
    this.errorContainerIsPrepared = true; // do this once and flag it

    if (!(this.parent instanceof FormGroupDirective)) {
      return;
    }
    const fgd: FormGroupDirective = this.parent;
    if (fgd.form == null || fgd.form === undefined) {
      return;
    }

    let formControlName = this.formControlName;
    if (formControlName == null || formControlName === undefined) {
      // can be null in case we used square brackets formControlName='name' vs [formControlName]="dynamicStuff()"
      if (!(this.control instanceof FormControlName)) {
        // assume an instance of from control name
        return;
      }
      formControlName = this.control.name;
    }

    if (formControlName == null || formControlName === undefined) {
      return;
    }

    if (component == null || component === undefined) {
      return;
    }

    if (!(component.errorReporter instanceof UIErrorReporterComponent)) {
      this.errorContainerIsPrepared = false; // we want to reevaluate - the area might not be visible
      return;
    }

    const errorReporter = <UIErrorReporterComponent>component.errorReporter;

    errorReporter.formGroup = fgd.form;
    errorReporter.formControlName = formControlName;
    this.changeDetectorRef.detectChanges(); // we modified the html - breath
  }
}
