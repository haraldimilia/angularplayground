export interface FieldError {
  [key: string]: string[];
}

export interface ValidationError {
  validationErrorMessage?: string;
  fieldErrors?: FieldError;
}
