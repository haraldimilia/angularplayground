import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormValidationService } from './formvalidation.service';

import { BSControlStatusDirective } from './bscontrolstatus.directive';

import { NotificationModule } from '../notification/notification.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NotificationModule
  ],
  declarations: [
    BSControlStatusDirective
  ],
  exports: [
    BSControlStatusDirective
  ],
  providers: [
    FormValidationService
  ]
})
export class ValidationModule { }
