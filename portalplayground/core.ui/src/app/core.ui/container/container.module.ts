import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CardComponent } from './card/card.component';
import { CardHeaderComponent } from './cardheader/cardheader.component';
import { FromGroupComponent } from './fromgroup/fromgroup.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    CardComponent,
    CardHeaderComponent,
    FromGroupComponent,
  ],
  exports: [
    CardComponent,
    CardHeaderComponent,
    FromGroupComponent,
  ]
})
export class ContainerModule { }
