import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line
  selector: 'ui-card-header', // we want the selector to start with "ui-"
  templateUrl: './cardheader.component.html',
  styleUrls: ['./cardheader.component.scss']
})
export class CardHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
