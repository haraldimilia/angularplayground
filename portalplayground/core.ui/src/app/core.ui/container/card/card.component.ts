import { Component, Input, OnInit } from '@angular/core';

// idea for the custom template:
// https://stackoverflow.com/questions/41593973/how-to-conditionally-wrap-a-div-around-ng-content


@Component({
  // tslint:disable-next-line
  selector: 'ui-card', // we want the selector to start with "ui-"
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() header: string = null;
  @Input() showFlat: boolean = null;

  constructor() { }

  ngOnInit() {
  }

}
