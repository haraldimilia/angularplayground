import { Component, Input, OnInit } from '@angular/core';

// https://stackoverflow.com/questions/41593973/how-to-conditionally-wrap-a-div-around-ng-content

@Component({
  // tslint:disable-next-line
  selector: 'ui-fromgroup', // we want the selector to start with "ui-"
  templateUrl: './fromgroup.component.html',
  styleUrls: ['./fromgroup.component.scss'],
})
export class FromGroupComponent implements OnInit {
  public static Horizontal = true;
  public static DefaultColums = 3;
  private _horizontalcols = FromGroupComponent.DefaultColums;
  private _horizontal: boolean = FromGroupComponent.Horizontal;
  @Input() htmlId: string;

  @Input() get horizontal(): boolean {
    return this._horizontal;
  }
  set horizontal(value: boolean) {
    if (value == null || value === undefined) {
      value = FromGroupComponent.Horizontal;
    }
    this._horizontal = value;
  }

  @Input() get horizontalcols(): number {
    return this._horizontalcols;
  }
  set horizontalcols(value: number) {
    if (value == null || value === undefined || value < 0 || value > 11) {
      value = FromGroupComponent.DefaultColums;
    }
    this._horizontalcols = value;
  }

  get labelCols(): string {
    if (!this.horizontal) { return ''; }
    return 'col-md-' + this.horizontalcols;
  }

  get contentCols(): string {
    if (!this.horizontal) { return ''; }
    const cols = 12 - this.horizontalcols;
    return 'col-md-' + cols;
  }

  get formGroup(): string {
    if (!this.horizontal) { return ''; }
    return 'row';
  }

  constructor() { }

  ngOnInit() {
  }

}

