import { Directive, ElementRef, Input, OnInit } from '@angular/core';

// idea from here: https://stackoverflow.com/questions/41873893/angular2-autofocus-input-element

@Directive({
  // tslint:disable-next-line
  selector: '[ui-autofocus]'// we want this selector"
})
export class AutofocusDirective implements OnInit {
  private initalized = false;
  private _autofocus;
  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    this.initalized = true;
    this.setFocus();
  }

  @Input('ui-autofocus') set autofocus(condition: boolean) {
    this._autofocus = condition !== false;
    this.setFocus();
  }

  private setFocus() {
    if (!this.initalized) {
      return;
    }

    if (this._autofocus || typeof this._autofocus === 'undefined') {
      this.el.nativeElement.focus();
    }
    // For SSR (server side rendering) this is not safe.
    // Use: https://github.com/angular/angular/issues/15008#issuecomment-285141070)
  }
}
