dir /al | find /i ".vscode" | find "<SYMLINKD>"  >nul || (
  del  /F /Q .vscode
  mklink /d .vscode ..\core.lib\.vscode
)

dir /al "src\app" | find /i "core.lib" | find "<SYMLINKD>" >nul || (
  del  /F /Q src\app\core.lib
  mklink /d src\app\core.lib ..\..\..\core.lib\src\app\core.lib
)
