# Core UI

ToDo:

- Modal Dialog Component
  -> Take from Bootstrap
  -> Take Service from ng-material <https://material.angular.io/components/dialog/overview>, <https://github.com/angular/material2>

let dialogRef = dialog.open(UserProfileComponent, {
  height: '400px',
  width: '600px',
});

dialogRef.afterClosed().subscribe(result => {
  console.log(`Dialog result: ${result}`); // Pizza!
});

dialogRef.close('Pizza!');

- Deal with "disabled" warning in dynamic form
  -> Add this into a directoive
  -> Get Rid of the validation module

- Button handler for Dynamic Form (next only if valid?, back always?, reset values?)
- Load Handler for Dynamic Form

- Service for detecting a mobile / pad device.

- Masked Input: https://www.npmjs.com/package/ngx-mask

- Datepicker: https://ng-bootstrap.github.io/#/components/datepicker/examples

- Timepicker: https://ng-bootstrap.github.io/#/components/timepicker/examples

- Modal Dialog (Boostrap): Works like Material https://ng-bootstrap.github.io/#/components/modal/examples

- Advanced Forms: https://coreui.io/angular/demo/#/forms/advanced-forms

