# Ideas from

<https://stackoverflow.com/questions/18387598/selenium-webdriver-click-google-search>

Screenshots + other ideas to be investigated
<https://www.protractortest.org/#/faq>


Selenium & Grid

<https://github.com/SeleniumHQ/docker-selenium>

$ docker run -d -p 4444:4444 --shm-size=2g --name selenium-docker --restart=always  selenium/standalone-chrome
$ docker run -d -p 4444:4444 -p 5900:5900 -e SCREEN_HEIGHT=1080 -e SCREEN_WIDTH=1920 -e SCREEN_DEPTH=24  --shm-size=2g --name selenium-docker --restart=always  selenium/standalone-chrome-debug
$ docker inspect selenium-docker
$ docker rm -fv selenium-docker

## screen size vm

<https://github.com/SeleniumHQ/docker-selenium/issues/220>
