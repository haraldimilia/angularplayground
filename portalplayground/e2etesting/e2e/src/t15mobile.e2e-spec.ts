import { Scenario1Page } from './pages/scenario1mobile';
import { StepTestPage } from './pages/steptest.page';
import { browser, by, ExpectedConditions, Button, $, element, $$ } from 'protractor';

describe('t15 mobile should work fine', () => {
  const test = new StepTestPage();
  const s1 = new Scenario1Page();

  beforeAll(() => {
    test.getPage();
  });

  // mobile view

  it('should find initial header', async () => {
    const mobileHead = await browser.wait( ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Terminangaben' ), 5000, 'Termin wählen not found');

    expect(mobileHead).toBeTruthy();
  })

  it('should select 6th option', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#purpDropdown')), 5000, 'dropdown not found');
    const option = await s1.getByXpath('//*[@id="purpDropdown"]/option[6]');
    await option.click();

  })

  it('should click search and display appointments', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[4]/button')) ), 5000, 'suchen button not found');
    const continueBtn = await s1.getByXpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[4]/button');
    continueBtn.click();

    // await browser.wait( ExpectedConditions.visibilityOf( $('.weekSlotTable')), 5000, 'weekslottable not found');
  })

  it('should click an appointment', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[4]/div[1]/button'))), 5000, 'appointment button not found');
    const appBtn = await s1.getByXpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[4]/div[1]/button');
    appBtn.click();

    await browser.wait( ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'Verifizierung not found');
  })

  it('should find email input and enter email', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#uEmail')), 5000, 'email field not found');

    const emailInput = await s1.getByID('uEmail');
    emailInput.sendKeys('g.purcell@hotmail.com');

    const prufen = await s1.getByID('verificationBtn');
    prufen.click();

    const codefield = await browser.wait( ExpectedConditions.visibilityOf( $('#emailverifcationcode')), 5000, 'codefield not found');
    expect(codefield).toBeTruthy();
  })

  it('should display terms and conditions check box', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#emailverifcationcode')), 5000, 'codefield not found');
    const emailInput = await s1.getByID('emailverifcationcode');
    emailInput.sendKeys('12345');

    const privacy = await browser.wait( ExpectedConditions.visibilityOf( $('.bookingStepPrivacy')), 5000, 'bookingStepPrivacy not found');
    expect(privacy).toBeTruthy();
  })

  it('should check box for terms and conditions', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#privacyCheck')), 5000, 'privacy Check box not found');
    const privacyCheck = await s1.getByID('privacyCheck');
    privacyCheck.click();

    const continueBtn = await s1.getByXpath('//*[@id="formAndIdent"]/div[10]/button');
    await expect(continueBtn.isEnabled()).toBeTruthy();
  })

  it('should click button and carry onto contact form', async () => {
    const continueBtn = await s1.getByXpath('//*[@id="formAndIdent"]/div[10]/button');
    continueBtn.click();

    const newTitle = await browser.wait( ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Bitte geben Sie Ihre Daten ein' ), 5000, 'timerbeeMobileHeader not changed');
    expect(newTitle).toBeTruthy();
  })

  it('should find name fields and fill them out ', async () => {
    const firstName = await s1.getByID('firstName');
    firstName.sendKeys('Joe');
  })

  it('should find name fields and fill them out ', async () => {
    const lastName = await s1.getByID('surname');
    lastName.sendKeys('Bloggs');
  })

  it('on load "Termin buchen" should be disabled', async () => {
    const submitBtn = await s1.getByXpath('//*[@id="formAndMobileIdent"]/div[12]/button');

    expect(submitBtn.isEnabled()).toBeFalsy();
  })

  it('should choose gender ', async () => {
    // const firstName = await browser.element(by.id('genderDropdown'));
    const option = await s1.getByXpath('//*[@id="genderDropdown"]/option[3]');
    await option.click();
  })

  xit('should find dob field ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#dobPat') ), 5000, 'dob field not found');
    const field = await s1.getByID('dobPat');
    expect(field).toMatch('(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}');
  })

  it('should enter Date ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#dobPat') ), 5000, 'dob field not found');
    const field = await s1.getByID('dobPat');
    field.sendKeys('02.06.1995');
  })

  it('should enter address ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#street') ), 5000, 'street field not found');
    const field = await s1.getByID('street');
    field.sendKeys('Naumannstraße');
  })
  it('should enter housenumber ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#houseNo') ), 5000, 'houseNo field not found');
    const field = await s1.getByID('houseNo');
    field.sendKeys('30');
  })
  it('should enter plz ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#portalPlzInput') ), 5000, 'portalPlzInput not found');
    const field = await s1.getByID('portalPlzInput');
    field.sendKeys('10829');
  })
  it('should enter ort ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#portalCityInput') ), 5000, 'portalCityInput field not found');
    const field = await s1.getByID('portalCityInput');
    field.sendKeys('Berlin');
  })

  it('should choose insurance ', async () => {
    const option = await s1.getByXpath('//*[@id="insuranceProv"]/option[2]');
    await option.click();
  })

  it('should check email field ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#userEmail') ), 5000, 'userEmail field not found');
    const emailfield = await s1.getByID('userEmail');
    expect(emailfield.isEnabled()).toBeFalsy();

    const emailValue = await browser.wait( ExpectedConditions.textToBePresentInElementValue( $('#userEmail'), 'g.purcell@hotmail.com'), 5000, 'email does not match');
    expect(emailValue).toBeTruthy();
  })

  it('should check email service check box ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#userReminderEmail') ), 5000, 'userEmail field not found');
    const emailcheckbox = await s1.getByID('userReminderEmail');
    expect(emailcheckbox.isSelected()).toBeTruthy();

  })

  it('should enter telephone ', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('#phone') ), 5000, 'phone field not found');
    const field = await s1.getByID('phone');
    field.sendKeys('00353891234567');
  })

  it('on load "Termin buchen" should be enabled', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('.portalMainButton') ), 5000, 'phone field not found');

    const submitBtn = await s1.getByXpath('//*[@id="formAndMobileIdent"]/div[12]/button');

    submitBtn.click();
  })

  /**
   *
   * WAMP ERROR PREVENTS TEST FROM CONTINUING ANY FURTHER
   *
  */

  // it('should be taken to confirmation page', async () => {
  //   const title = await browser.wait( ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Bestätigung' ), 5000, 'page title not found');

  //   expect(title).toBeTruthy();
  // })



  // it('should be display previously entered data', async () => {
  //   // const date = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), '26.06.2018'), 5000, 'date does not match');
  //   // expect(date).toBeTruthy();

  //   const purpose = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), 'Nachkontrolle'), 5000, 'purpose does not match');
  //   expect(purpose).toBeTruthy();

  //   const name = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), 'Joe Bloggs'), 5000, 'name does not match');
  //   expect(name).toBeTruthy();

  //   const dob = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), '02.06.1995'), 5000, 'dob does not match');
  //   expect(dob).toBeTruthy();

  //   const email = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), 'g.purcell@hotmail.com'), 5000, 'email does not match');
  //   expect(email).toBeTruthy();

  //   const phone = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), '00353891234567'), 5000, 'phone does not match');
  //   expect(phone).toBeTruthy();
  // })

  // it('should download appointment', async () => {
  //   await browser.wait( ExpectedConditions.visibilityOf( element(by.xpath('//*[@id="confirmation"]/div[3]')) ), 5000, 'button not found');
  //   const downloadBtn = await s1.getByXpath('//*[@id="confirmation"]/div[3]');
  //   downloadBtn.click();
  // })

  // it('should click thank you and take you back to start', async () => {
  //   await browser.wait( ExpectedConditions.visibilityOf( element(by.xpath('//*[@id="confirmation"]/div[4]/button')) ), 5000, 'button not found');
  //   const tyBtn = await s1.getByXpath('//*[@id="confirmation"]/div[4]/button');
  //   tyBtn.click();

  //   const nextpage = await browser.wait( ExpectedConditions.visibilityOf( $('#purpDropdown')), 5000);
  //   expect(nextpage).toBeTruthy();
  // })

});
