import { Scenario1Page } from './pages/scenario1mobile';
import { StepTestPage } from './pages/steptest.page';
import { Step1Page } from './pages/step1.page';
import { browser, by, ExpectedConditions, Button, $, element, $$ } from 'protractor';
import { SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG } from 'constants';
import { BlockingProxy } from 'blocking-proxy';
import { isFulfilled } from 'q';

describe('sprechstunde LH mobile should work fine', () => {
  const step1 = new Step1Page();
  const test = new StepTestPage();
  const s1 = new Scenario1Page();

  beforeAll(() => {
    test.getPage();
  });

  // mobile view

  it('should find initial header', async () => {
    const mobileHead = await browser.wait( ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Terminangaben' ), 5000, 'Termin wählen not found');

    expect(mobileHead).toBeTruthy();
  })

  it('should select 5th option and then select plan', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#purpDropdown')), 5000, 'dropdown not found');
    const option = await s1.getByXpath('//*[@id="purpDropdown"]/option[5]');
    await option.click();

    await browser.wait( ExpectedConditions.visibilityOf( $('#plan') ), 5000, 'planDropdown not found');
    const planDd = await s1.getByXpath('//*[@id="plan"]/option[2]');
    planDd.click();
  })

  it('should pick dob', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#purpDropdown')), 5000, 'dropdown not found');
    // const calendar = await s1.getByClass('calBtn');
    // await calendar.click();

    // await browser.wait( ExpectedConditions.visibilityOf( $('.dropdown-menu')), 5000, 'calendar not found');
    // const calDropdown = await s1.getByXpath('//*[@id="datepicker-6458-659-title"]');
    // calDropdown.click();

    // calDropdown.click();

    // const yearbtn = await s1.getByXpath('//*[@id="datepicker-6658-6211-7"]/button');
    // yearbtn.click();
    // const monthbtn = await s1.getByXpath('//*[@id="datepicker-6658-6211-4"]/button');
    // monthbtn.click();
    // const daybtn = await s1.getByXpath('//*[@id="datepicker-2245-676-11"]/button');
    // daybtn.click();

    await browser.wait( ExpectedConditions.visibilityOf( $('.inputField')), 5000, 'dob field not found');

    const dobfield = await s1.getByXpath('//*[@id="attributeInput"]/form/div/div/div/div/div/input');
    dobfield.sendKeys('08.05.2008');

    // const datefield = await browser.wait( ExpectedConditions.textToBePresentInElement($('.inputField'), '08.05.2008'), 5000, 'Date not entered correctly');
    // expect(datefield).toBeTruthy();
  })

  it('should click continue and show message', async () => {
    const weiter = await s1.getByXpath('//*[@id="attributeInput"]/form/button');
    expect(weiter.isEnabled()).toBeTruthy();

    weiter.click();

    const alert = await browser.wait( ExpectedConditions.textToBePresentInElement( $('#portalLeftPanel'), 'Behandlungsplan enthält mehrere Ressourcen!'), 5000, 'note did not show');
    expect(alert).toBeTruthy();
  })
});

