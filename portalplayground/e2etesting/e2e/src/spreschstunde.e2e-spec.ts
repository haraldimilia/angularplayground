import { StepTestPage } from './pages/steptest.page';
import { Step1Page } from './pages/step1.page';
import { browser, by, ExpectedConditions, Button, $, element } from 'protractor';
import { SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG, EHOSTUNREACH } from 'constants';
import { Scenario1Page } from './pages/scenario1mobile';

describe('step test should work fine', () => {
  const step1 = new Step1Page();
  const test = new StepTestPage();
  const s1 = new Scenario1Page();

  beforeAll(() => {
    test.getPage();
  });

  // XXXXXXXXXXXXXXXXXXXXXXXXX
  // XXXXX Spreschstunde XXXXX
  // XXXXXXXXXXXXXXXXXXXXXXXXX

  it('should have purpose drop down', async () => {
    const contains = await test.pageContainsElement('purpDropdown');
    expect(contains).toBeTruthy();
  })

  it('should select 4th (Spreschstunde option)', async () => {
    await test.getPage();

    await browser.wait(ExpectedConditions.visibilityOf($('#purpDropdown')));
    const optionIndex = await browser.element(by.xpath('//*[@id="purpDropdown"]/option[4]'));
    optionIndex.click();
  })

  it('Should select a DOB from datepicker', async () => {
    await browser.wait(ExpectedConditions.visibilityOf(element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[1]/tb-op-attribute-input/div/form/div[1]/div/div/div/div/input'))), 5000, 'DOB not found');

    const dobField = browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[1]/tb-op-attribute-input/div/form/div[1]/div/div/div/div/input'));
    dobField.sendKeys('21.08.1995');
  })

  it('Should select 2nd (normal option)', async () => {
    await browser.wait(ExpectedConditions.visibilityOf(element(by.xpath('//*[@id="attributeInput"]/form/div[2]/div/div/select'))), 5000, 'choiceIndex not found');

    const choiceIndex = await browser.element(by.xpath('//*[@id="attributeInput"]/form/div[2]/div/div/select/option[2]'));
    choiceIndex.click();
  })

  it('should click continue / weiter', async () => {
    const continueButton = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[1]/tb-op-attribute-input/div/form/button'));
    expect(continueButton.isEnabled()).toBeTruthy();
    continueButton.click();
  })

  it('should click search', async () => {
    const suchen = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[5]/div/button'));
    expect(suchen.isEnabled()).toBeTruthy();
    suchen.click();

    await browser.wait(ExpectedConditions.visibilityOf($('#slotTables')), 5000, 'slottables not found');
    await browser.wait(ExpectedConditions.visibilityOf(element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[1]/div/div[2]/table/tbody/tr/td[5]/div[4]/button'))), 5000, 'appointment not found');
    const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[1]/div/div[2]/table/tbody/tr/td[5]/div[4]/button'));
    app.click();

    expect(browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[1]/button')).isDisplayed()).toBeTruthy();
  })

  it('should click button and carry onto contact form', async () => {
    await browser.wait(ExpectedConditions.visibilityOf(element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[1]/button'))), 5000, 'buchen btn not found');

    const continueBtn = browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[1]/button'));
    continueBtn.click();
  })

  it('should find email input and enter email', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#uEmail')), 5000, 'email field not found');

    const emailInput = browser.element(by.id('uEmail'));
    emailInput.sendKeys('j-123@hotmail.com');

    const prufen = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[1]/div[1]/div[2]/button'));
    prufen.click();

    const codefield = await browser.wait(ExpectedConditions.visibilityOf($('#emailverifcationcode')), 5000, 'codefield not found');
    expect(codefield).toBeTruthy();

    const cfield = await browser.element(by.id('emailverifcationcode'));
    cfield.sendKeys('12345');

    const checkbox = await browser.wait(ExpectedConditions.visibilityOf($('#privacy')), 5000, 'privacy checkbox not found');
    expect(checkbox).toBeTruthy();
    const tnc = browser.element(by.id('privacy'));
    tnc.click();
  })

  it('should check box for terms and conditions', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#agb')), 5000, 'Terms and C checkbox not found');

    const privacyCheck = browser.element(by.id('agb'));
    privacyCheck.click();
  })

  it('should click button and carry onto contact form', async () => {
    const continueBtn = browser.element(by.className('portalMainButton')).isEnabled();
    expect(continueBtn).toBeTruthy();

    const weiter = browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[1]/div[3]/button'));
    weiter.click();

    const newTitle = await browser.wait(ExpectedConditions.textToBePresentInElement(element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[2]/table/tbody/tr[1]/td[2]')), 'Ihr Termin'), 5000, 'Ihr termin not found');
    expect(newTitle).toBeTruthy();
  })

  it('should find firstname field and fill the out', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#firstName')), 5000, 'firstname field not found');
    const firstNameField = browser.element(by.id('firstName'));

    firstNameField.sendKeys('James');
    expect(firstNameField).toBeTruthy();
  })

  it('should find lastname field and fill the out', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#surname')), 5000, 'surname field not found');
    const lastNameField = browser.element(by.id('surname'));

    lastNameField.sendKeys('Hatt');
    expect(lastNameField).toBeTruthy();
  })

  it('should have gender dropdown & choose available gender', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#genderDropdown')), 5000, 'genderDropdown field not found');
    const option = await browser.element(by.xpath('//*[@id="genderDropdown"]/option[3]'))
    option.click();
  })

  it('should find D.O.B field and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#dobPat')), 5000, 'D.O.B field not found');
    const dobField = browser.element(by.id('dobPat'));

    dobField.sendKeys('21.08.1995');
  })

  it('should have streetInput field and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#street')), 5000, 'Street field not found');
    const streetInput = await browser.element(by.id('street'));

    streetInput.sendKeys('Möckernstraße');
  })

  it('should have HouseNo field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#houseNo')), 5000, 'HouseNo field not found');
    const houseNo = await browser.element(by.id('houseNo'));

    houseNo.sendKeys('71');
  })

  it('should have Plz Input field and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#portalPlzInput')), 5000, 'PlzInput field not found');
    const plzNo = await browser.element(by.id('portalPlzInput'));

    plzNo.sendKeys('10965');
  })

  it('should have City Input field and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#portalCityInput')), 5000, 'City Input field not found');
    const cityInput = await browser.element(by.id('portalCityInput'));

    cityInput.sendKeys('Berlin');
  })

  it('should have an insurance drop down field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#insuranceProv')), 5000, 'insuranceProvider dropdown field not found');
    const option = await browser.element(by.id('insuranceProv'));
    option.click();

    const contains = await element(by.xpath('//*[@id="insuranceProv"]/option[3]'));
    expect(contains).toBeTruthy();
  })

  it('email should be disabled', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#personEmail')), 5000, 'Disbabled Email field not found');

    const continueBtn = await browser.element(by.id('personEmail')).isEnabled();
    expect(continueBtn).toBeFalsy();
  })

  it('email should contain previously entered email in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#personEmail')), 5000, 'Disbabled Email field not found');

    const emailValue = await browser.wait(ExpectedConditions.textToBePresentInElementValue($('#personEmail'), 'j-123@hotmail.com'), 5000, 'email does not match');
    expect(emailValue).toBeTruthy();
  })

  it('should have email service check box', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#reminderEmail')), 5000, 'Email checkbox not found');

    const contains = await browser.element(by.id('reminderEmail'));
    expect(contains).toBeTruthy();
  })

  it('checkbox should be checked and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#reminderEmail')), 5000, 'Email checkbox not found');

    const contains = await browser.element(by.id('reminderEmail')).isSelected();
    expect(contains).toBeTruthy();
  })

  it('should have the mobile input field and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#phone')), 5000, 'phone inoput field not found');
    const contains = await browser.element(by.id('phone'));

    contains.sendKeys('353831963235');
  })

  it('should have submit button', async () => {
    const nextBtn = await browser.element(by.className('portalMainButton'));
    expect(nextBtn).toBeTruthy();
  })

  it('"Anfrage senden" should now enable after all required is filled', async () => {
    const continueBtn = await browser.element(by.className('portalMainButton')).isEnabled();
    await expect(continueBtn).toBeTruthy();
  })

  // xit('should show thank you page', async () => {
  //   const continueBtn = browser.element(by.className('portalMainButton'));
  //   continueBtn.click();

  //   const btnValue = await browser.wait(ExpectedConditions.textToBePresentInElementValue($('#portalMainButton'), 'Danke'), 5000, 'email does not match');
  //   expect(btnValue).toBeTruthy();
  // })

  // xit('should show successfully created Title', async () => {
  //   const newTitle = await browser.wait(element(by.className('bookingStepInfoText')).isDisplayed(), 5000, 'cannot find title');
  //   expect(newTitle).toBeTruthy();
  // })

  // xit('should display Confirmation Table Holder', async () => {
  //   const table = await browser.wait(element(by.className('confirmationTableHolder')).isDisplayed(), 5000, 'cannot find table');
  //   expect(table).toBeTruthy();
  // })

  // fit('should be display previously entered data on confirmation table holder', async () => {
  //   const date = await browser.wait(ExpectedConditions.textToBePresentInElement($('.confirmationTableHolder'), '26.06.2018'), 5000, 'date does not match');
  //   expect(date).toBeTruthy();

  //   const purpose = await browser.wait(ExpectedConditions.textToBePresentInElement($('.confirmationTableHolder'), 'LWS-MRT'), 5000, 'purpose does not match');
  //   expect(purpose).toBeTruthy();

  //   const name = await browser.wait(ExpectedConditions.textToBePresentInElement($('.confirmationTableHolder'), 'James Hatt'), 5000, 'name does not match');
  //   expect(name).toBeTruthy();

  //   const dob = await browser.wait(ExpectedConditions.textToBePresentInElement($('.confirmationTableHolder'), '21.08.1995'), 5000, 'dob does not match');
  //   expect(dob).toBeTruthy();

  //   const email = await browser.wait(ExpectedConditions.textToBePresentInElement($('.confirmationTableHolder'), 'james-hatt@hotmail.com'), 5000, 'email does not match');
  //   expect(email).toBeTruthy();

  //   const phone = await browser.wait(ExpectedConditions.textToBePresentInElement($('.confirmationTableHolder'), '353831963235'), 5000, 'phone does not match');
  //   expect(phone).toBeTruthy();
  // })

  // fit('should have a thank you button available', async () => {
  //   const thankyouBtn = await browser.element(by.className('portalMainButton'));
  //   thankyouBtn.click();

  //   const contains = await browser.element(by.className('portalMainButton'));
  //   expect(contains).toBeTruthy();
  // })

  // fit('should download to PC', async () => {
  //   const downloadBtn = browser.element(by.className('buttonDiv'));
  //   downloadBtn.click();

  //   const download = await browser.wait(ExpectedConditions.textToBePresentInElement($('.buttonDiv'), 'Buchungsbestätigung herunterladen '), 5000, 'buttonDiv not changed');
  //   expect(download).toBeTruthy();
  // })
});
