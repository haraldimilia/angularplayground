import { browser, by, element, $ } from 'protractor';

export class Step1Page {

  purpdd = $('#purpDropdown select');

  constructor() {
    // https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular-on-page-load
    browser.waitForAngularEnabled(false);
  }

  async getPage() {
    browser.get('#/portal/96944e36-56a3-41fc-8a5e-fc28201613eb');
  }

  getPageTitle() {
    return browser.getTitle();
  }

  getPurposeDropdown() {
    // const purpo = browser.element(by.id('purpDropdown')).isPresent();
    // console.log(purpo);
    return browser.element(by.id('purpDropdown'));
  }

  getOptionValue() {
    const optionIndex = element(by.xpath('//*[@id="purpDropdown"]/option[4]'));
    return optionIndex;
  }
  selectOptionValue() {
    return this.getOptionValue().click;
  }

  getDate() {
    const timing = element(by.className('portalDatePickerTable portalDatePickerTableOdd'));
    return timing;
  }

  // XPATH for indiviual date within datepicker
  getIndiviualDate() {
    const date = element(by.xpath('//*[@id="bookingOptions"]/div[2]/tb-portal-date-picker-simple/div/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td[4]'));
    return date;
  }

  getFormTitle() {
    const h5 = browser.element(by.tagName('h5'));
    return h5;
  }

  getDaysofWeekContainer() {
    return browser.element(by.className('portalWeekdayPickerDaysTable'));
  }

  getDays() {
    let countDays = 0;
    let i;
    for (i = 0; i < 7; i++) {
      const td = browser.element(by.xpath('//*[@id="bookingOptions"]/div[4]/tb-portal-weekday-picker/div/table/tbody/tr/td[' + i + ']'));
      // if(browser.element(by.xpath('//*[@id="bookingOptions"]/div[4]/tb-portal-weekday-picker/div/table/tbody/tr/td['+i+']'))){
      if (td) {
        countDays++;
      }
    }
    if (countDays === 7) {
      return true;
    }
    return false;
  }

  getCheckedDays() {
    let countDays = 0;
    let i;
    for (i = 1; i < 8; i++) {
      const input = browser.element(by.xpath('//*[@id="bookingOptions"]/div[4]/tb-portal-weekday-picker/div/table/tbody/tr/td[' + i + ']/div[2]/input'));
      if (input.isSelected) {
        countDays++;
      }
    }
    if (countDays === 7) {
      return true;
    }
    return false;
  }

  getDayIntervals() {
    return browser.element(by.className('portalPartOfDayPickerTable'));
  }

  getIntervals() {
    let countIntervals = 0;
    let i;
    for (i = 0; i < 3; i++) {
      const td = browser.element(by.xpath('//*[@id="bookingOptions"]/div[5]/tb-timeofday-button/div/table/tbody/tr/td[' + i + ']'));

      if (td) {
        countIntervals++;
      }
    }
    if (countIntervals === 3) {
      return true;
    }
    return false;
  }

  getCheckedIntervals() {
    let countIntervals = 0;
    let i;
    for (i = 1; i < 4; i++) {
      const input = browser.element(by.xpath('//*[@id="bookingOptions"]/div[5]/tb-timeofday-button/div/table/tbody/tr/td[' + i + ']/div[2]/input'));

      if (input.isSelected) {
        countIntervals++;
      }
    }
    if (countIntervals === 3) {
      return true;
    }
    return false;
  }

  getWeekTitle() {
    const title = browser.element(by.className('monthLabel'));
    return title;
  }

  getClickLeft() {
    const left = browser.element(by.xpath('//*[@id="previewMonthSelection"]/button[1]'));
    return left;
  }

  getClickRight() {
    const right = browser.element(by.xpath('//*[@id="previewMonthSelection"]/button[2]'));
    return right;
  }

  getDisplayTitleRight() {
    const titleRight = browser.element(by.className('monthLabel'));
    return titleRight;
  }

  getAppointmentTimes() {
    const timing = element(by.Id('previewDaySelection'));
  	return timing;
  }

  async pageContainsText(str: string) {
    // https://stackoverflow.com/questions/11454798/how-can-i-check-if-some-text-exist-or-not-in-the-page-using-selenium
    const bodyText = await browser.element(by.tagName("body")).getText();
    return bodyText.indexOf(str) != -1;
  }

  async sleep(ms: number) {
    await browser.driver.sleep(ms);
  }
}
