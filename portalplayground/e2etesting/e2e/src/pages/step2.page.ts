import { browser, by, element } from 'protractor';

export class Step2Page {
  constructor() {
    // https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular-on-page-load
    browser.waitForAngularEnabled(false);
  }

	async getPage() {
		browser.get('#/portal/96944e36-56a3-41fc-8a5e-fc28201613eb');
	}

	getPageTitle() {
		return browser.getTitle();
  }

}
