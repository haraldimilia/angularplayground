import { browser, by, element, $, ExpectedConditions } from 'protractor';

export class Scenario1Page {

  constructor() {
    // https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular-on-page-load
    browser.waitForAngularEnabled(false);
  }

  async getPage() {
    browser.get('#/portal/96944e36-56a3-41fc-8a5e-fc28201613eb');
  }

  getPageTitle() {
    return browser.getTitle();
  }

  async getByXpath(XPath) {
    return browser.element(by.xpath(XPath));
  }

  async getByID(ID) {
    return browser.element(by.id(ID));
  }

  async getByClass(Class) {
    return browser.element(by.className(Class));
  }

  getDaysofWeekContainer() {
    return browser.element(by.className('portalWeekdayPickerDaysTable'));
  }

  getDays() {
    let countDays = 0;
    let i;
    for (i = 0; i < 7; i++) {
      const td = browser.element(by.xpath('//*[@id="bookingOptions"]/div[4]/tb-portal-weekday-picker/div/table/tbody/tr/td[' + i + ']'));
      // if(browser.element(by.xpath('//*[@id="bookingOptions"]/div[4]/tb-portal-weekday-picker/div/table/tbody/tr/td['+i+']'))){
      if (td) {
        countDays++;
      }
    }
    if (countDays === 7) {
      return true;
    }
    return false;
  }

  getCheckedDays() {
    let countDays = 0;
    let i;
    for (i = 1; i < 8; i++) {
      const input = browser.element(by.xpath('//*[@id="bookingOptions"]/div[4]/tb-portal-weekday-picker/div/table/tbody/tr/td[' + i + ']/div[2]/input'));
      if (input.isSelected) {
        countDays++;
      }
    }
    if (countDays === 7) {
      return true;
    }
    return false;
  }

  getDayIntervals() {
    return browser.element(by.className('portalPartOfDayPickerTable'));
  }

  getIntervals() {
    let countIntervals = 0;
    let i;
    for (i = 0; i < 3; i++) {
      const td = browser.element(by.xpath('//*[@id="bookingOptions"]/div[5]/tb-timeofday-button/div/table/tbody/tr/td[' + i + ']'));

      if (td) {
        countIntervals++;
      }
    }
    if (countIntervals === 3) {
      return true;
    }
    return false;
  }

  getCheckedIntervals() {
    let countIntervals = 0;
    let i;
    for (i = 1; i < 4; i++) {
      const input = browser.element(by.xpath('//*[@id="bookingOptions"]/div[5]/tb-timeofday-button/div/table/tbody/tr/td[' + i + ']/div[2]/input'));

      if (input.isSelected) {
        countIntervals++;
      }
    }
    if (countIntervals === 3) {
      return true;
    }
    return false;
  }

  async getAppointmentSlots(noappdialog) {
    if (noappdialog) {
      // find next day button
      const nextWeek = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[1]/button[2]'));
      browser.wait(nextWeek.click(), 10000, '');

      if (browser.findElements(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[1]/div'))) {
        const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[1]/div[1]'));
        app.click();
        await browser.wait(ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'title not found');

      } else if (browser.findElements(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[2]/div'))) {
        const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[2]/div[1]'));
        app.click();
        await browser.wait(ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'title not found');
      }else if (browser.findElements(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[3]/div'))) {
        const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[3]/div[1]'));
        app.click();
        await browser.wait(ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'title not found');
      }else if (browser.findElements(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[3]/div'))) {
        const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[3]/div[1]'));
        app.click();
        await browser.wait(ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'title not found');
      }else if (browser.findElements(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[4]/div'))) {
        const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[4]/div[1]'));
        app.click();
        await browser.wait(ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'title not found');
      }else if (browser.findElements(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[5]/div'))) {
        const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[5]/div[1]'));
        app.click();
        await browser.wait(ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'title not found');
      }else if (browser.findElements(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[6]/div'))) {
        const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[6]/div[1]'));
        app.click();
        await browser.wait(ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'title not found');
      }else if (browser.findElements(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[7]/div'))) {
        const app = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[7]/div[3]/tb-slot-week-preview/div[2]/div/div[2]/table/tbody/tr/td[7]/div[1]'));
        app.click();
        await browser.wait(ExpectedConditions.textToBePresentInElement( $('#timerbeeMobileHeader'), 'Verifizierung'), 5000, 'title not found');
      }
    } else {
    }
  }

  async pageContainsText(str: string) {
    // https://stackoverflow.com/questions/11454798/how-can-i-check-if-some-text-exist-or-not-in-the-page-using-selenium
    const bodyText = await browser.element(by.tagName("body")).getText();
    return bodyText.indexOf(str) != -1;
  }

  async sleep(ms: number) {
    await browser.driver.sleep(ms);
  }
}
