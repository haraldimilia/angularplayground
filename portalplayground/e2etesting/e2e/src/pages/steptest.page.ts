import { browser, by, ExpectedConditions, element, $ } from 'protractor';

export class StepTestPage {


  constructor() {
    // https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular-on-page-load
    browser.waitForAngularEnabled(false);
  }

  async getPage() {
    browser.get('#/portal/96944e36-56a3-41fc-8a5e-fc28201613eb');
  }

  getPageTitle() {
    return browser.getTitle();
  }

  getPurposeDropdown() {
    // const purpo = browser.element(by.id('purpDropdown')).isPresent();
    // console.log(purpo);
    return browser.element(by.id('purpDropdown'));
  }

  getByXpath(xpath) {
    return browser.element(by.Xpath(xpath));
  }

  async getOptionValue() {
    await browser.wait( ExpectedConditions.visibilityOf( $('#purpDropdown') ));
    const optionIndex = await element(by.xpath('//*[@id="purpDropdown"]/option[6]'));
    return optionIndex;
  }

  async getOptionValueOne() {
    await browser.wait( ExpectedConditions.visibilityOf( $('#genderDropdown') ));
    const optionIndex = await element(by.xpath('//*[@id="genderDropdown"]/option[2]'));
    return optionIndex;
  }

  async getOptionValueTwo() {
    await browser.wait( ExpectedConditions.visibilityOf( $('#purpDropdown') ));
    const optionIndex = await element(by.xpath('//*[@id="purpDropdown"]/option[5]'));
    return optionIndex;
  }

  async getAppointmentButton() {
    await browser.wait( ExpectedConditions.visibilityOf( $('#slotPreviewHolder') ));
    const appBtn = await element.all(by.xpath('//*[@id="previewDaySelection"]/table/tbody/tr/td[5]/div[10]/button')).first();
    return appBtn;
  }

  async pageContainsText(str: string) {
    // https://stackoverflow.com/questions/11454798/how-can-i-check-if-some-text-exist-or-not-in-the-page-using-selenium
    const bodyText = await browser.element(by.tagName("body")).getText();
    return bodyText.indexOf(str) != -1;
  }

  async pageContainsElement(ele: string) {
    // https://stackoverflow.com/questions/11454798/how-can-i-check-if-some-text-exist-or-not-in-the-page-using-selenium
    const body = await browser.element(by.tagName("body"));
    return body.element(by.className(ele));
  }

  async sleep(ms: number) {
    await browser.driver.sleep(ms);
  }
}
