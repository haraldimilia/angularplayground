import { StepTestPage } from './pages/steptest.page';
import { Step1Page } from './pages/step1.page';
import { browser, by, ExpectedConditions, Button, $, element } from 'protractor';
import { SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG, EHOSTUNREACH } from 'constants';
import { Scenario1Page } from './pages/scenario1mobile';

describe('step test should work fine', () => {
  const step1 = new Step1Page();
  const test = new StepTestPage();
  const s1 = new Scenario1Page();

  beforeAll(() => {
    test.getPage();
  });

  // XXXXXXXXXXXXXXXXXXXXXXXXXXXX
  // XXXXXX NACHKONTROLLE XXXXXXX
  // XXXXXXXXXXXXXXXXXXXXXXXXXXXX

  it('should have purpose drop down', async () => {
    const contains = await test.pageContainsElement('purpDropDown');
    expect(contains).toBeTruthy();
  })

  it('should select 3th option-nachkontrolle', async () => {
    await test.getPage();

    await browser.wait( ExpectedConditions.visibilityOf( $('#purpDropdown')), 5000, 'purp dropdown not found');
    const option = await browser.element(by.xpath('//*[@id="purpDropdown"]/option[3]'));
    await option.click();

    const codefield = await browser.wait(ExpectedConditions.textToBePresentInElement($('#portalLeftPanel'), 'Startdatum der Terminanfrage'), 5000, 'appointment info not found');
    expect(codefield).toBeTruthy();
  })

  it('should click search', async () => {
    const suchen = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[5]/div/button'));
    expect(suchen.isEnabled()).toBeTruthy();
    suchen.click();
  })

  it('continue button should be disabled until everything is okay', async () => {
    await browser.wait(ExpectedConditions.visibilityOf(element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[1]/div[3]'))), 5000, 'weiter button not found');

    const continueBtn = browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[1]/div[3]')).isEnabled();
    await expect(continueBtn).toBeTruthy();
  })

  it('should find email input and enter email', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#uEmail')), 5000, 'email field not found');

    const emailInput = await s1.getByID('uEmail');
    emailInput.sendKeys('james-hatt@hotmail.com');

    const prufen = await browser.element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[1]/div[1]/div[4]/button'));
    prufen.click();

    const codefield = await browser.wait( ExpectedConditions.visibilityOf( $('#emailverifcationcode')), 5000, 'codefield not found');
    expect(codefield).toBeTruthy();
  })

  it('should display terms and conditions check box', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#emailverifcationcode')), 5000, 'codefield not found');
    const emailInput = await s1.getByID('emailverifcationcode');
    emailInput.sendKeys('12345');

    const privacy = await browser.wait( ExpectedConditions.visibilityOf( $('.bookingStepPrivacy')), 5000, 'bookingStepPrivacy not found');
    expect(privacy).toBeTruthy();
  })

  it('should check box for terms and conditions', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#privacy')), 5000, 'privacy Check box not found');

    const checkOne = await s1.getByID('privacy');
    checkOne.click();

    const checkTwo = await s1.getByID('agb');
    checkTwo.click();

    const continueBtn = await s1.getByXpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[1]/div[3]/button');
    await expect(continueBtn.isEnabled()).toBeTruthy();
  })

  it('should click button and carry onto contact form', async () => {
    const continueBtn = await s1.getByXpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[1]/div[3]');
    continueBtn.click();

    const newTitle = await browser.wait( ExpectedConditions.textToBePresentInElement( element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[2]/table/tbody/tr[1]/td[2]')), 'Ihre Termin'), 5000, 'Ihre termin not found');
    expect(newTitle).toBeTruthy();
  })

  it('should find name fields and fill them out ', async () => {
    const firstName = await s1.getByID('firstName');
    firstName.sendKeys('Joe');
  })

  it('should find name fields and fill them out ', async () => {
    const lastName = await s1.getByID('surname');
    lastName.sendKeys('Bloggs');
  })

  it('should have gender dropdown & choose available gender', async () => {
    await browser.wait( ExpectedConditions.visibilityOf($('#genderDropdown')), 5000, 'genderDropdown field not found');
    const option = await browser.element(by.xpath('//*[@id="genderDropdown"]/option[3]'))
    option.click();
  })

  it('should find D.O.B field and fill in field', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#dobPat')), 5000, 'D.O.B field not found');
    const dobField = browser.element(by.id('dobPat'));

    dobField.sendKeys('21.08.1995');
  })

  it('should have streetInput field and fill in field', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#street')), 5000, 'Street field not found');
    const streetInput = await browser.element(by.id('street'));

    streetInput.sendKeys('Möckernstraße');
  })

  it('should have HouseNo field', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( $('#houseNo')), 5000, 'HouseNo field not found');
    const houseNo = await browser.element(by.id('houseNo'));

    houseNo.sendKeys('71');
  })

  it('should have Plz Input field and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#portalPlzInput')), 5000, 'PlzInput field not found');
    const plzNo = await browser.element(by.id('portalPlzInput'));

    plzNo.sendKeys('10965');
  })

  it('should have City Input field and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#portalCityInput')), 5000, 'City Input field not found');
    const cityInput = await browser.element(by.id('portalCityInput'));

    cityInput.sendKeys('Berlin');
  })

  it('should have an insurance drop down field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#insuranceProv')), 5000, 'insuranceProvider dropdown field not found');
    const option = await element(by.xpath('//*[@id="insuranceProv"]/option[3]'));
    option.click();


    // expect(contains).toBeTruthy();
  })

  it('email should be disabled', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#personEmail')), 5000, 'Disbabled Email field not found');

    const continueBtn = await browser.element(by.id('personEmail')).isEnabled();
    expect(continueBtn).toBeFalsy();
  })

  // not working and need to test this
  xit('email should contain previously entered email in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#personEmail')), 5000, 'Disbabled Email field not found');

    const emailValue = await browser.wait( ExpectedConditions.textToBePresentInElementValue( element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[2]/table/tbody/tr[2]/td/div[2]')), 'jj@hot.com'), 7000, 'email does not match');
    expect(emailValue).toBeTruthy();
  })

  it('should have email service check box', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#reminderEmail')), 5000, 'Email checkbox not found');

    const contains = await browser.element(by.id('reminderEmail'));
    expect(contains).toBeTruthy();
  })

  it('checkbox should be checked and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#reminderEmail')), 5000, 'Email checkbox not found');

    const contains = await browser.element(by.id('reminderEmail')).isSelected();
    expect(contains).toBeTruthy();
  })

  it('should have the mobile input field and fill in field', async () => {
    await browser.wait(ExpectedConditions.visibilityOf($('#phone')), 5000, 'phone input field not found');
    const contains = await browser.element(by.id('phone'));

    contains.sendKeys('353831963235');
  })

  it('on load "Anfrage Senden" should be enabled', async () => {
    await browser.wait( ExpectedConditions.presenceOf( $('.portalMainButton') ), 5000, 'anfrage senden not found');

    const submitBtn = await s1.getByXpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[2]/div');

    submitBtn.click();
  })

  xit('should be taken to confirmation page', async () => {
    const title = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.portalMainButton'), 'Danke' ), 5000, 'Danke not found');

    expect(title).toBeTruthy();
  })

  it('should be display previously entered data', async () => {
    // const date = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), '26.06.2018'), 5000, 'date does not match');
    // expect(date).toBeTruthy();

    // const purpose = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), 'Nachkontrolle'), 5000, 'purpose does not match');
    // expect(purpose).toBeTruthy();

    const name = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), 'Joe Bloggs'), 5000, 'name does not match');
    expect(name).toBeTruthy();

    const dob = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), '21.08.1995'), 5000, 'dob does not match');
    expect(dob).toBeTruthy();

    const email = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), 'james-hatt@hotmail.com'), 5000, 'email does not match');
    expect(email).toBeTruthy();

    const phone = await browser.wait( ExpectedConditions.textToBePresentInElement( $('.confirmationTableHolder'), '353831963235'), 5000, 'phone does not match');
    expect(phone).toBeTruthy();
  })

  // doesnt work becuase its only viewable on mobile screens ????
  xit('should download appointment', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( element(by.xpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div/ng-form/div[4]/div[3]/translate')) ), 5000, 'button not found');
    const downloadBtn = await s1.getByXpath('/html/body/div[1]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div/ng-form/div[4]/div[3]/translate');
    downloadBtn.click();
  })

  it('should click thank you and take you back to start', async () => {
    await browser.wait( ExpectedConditions.visibilityOf( element(by.xpath('//*[@id="timerbee"]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[3]/div[3]/button')) ), 5000, 'button not found');
    const tyBtn = await s1.getByXpath('//*[@id="timerbee"]/div[3]/div[2]/div/div/div/div[8]/tb-booking-steps-view/div/div[2]/ng-form/div[3]/div[3]/button');
    tyBtn.click();

    const nextpage = await browser.wait( ExpectedConditions.visibilityOf( $('#purpDropdown')), 5000);
    expect(nextpage).toBeTruthy();
  })
});
