import { Component, OnInit } from '@angular/core';
import { Log } from 'ng2-logger/client';

const log = Log.create('AppComponent');

import { PortalSessionService } from './wamp';

@Component({
  // tslint:disable-next-line
  selector: 'body', // coreui theme needs this
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  errorText = '';

  constructor(private portalSessionService: PortalSessionService) {
    log.info('constructor');
  }

  ngOnInit() {
    log.info('ngOnInit');
    const uuid = '96944e36-56a3-41fc-8a5e-fc28201613eb';
    this.portalSessionService.portalLogin(uuid)
    .subscribe(
      (ok: boolean) => {
        log.info('portalLogin', ok);
        if (!ok) {
          this.errorText = 'portalLogin failed';
          return;
        }
        this.portalSessionService.loadBaseData()
        .subscribe(
          (data: boolean) => {
            log.info('loadBaseData', data);
            if (!data) {
              this.errorText = 'loadBaseData failed';
            }
          },
          (err: any) => {
            log.error('loadBaseData error', err);
            this.errorText = err;
          }
        );
      },
      (err: any) => {
        log.error('portalLogin error', err);
        this.errorText = err;
      }
    );
  }
}
