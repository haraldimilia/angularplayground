import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { LoggerConfigModule } from './core.lib';

import { AppComponent } from './app.component';
import { WampPortalModule } from './wamp';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    LoggerConfigModule,
    WampPortalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
