import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServerConfig } from './dto/serverconfig';

import { PlatformLocation } from '@angular/common';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ServerConfigService {
  public static baseUrl = '/';

  constructor(private httpClient: HttpClient, platformLocation: PlatformLocation) {
    // depending on the enviormnent we need to change the baseUrl
    // console.log((platformLocation as any).location.href);
    ServerConfigService.baseUrl = 'http://localhost:8080/'; // timerbee eclipse default
  }

  getServerInfo(): Observable<ServerConfig> {
    const url = ServerConfigService.baseUrl + 'server/config';

    return this.httpClient.get<ServerConfig>(url).pipe(
      catchError(this.handleError));
  }

  private handleError(err: any) {
    let errorMessage = '';
    if (err.error instanceof Error) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

}
