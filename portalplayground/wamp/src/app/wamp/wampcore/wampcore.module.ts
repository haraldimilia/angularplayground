import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ZoneSchedulerModule } from 'ngx-zone-scheduler';

import { WampService } from './wamp.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ZoneSchedulerModule
  ],
  declarations: [
  ],
  providers: [
    WampService,
  ],
  exports: [
  ]
})
export class WampCoreModule { }
