export interface ServerConfig {
  url: string;
  realm: string;
  username: string;
  password: string;
  casperEnabled: boolean;
}
