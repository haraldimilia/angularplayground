import { inject, TestBed } from '@angular/core/testing';

import { ZoneSchedulerModule } from 'ngx-zone-scheduler';

import { WampService } from './wamp.service';

describe('WampService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ZoneSchedulerModule
      ],
      providers: [WampService]
    });
  });

  it('should be created', inject([WampService], (service: WampService) => {
    expect(service).toBeTruthy();
  }));
});
