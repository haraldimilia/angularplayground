import { from } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import { Injectable } from '@angular/core';

import { Connection, IConnectionOptions, Session } from 'autobahn';

import { Log } from 'ng2-logger/client';
import { ZoneScheduler } from 'ngx-zone-scheduler';
import { ServerConfig } from '..';

const log = Log.create('WampService');

interface CallContext {
  username: string;
  tenantOid: number;
}

@Injectable()
export class WampService {
  private static instance: WampService;
  public static serverConfig: ServerConfig;
  private _tenantOid: number = null;
  private connection: Connection;
  private session: Session;
  private callContext: CallContext;

  get tenantOid(): number {
    return this._tenantOid;
  }

  set tenantOid(value: number) {
    if (this.session) {
      this._tenantOid = value;
    }
    if (this.callContext) {
      this.callContext.tenantOid = value;
    }
  }

  constructor(private scheduler: ZoneScheduler) {
    // https://stackoverflow.com/questions/36198785/how-do-i-create-a-singleton-service-in-angular-2
    // force this as singleton
    if (WampService.instance) {
      log.info('constructor -> singleton');
      return WampService.instance;
    }

    log.info('constructor');
    return this;
  }

  public loginByUserName(username: string, password: string): Observable<boolean> {
    log.info('loginByUserName', username, '***');

    const authmethod = 'oauth2.password.timerbee';
    const options: IConnectionOptions = {
      url: this.getUrl(),
      realm: WampService.serverConfig.realm,
      authmethods: [authmethod],
      authid: username,
      onchallenge: (session: Session, method: string, extra: any): string => {
        log.info('onchallenge');
        return password;
      }
    };

    return this.login(username, options);
  }

  public loginByUUID(uuid: string): Observable<boolean> {
    log.info('loginByUUID', uuid);

    const authmethod = 'uuid.auth';
    const options: IConnectionOptions = {
      url: this.getUrl(),
      realm: WampService.serverConfig.realm,
      authmethods: [authmethod],
      authid: uuid
    };

    return this.login(uuid, options);
  }

  public wampCall<TResult>(procedure: string, args?: any[] | any): Observable<TResult> {
    // https://stackoverflow.com/questions/31706948/angular2-view-not-changing-after-data-is-updated
    // https://github.com/wmaurer/rxjs-ng-extras
    // https://www.npmjs.com/package/ngx-zone-scheduler

    if (this.session == null || this.session === undefined) {
      return Observable.throw('no wamp session');
    }

    // treat undefined / empty
    if (args == null || args === undefined) {
      args = [];
    }

    // ensure array
    if (!Array.isArray(args)) {
      const arg = args;
      args = [];
      args.push(arg);
    }

    const modifiedArgs: any = [];

    for (const arg of args) {
      if (arg instanceof Date) {
        // no idea if we need this
        const date = <Date>arg;
        modifiedArgs.push(date.toISOString());
      } else if (arg === Object(arg)) {
        // objects need to be converted to json string
        modifiedArgs.push(JSON.stringify(arg));
      } else {
        modifiedArgs.push(arg);
      }
    }

    // always add the caller context as 1st parameter
    modifiedArgs.splice(0, 0, JSON.stringify(this.callContext));

    return from(
      <any>this.session.call<TResult>(procedure, modifiedArgs),
      <any>this.scheduler
    );
  }

  private getUrl() {
    // Default development/test configuration
    let proto = 'ws';

    if (window.location.protocol === 'https:') {
      proto = 'wss';
    }

    let url = proto + '://' + window.location.hostname + ':8444/wamp';	// Server url - replace with Server URL

    if (WampService.serverConfig.url != null && WampService.serverConfig.url !== undefined &&
      WampService.serverConfig.url !== '') {
      // overwrite from server
      url = WampService.serverConfig.url;
    }

    return url;
  }

  private login(username: string, options: IConnectionOptions): Observable<boolean> {
    log.info('login', username);

    return new Observable<boolean>((observer) => {
      this.closeConnection();
      this.connection = new Connection(options);
      this.connection.onopen = (session: Session, details: any) => {
        log.info('onopen' /*, session, details*/);
        this.session = session;
        this.callContext = {
          username: username,
          tenantOid: this.tenantOid
        };
        observer.next(true);
        observer.complete();
      };

      this.connection.onclose = (reason: string, details: any): boolean => {
        log.info('onclose', reason, details);
        if (!details || details.will_retry == null || details.will_retry === undefined || details.will_retry === false) {
          this.connection = null;
          this.closeConnection();
          observer.error(reason + ' - ' + details);
          observer.complete();
          return true;
        } else {
          log.info('onclose - retrying');
          return false;
        }
      };
      this.connection.open();
    });
  }

  private closeConnection() {
    this.session = null;
    this.callContext = null;
    this._tenantOid = null;
    if (this.connection) {
      this.connection.close('login');
    }
    this.connection = null;
  }

}
