import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TimelineQuery } from './dto/timelinequery';
import { TimeOfDayInterval } from './dto/timeofdayinterval';
import { TimerbeeCalendar } from './dto/timerbeecalendar';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class TimelineService {
  constructor(private wampService: WampService) { }

  public getDefaultTimeOfDayIntervals(): Observable<TimeOfDayInterval[]> {
    const __method = 'com.imilia.timerbee.timeline.getDefaultTimeOfDayIntervals';
    return this.wampService.wampCall<string>(
    __method
    ).map((json: string): TimeOfDayInterval[] => JSON.parse(json));
  }

  public getTimerbeePurposeCalendar(usergroupoid: number, purposeOid: number, searchDate: Date): Observable<TimerbeeCalendar> {
    const __method = 'com.imilia.timerbee.timeline.getTimerbeePurposeCalendar';
    return this.wampService.wampCall<string>(
      __method, [usergroupoid, purposeOid, searchDate]
    ).map((json: string): TimerbeeCalendar => JSON.parse(json));
  }

  public getFreeSlotWeekTimeLine(timelineQuery: TimelineQuery, locale: string): Observable<TimerbeeCalendar> {
    const __method = 'com.imilia.timerbee.timeline.getFreeSlotWeekTimeLine';
    const freeslotRequestJSON = JSON.stringify(timelineQuery);
    return this.wampService.wampCall<string>(
      __method, [freeslotRequestJSON, locale]
    ).map((json: string): TimerbeeCalendar => JSON.parse(json));
  }
}

