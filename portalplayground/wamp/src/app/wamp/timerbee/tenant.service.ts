import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { ResourceGroup } from './dto/resourcegroup';
import { Observable } from 'rxjs/Observable';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class TenantService {
  constructor(private wampService: WampService) { }

  public getTenantResourceGroups(tenantOid: number): Observable<ResourceGroup[]> {
    const __method = 'com.imilia.timerbee.tenant.getTenantResourceGroups';
    return this.wampService.wampCall<string>(
      __method, tenantOid
    ).map((json: string): ResourceGroup[] => JSON.parse(json));
  }
}
