import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { DomainRole } from './dto/domainrole';
import { Observable } from 'rxjs/Observable';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class DomainroleService {
  constructor(private wampService: WampService) { }

  public getPatientDomainRole(): Observable<DomainRole> {
    const __method = 'com.imilia.timerbee.domainrole.getPatientDomainRole';
    return this.wampService.wampCall<string>(
      __method
    ).map((json: string): DomainRole => JSON.parse(json));
  }
}
