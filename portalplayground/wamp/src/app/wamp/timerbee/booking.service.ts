import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Booking } from './dto/booking';
import { Observable } from 'rxjs/Observable';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class BookingService {
  constructor(private wampService: WampService) { }

  public createBooking(booking: Booking): Observable<string> {
    const __method = 'com.imilia.timerbee.booking.createBooking';
    return this.wampService.wampCall<string>(
      __method, booking
    ).map((json: string): string => JSON.parse(json));
  }
}
