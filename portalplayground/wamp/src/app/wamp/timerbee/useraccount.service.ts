import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { UserAccount } from './dto/useraccount';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class UseraccountService {
  constructor(private wampService: WampService) { }

  public getUserAccountByUsername(username: string): Observable<UserAccount> {
    const __method = 'com.imilia.timerbee.useraccount.getUserAccountByUsername';
    return this.wampService.wampCall<string>(
      __method, username
    ).map((json: string): UserAccount => JSON.parse(json));
  }

  public getUserAccountByUsernameOrNull(username: string): Observable<UserAccount> {
    const __method = 'com.imilia.timerbee.useraccount.getUserAccountByUsernameOrNull';
    return this.wampService.wampCall<string>(
      __method, username
    ).map((json: string): UserAccount => JSON.parse(json));
  }
}
