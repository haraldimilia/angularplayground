import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BookingService } from './booking.service';
import { CustomerService } from './customer.service';
import { DomainroleService } from './domainrole.service';
import { HealthinsuranceProviderService } from './healthinsuranceprovider.service';
import { ZoneSchedulerModule } from 'ngx-zone-scheduler';
import { PlanExecutionService } from './planexecution.service';
import { PortalService } from './portal.service';
import { TenantService } from './tenant.service';
import { TenantLocationService } from './tenantlocation.service';
import { TimelineService } from './timeline.service';
import { UseraccountService } from './useraccount.service';
import { VerificationService } from './verification.service';
import { WampCoreModule } from '../wampcore/wampcore.module';

@NgModule({
  imports: [
    CommonModule,
    WampCoreModule,
    ZoneSchedulerModule
  ],
  providers: [
    BookingService,
    CustomerService,
    DomainroleService,
    PortalService,
    TenantService,
    TenantLocationService,
    TimelineService,
    UseraccountService,
    VerificationService,
    HealthinsuranceProviderService,
    PlanExecutionService
  ],
  declarations: []
})
export class TimerbeeModule { }
