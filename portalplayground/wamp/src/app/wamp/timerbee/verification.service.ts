import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ValidationRequest } from './dto/validationrequest';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class VerificationService {
  constructor(private wampService: WampService) { }

  public validateEmail(validationRequest: ValidationRequest): Observable<ValidationRequest> {
    const __method = 'com.imilia.timerbee.verification.validateEmail';
    return this.wampService.wampCall<string>(
      __method, validationRequest
    ).map((json: string): ValidationRequest => JSON.parse(json));
  }
}
