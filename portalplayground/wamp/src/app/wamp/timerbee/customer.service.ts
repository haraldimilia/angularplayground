import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Customer } from './dto/customer';
import { Observable } from 'rxjs/Observable';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class CustomerService {
  constructor(private wampService: WampService) { }

  public createCustomer(customer: Customer): Observable<string> {
    const __method = 'com.imilia.timerbee.customer.createCustomer';
    return this.wampService.wampCall<string>(
      __method, customer
    ).map((json: string): string => JSON.parse(json));
  }
}
