import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Config } from './dto/config';
import { PreConfigs } from './dto/preconfig';
import { Observable } from 'rxjs/Observable';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class PortalService {
  constructor(private wampService: WampService) { }

  public getPreConfig(username: string): Observable<PreConfigs> {
    const __method = 'com.imilia.timerbee.portal.getPreConfig';
    return this.wampService.wampCall<string>(
      __method, username
    ).map((json: string): PreConfigs => <any>json); // no idea what is going on here - JSON.parse fails
  }

  public getConfig(usergroupoid: number, purposeOid ?: number): Observable <Config> {
    const __method = 'com.imilia.timerbee.portal.getConfig';
    return this.wampService.wampCall<string>(
      __method, [usergroupoid, purposeOid]
    ).map((json: string): Config => JSON.parse(json));
  }

}
