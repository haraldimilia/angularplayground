import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { HealthInsuranceProvider } from './dto/healthinsuranceprovider';
import { Observable } from 'rxjs/Observable';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class HealthinsuranceProviderService {
  constructor(private wampService: WampService) { }

  public getHealthInsuranceProviders(): Observable<HealthInsuranceProvider[]> {
    const __method = 'com.imilia.timerbee.healthinsuranceprovider.getHealthInsuranceProviders';
    return this.wampService.wampCall<string>(
      __method
    ).map((json: string): HealthInsuranceProvider[] => JSON.parse(json));
  }
}
