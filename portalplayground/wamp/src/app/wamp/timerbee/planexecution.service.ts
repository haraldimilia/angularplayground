import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { PlanExecution } from './dto/planexecution';
import { Observable } from 'rxjs/Observable';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class PlanExecutionService {
  constructor(private wampService: WampService) { }

  public preparePlanExecution(planScriptOid: number, userGroupOid: number,
    customerOid: number, purposeOid: number): Observable<PlanExecution> {
    const __method = 'com.imilia.timerbee.planexecution.preparePlanExecution';
    return this.wampService.wampCall<string>(
      __method, [planScriptOid, userGroupOid, customerOid, purposeOid]
    ).map((json: string): PlanExecution => JSON.parse(json));
  }

 // public applyCustomerAttributes()
}
