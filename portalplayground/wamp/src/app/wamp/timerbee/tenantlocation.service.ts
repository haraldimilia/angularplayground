import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TenantLocation } from './dto/tenantlocation';
import { WampService } from '../wampcore/wamp.service';

@Injectable()
export class TenantLocationService {
  constructor(private wampService: WampService) { }

  public getTenantLocations(): Observable<TenantLocation[]> {
    const __method = 'com.imilia.timerbee.tenantlocation.getTenantLocations';
    return this.wampService.wampCall<string>(
      __method
    ).map((json: string): TenantLocation[] => JSON.parse(json));
  }
}
