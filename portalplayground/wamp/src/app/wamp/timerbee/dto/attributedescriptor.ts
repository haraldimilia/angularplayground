import { AttributeType } from './attributetype';
import { AttributeValueOwner } from './attributevalueowner';


export interface AttributeDescriptor {
  oid: number;
  name: string;
  attributeValueType: string;
  attributeCategory: string;
  listAttributeValueOwner?: AttributeValueOwner;
  attributeType?: AttributeType;
}
