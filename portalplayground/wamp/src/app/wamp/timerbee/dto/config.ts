import { PurposeSelectionConfig } from './purposeselectionconfig';
import { TimerbeeCalendar } from './timerbeecalendar';

export interface Config {
  oid: number;
  step: number;
  resourceGroupSelectable: boolean;
  resourceSelectable: boolean;
  purposeGroupSelectable: boolean;
  weekDaySelectable: boolean;
  dayTimeSelectable: boolean;
  smsAvailable: boolean;
  smsConfirmation: boolean;
  bookingWindowEmptySlotsText: string;
  bookingWindowCaption?: string;
  usePurposeBookingWindow: boolean;
  demoMode: boolean;
  calendar: TimerbeeCalendar;
  purposeSelection?: PurposeSelectionConfig;
}
