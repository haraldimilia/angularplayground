import { PreviewDay } from './previewday';

export interface PreviewWeek {
  weekNumber: string; // for some stupid reasons this is a string?
  days: PreviewDay[];
}
