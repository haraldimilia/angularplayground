import { TimelineSlot } from './timelineslot';

export interface Booking {
  slots: TimelineSlot[];
}
