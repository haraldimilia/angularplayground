import { Address } from './address';
import { HealthInsuranceType } from './healthinsurancetype';

export interface HealthInsuranceProvider {
  oid: number;
  name: string;
  healthInsuranceType: HealthInsuranceType;
  address: Address;
}
