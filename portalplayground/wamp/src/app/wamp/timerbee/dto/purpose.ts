import { BookingPlan } from './bookingplan';

export interface Purpose {
  oid: number;
  name: string;
  minDuration: string;
  maxDuration: string;
  standardDuration: string;
  selectedDuration: string; // no idea where this appears??? maybe just from the old frontend
  appointmentRequest: boolean;
  bookingPlans: BookingPlan[];
}
