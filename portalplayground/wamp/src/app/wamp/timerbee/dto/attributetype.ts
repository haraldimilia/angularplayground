export interface AttributeType {
  oid: number;
  attributeValueType: string;
  attributeCategory: string;
}
