import { Purpose } from './purpose';

export interface PurposeQuery {
  purpose: Purpose;
  minRequiredPeriod: string;
}



