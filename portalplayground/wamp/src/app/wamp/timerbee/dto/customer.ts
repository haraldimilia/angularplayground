import { DomainRole } from './domainrole';
import { Patient } from './patient';
import { Person } from './person';
import { Tenant } from './tenant';

export interface Customer {
  oid: number;
  person: Person;
  patient: Patient;
  owner: Tenant;
  grantedRole: DomainRole;
}
