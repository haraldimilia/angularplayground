export interface Tenant {
  customerId?: string;
  dataProtectionText?: string;
  legaleseText?: string;
  name?: string;
  oid: number;
  serverUri?: string;
}
