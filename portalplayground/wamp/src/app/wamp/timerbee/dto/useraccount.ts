export interface UserAccount {
  oid: number;
  userName: string;
}
