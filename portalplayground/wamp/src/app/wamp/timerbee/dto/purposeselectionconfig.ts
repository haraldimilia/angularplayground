import { Purpose } from './purpose';

export interface PurposeSelectionConfig {
  purposes: Purpose[];
}
