export interface ValidationRequest {
  email: string;
  tenantOid: number;
  mobileNumber?: string;
  hashCode?: string;
  error?: boolean;
}
