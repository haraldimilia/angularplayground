export interface TimeOfDayInterval  {
  name: string;
  beginTime: string;
  endTime: string;
}
