import { AttributeValue } from './attributevalue';

export interface AttributeValueOwner {
  oid: number;
  attributeValues?: AttributeValue[];
}
