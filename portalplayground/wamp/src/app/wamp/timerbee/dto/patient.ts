export interface Patient {
  oid: number;
  healthInsuranceProvider: string;
  healthInsuranceType: string;

  //
  // are these two below correct?
  //
  patientHeight: number;
  patientWeight: number;
}
