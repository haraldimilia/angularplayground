export interface UserGroup {
  oid: number;
  name: string;
}
