export interface Resource {
  description: string;
  flags: number;
  id: number;
  maxBookingsPerPersonDay: number;
  maxBookingsPerPersonInAdvance: number;
  name: string;
  oid: number;
}
