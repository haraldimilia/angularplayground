export interface DomainRole {
  oid: number;
  name: string;
  flags: number;
}
