import { UserGroup } from './usergroup';

export interface Option {
  userGroup: UserGroup;
}
