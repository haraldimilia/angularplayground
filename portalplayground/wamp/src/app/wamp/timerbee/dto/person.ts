export interface Person {
  alias: string;
  lastName: string;
  firstName: string;
  gender: string;
  dob: string;
}
