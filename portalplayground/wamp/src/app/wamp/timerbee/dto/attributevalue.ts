import { AttributeDescriptor } from './attributedescriptor';
import { TextBlock } from './textblock';

export interface AttributeValue {
  oid: number;
  name: string;
  stringValue?: string;
  booleanValue?: boolean;
  longValue?: number;
  doubleValue?: number;
  dateTimeValue?: Date;
  localDateValue?: Date;
  icon?: TextBlock;
  flags: number;
  attributeDescriptor: AttributeDescriptor;
}

