import { TimelinePreview } from './timelinepreview';

export interface TimerbeeCalendar {
  months: TimelinePreview[];
}
