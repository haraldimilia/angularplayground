import { CalendarRule } from './calendarrule';
import { Participant } from './participant';
import { Purpose } from './purpose';
import { Swimlane } from './swimlane';
import { TenantLocation } from './tenantlocation';

export interface TimelineSlot {
  plannedBegin: Date;
  plannedEnd: Date;
  swimlane: Swimlane;
  calendarRule: CalendarRule;
  purpose: Purpose;
  participants: Participant[];
  tenantLocation: TenantLocation;
}



