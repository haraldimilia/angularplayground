export interface PlanScript {
  oid: number;
  validFrom: Date;
}
