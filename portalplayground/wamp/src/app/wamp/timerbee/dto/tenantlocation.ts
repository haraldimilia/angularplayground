import { Address } from './address';

export interface TenantLocation {
  oid: number;
  name: string;
  address: Address;
}
