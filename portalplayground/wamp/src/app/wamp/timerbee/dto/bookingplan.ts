import { PlanScript } from './planscript';

export interface BookingPlan {
  oid: number;
  name: string;
  description: string;
  schemaId: string;
  planScripts: PlanScript[];
}
