export interface Address {
  oid: number;
  url: string;
  street: string;
  zip: string;
  city: string;
  country: string;
  houseNumber: string;
}
