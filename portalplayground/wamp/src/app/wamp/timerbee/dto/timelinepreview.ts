import { PreviewWeek } from './previewweek';

export interface TimelinePreview {
  firstOfMonth: Date;
  firstDayOfWeek: number;
  locale: string;
  weeks: PreviewWeek[];
}
