import { AttributeValue } from './attributevalue';
import { PlanScript } from './planscript';
import { Purpose } from './purpose';
import { UserGroup } from './usergroup';

export interface PlanExecution {
  cid: string;
  userGroup: UserGroup;
  planScript: PlanScript;
  purpose: Purpose;
  attributeValues: AttributeValue[];
}
