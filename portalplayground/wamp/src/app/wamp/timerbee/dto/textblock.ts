export interface TextBlock {
  oid: number;
  name: string;
  type: string;
}
