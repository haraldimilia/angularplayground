import { LocationDTO } from './location';
import { PurposeQuery } from './purposequery';
import { Resource } from './resource';
import { TimelineSlot } from './timelineslot';
import { TimeOfDayInterval } from './timeofdayinterval';
import { UserGroup } from './usergroup';

export interface TimelineQuery {
  intervals: string[];
  resources: Resource[];
  purposeQueries: PurposeQuery[];
  flags: number;
  locations: LocationDTO[];
  userGroup: UserGroup;
  prependDate: Date;
  slots2Ignore: TimelineSlot[];
  timeOfDayIntervals: TimeOfDayInterval[];
}



