import { Option } from './option';

export interface PreSelection {
  options: Option[];
}
