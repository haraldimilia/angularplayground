import { Resource } from './resource';
import { Tenant } from './tenant';

export interface ResourceGroup {
  id?: string;
  name?: string;
  oid?: number;
  owner?: Tenant;
  resource?: Resource;
  resourceGroupMembers: ResourceGroup[];
}
