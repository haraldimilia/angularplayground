export interface PreviewDay {
  date: Date;
  flags: number;
}
