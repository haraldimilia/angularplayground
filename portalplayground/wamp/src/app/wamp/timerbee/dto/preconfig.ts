import { PreSelection } from './preselection';

export interface PreConfigs {
  preConfigs: PreConfig[];
}

export interface PreConfig {
  oid: number;
  name: string;
  dataProtectionText: string;
  legaleseText: string;
  ownerOid: string;
  preSelection: PreSelection;
  tenantLocationOid: number;
  tenantName: string;
  userName: string;
  uuid: string;
}
