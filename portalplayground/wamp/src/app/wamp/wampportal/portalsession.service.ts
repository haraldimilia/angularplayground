import { Injectable } from '@angular/core';
import { PlanExecution } from './../timerbee/dto/planexecution';

import { Config } from '../timerbee/dto/config';
import { DomainRole } from '../timerbee/dto/domainrole';
import { DomainroleService } from '../timerbee/domainrole.service';
import { HealthInsuranceProvider } from '../timerbee/dto/healthinsuranceprovider';
import { HealthinsuranceProviderService } from '../timerbee/healthinsuranceprovider.service';
import { Log } from 'ng2-logger/client';
import { PlanExecutionService } from './../timerbee/planexecution.service';
import { PortalService } from '../timerbee/portal.service';
import { PreConfigs } from '../timerbee/dto/preconfig';
import { Purpose } from '../timerbee/dto/purpose';
import { ResourceGroup } from '../timerbee/dto/resourcegroup';
import { forkJoin } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { ServerConfig } from '../wampcore/dto/serverconfig';
import { ServerConfigService } from '../wampcore/serverconfig.service';
import { TenantService } from '../timerbee/tenant.service';
import { TenantLocation } from '../timerbee/dto/tenantlocation';
import { TenantLocationService } from '../timerbee/tenantlocation.service';
import { TimelineService } from '../timerbee/timeline.service';
import { TimeOfDayInterval } from '../timerbee/dto/timeofdayinterval';
import { WampService } from '../wampcore/wamp.service';

const log = Log.create('PortalSessionService');

@Injectable({
  providedIn: 'root'
})
export class PortalSessionService {
  private static instance: PortalSessionService;
  private userName: string;
  private domainRole: DomainRole;
  private healthInsuranceProviders: HealthInsuranceProvider[] = [];
  public timeOfDayIntervals: TimeOfDayInterval[] = [];
  private preConfigs: PreConfigs = null;
  private tenantOid: number = null;
  private usergroupoid: number = null;
  private tenantResourceGroup: ResourceGroup[] = [];
  private tenantLocations: TenantLocation[] = [];
  private config: Config = null;
  public purposes: Purpose[];

  constructor(private serverConfigService: ServerConfigService, private wampService: WampService,
    private domainroleService: DomainroleService, private healthinsuranceProviderService: HealthinsuranceProviderService,
    private timelineService: TimelineService, private portalService: PortalService,
    private tenantService: TenantService, private tenantLocationService: TenantLocationService,
    private planExecutionService: PlanExecutionService) {
    // https://stackoverflow.com/questions/36198785/how-do-i-create-a-singleton-service-in-angular-2
    // force this as singleton
    if (PortalSessionService.instance) {
      log.info('constructor -> singleton');
      return PortalSessionService.instance;
    }

    log.info('constructor');
    return this;
  }

  public portalLogin(uuid: string): Observable<boolean> {
    log.info('portalLogin', uuid);

    this.resetData();

    if (WampService.serverConfig == null) {
      // this is the very first call - so we need a rest call to the server
      return new Observable<boolean>((observer) => {
        this.serverConfigService.getServerInfo().subscribe(
          (data: ServerConfig) => {
            // log.info('getServerInfo', data);
            WampService.serverConfig = data;
            this.portalLoginInternal(uuid).subscribe(
              (ok: boolean) => {
                log.info('portalLoginInternal', ok);
                observer.next(ok);
                observer.complete();
              },
              (err: any) => {
                log.error('portalLoginInternal error', err);
                observer.error(err);
                observer.complete();
              }
            );
          },
          (err: any) => {
            log.error('getServerInfo error', err);
            observer.error(err);
            observer.complete();
          }
        );
      });
    }
    return this.portalLoginInternal(uuid);
  }

  public loadBaseData(): Observable<boolean> {
    log.info('loadBaseData');
    return new Observable<boolean>((observer) => {
      forkJoin(
        this.domainroleService.getPatientDomainRole(),
        this.healthinsuranceProviderService.getHealthInsuranceProviders(),
        this.timelineService.getDefaultTimeOfDayIntervals(),
        this.portalService.getPreConfig(this.userName)

      )
      .subscribe(
        ([domainRole, healthInsuranceProviders, timeOfDayIntervals, preConfigs]) => {
          this.domainRole = domainRole;
          this.healthInsuranceProviders = healthInsuranceProviders;
          this.timeOfDayIntervals = timeOfDayIntervals;
          this.preConfigs = preConfigs;

          if ( this.preConfigs == null || this.preConfigs === undefined || this.preConfigs.preConfigs == null ||
            this.preConfigs.preConfigs === undefined || this.preConfigs.preConfigs.length !== 1) {
            log.error('preConfigs empty', this.preConfigs);
            observer.error('cant get configuration');
            observer.complete();
            return;
          }

            this.tenantOid =  this.preConfigs.preConfigs[0].oid;
            this.usergroupoid =  this.preConfigs.preConfigs[0].preSelection.options[0].userGroup.oid; // wtf?!
            this.wampService.tenantOid = this.tenantOid;

            this.loadTenantBasedata().subscribe(
              (ok: boolean) => {
                observer.next(ok);
                observer.complete();
              },
              (err: any) => {
                log.error('loadTenantBasedata error', err);
                observer.error(err);
                observer.complete();
              }
            );
        },
        (err) => {
          log.error('loadBaseData error', err);
          observer.error(err);
          observer.complete();
        });
    });
  }

  selectPurpose(purposeOid: number): Observable<Config> {
    return new Observable<Config>((observer) => {
      this.portalService.getConfig(this.usergroupoid, purposeOid).subscribe(
        (config: Config) => {
          log.info('selectPurpose', config);
          observer.next(config);
          observer.complete();
        },
        (err: any) => {
          log.error('selectPurpose error', err);
          observer.error(err);
          observer.complete();
        }
      );
    });
  }

  preparePlanExecution(planScriptOid: number, purposeOid: number): Observable<PlanExecution> {
    return new Observable<PlanExecution>((observer) => {
      this.planExecutionService.preparePlanExecution(planScriptOid, this.usergroupoid, this.wampService.tenantOid, purposeOid).subscribe(
        (planExecution: PlanExecution) => {
          log.info('selectPurpose', planExecution);
          observer.next(planExecution);
          observer.complete();
        },
        (err: any) => {
          log.error('selectPurpose error', err);
          observer.error(err);
          observer.complete();
        }
      );
    });
  }

  private resetData() {
    log.info('resetData');
    this.userName = null;
    this.domainRole = null;
    this.healthInsuranceProviders = [];
    this.timeOfDayIntervals = [];
    this.preConfigs = null;
    this.tenantOid = null;
    this.usergroupoid = null;
    this.tenantResourceGroup = [];
    this.tenantLocations = [];
    this.config = null;
    this.purposes = [];
  }

  private portalLoginInternal(uuid: string): Observable<boolean> {
    log.info('portalLoginInternal', uuid);

    return new Observable<boolean>((observer) => {
      this.wampService.loginByUUID(uuid).subscribe(
        (ok: boolean) => {
          log.info('loginByUUID', ok);
          this.userName = uuid; // save uuid
          observer.next(ok);
          observer.complete();
        },
        (err: any) => {
          log.error('loginByUUID error', err);
          observer.error(err);
          observer.complete();
        }
      );
    });
  }

  private loadTenantBasedata(): Observable<boolean> {
    log.info('loadTenantBasedata');

    return new Observable<boolean>((observer) => {
      forkJoin(
        this.tenantService.getTenantResourceGroups(this.tenantOid),
        this.tenantLocationService.getTenantLocations(),
        this.portalService.getConfig(this.usergroupoid)

      )
      .subscribe(
        ([tenantResourceGroup, tenantLocations, config]) => {
          this.tenantResourceGroup = tenantResourceGroup;
          this.tenantLocations = tenantLocations;
          this.config = config;

          if (!this.config || !this.config.purposeSelection ||
            !this.config.purposeSelection.purposes || this.config.purposeSelection.purposes.length === 0) {
            log.error('purposeSelection empty');
            observer.error('cant get loadTenantBasedata');
            observer.complete();
            return;
          }

          this.purposes = this.config.purposeSelection.purposes;
          observer.next(true);
          observer.complete();
        },
        (err) => {
          log.error('loadTenantBasedata error', err);
          observer.error(err);
          observer.complete();
        });
    });
  }
}
