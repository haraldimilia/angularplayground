import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ZoneSchedulerModule } from 'ngx-zone-scheduler';
import { PortalSessionService } from './portalsession.service';
import { TimerbeeModule } from '../timerbee/timerbee.module';
import { WampCoreModule } from '../wampcore/wampcore.module';

@NgModule({
  imports: [
    CommonModule,
    WampCoreModule,
    TimerbeeModule,
    ZoneSchedulerModule,
  ],
  providers: [
    PortalSessionService,
  ],
  declarations: []
})
export class WampPortalModule { }
