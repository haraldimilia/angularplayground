netsh advfirewall firewall add rule name="Imilia TCP Port 8080" dir=in action=allow protocol=TCP localport=8080
netsh advfirewall firewall add rule name="Imilia TCP Port 8080" dir=out action=allow protocol=TCP localport=8080

netsh advfirewall firewall add rule name="Imilia TCP Port 4200" dir=in action=allow protocol=TCP localport=4200
netsh advfirewall firewall add rule name="Imilia TCP Port 4200" dir=out action=allow protocol=TCP localport=4200

netsh advfirewall firewall add rule name="Imilia TCP Port 8444" dir=in action=allow protocol=TCP localport=8444
netsh advfirewall firewall add rule name="Imilia TCP Port 8444" dir=out action=allow protocol=TCP localport=8444