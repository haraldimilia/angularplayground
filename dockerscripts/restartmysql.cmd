docker rm -fv mysql
docker rm helper

docker volume create --driver local conf.d
docker run -v conf.d:/etc/mysql/conf.d --name helper busybox true
cd conf.d
docker cp . helper:/etc/mysql/conf.d
cd ..
docker rm helper

docker run --name mysql --restart=always -v conf.d:/etc/mysql/conf.d -e MYSQL_DATABASE=imilia -e MYSQL_USER=imilia -e MYSQL_PASSWORD=imilia -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 -d mysql:5.6
docker cp .\timerbee_demo.sql mysql:/tmp/timerbee_demo.sql
rem Windows way to sleep 30sec
ping 127.0.0.1 -n 30  > nul
docker exec -it mysql /bin/sh -c "mysql imilia -uimilia -pimilia < /tmp/timerbee_demo.sql"